# Archivage

Archivage est une application qui permet de gérer un répertoire local de fichier
.eml et de réaliser des opérations de synchronisation avec un serveur Zimbra distant.

## Prérequis

Java 17, par exemple avec `java -version`

```
openjdk version "17.0.6" 2023-01-17
OpenJDK Runtime Environment (build 17.0.6+10-Ubuntu-0ubuntu122.04)
OpenJDK 64-Bit Server VM (build 17.0.6+10-Ubuntu-0ubuntu122.04, mixed mode, sharing)
```

## Lancer l’application

À partir du `.jar` produit par le `mvn package`,

```bash
java -jar archivage-.jar
```

## Configuration

Pour l’utilisateur `Bernard` qui va utiliser l’application, le fichier de configuration va se trouver :

- Sous Windows : `C:\Users\Bernard\AppData\Roaming\MIM-Libre/Archivage`
- Sous Linux : `/home/bernard/.config/mim-libre-archivage/archivage-config.json`

Le fichier sera créé au premier lancement.

Configuration à l’installation :

```json
{
  "version": 0,
  "zimbraSoapUri": "",
  "zimbraRestUri": "",
  "loggingLevel": "STANDARD",
  "loggingDaysOfRetention": 14,
  "synchronisationBatchSize": 500,
  "profiles": [ ]
}
```

Par défaut, les URLs vers Zimbra sont vides. Il faut les renseigner avec celles du serveur que vous voulez utiliser. Normalement, l'URL `zimbraSoapUri` est de la forme `https://domaine.com/service/soap` et l'URL `zimbraRestUri` est de la forme `https://domaine.com/service`. Le fichier de configuration `archivage.properties`, contenant les valeurs qui sont injectées dans ce JSON de configuration, est volontairement laissé vide sur ce dépôt, et doit le rester. Vous pouvez y renseigner les valeurs dans le cas où vous avez fork du dépôt original d'Archivage à partir duquel vous réalisez votre propre distribution du logiciel.

Une configuration avec des paramètres optionnels et un profil définit :

```json
{
  "version": 0,
  "zimbraSoapUri": "https://mail.onzextras.fr/service/soap",
  "zimbraRestUri": "https://mail.onzextras.fr/service",
  "loggingLevel": "VERBOSE",
  "loggingDaysOfRetention": 14,
  "zoneId": "Europe/Paris",
  "synchronisationBatchSize": 500,
  "profiles": [
    {
      "id": "1",
      "mountPointPaths": [
        "file:///C:/Users/Bernard/email-archivage/"
      ],
      "synchronizationMountPointPath" : "file:///C:/Users/Bernard/email-archivage/",
      "visualizationMountPointPaths" : [
        "file:///C:/Users/Bernard/un-autre-dossier-archive/"
      ],
      "login": "bernard@try.onzextras.fr",
      "remoteEmailsMustBeArchivedAfterDaysCount": 1000,
      "defaultAttachmentsSavingPath": "file:///C:/Users/Bernard/",
      "mailTableSortPreference": {
        "column": "SENT_DATE",
        "sortType": "DESCENDING"
      }
    }
  ]
}
```

## Build de l'application

### Prérequis

Maven 3, par exemple avec `mvn -version`

```
Apache Maven 3.6.3
Maven home: /usr/share/maven
Java version: 17.0.6, vendor: Private Build, runtime: /usr/lib/jvm/java-17-openjdk-amd64
Default locale: fr_FR, platform encoding: UTF-8
OS name: "linux", version: "5.15.0-69-generic", arch: "amd64", family: "unix"
```

### Produire un .jar exécutable

```
mvn package
```

### Pendant le développment

On peut lancer l’application avec `mvn javafx:run`

Pour pouvoir lancer tous les tests de l'application, il faut indiquer les URLs vers un serveur Zimbra, ainsi que les identifiants d'un compte sur ce serveur. Les valeurs sont à renseigner dans les fichiers suivants que l'on trouve dans le dossier `src/test/resources` :

- `archivage.properties`
- `zimbra-credentials.properties`

### Produire un livrable standalone multiplateformes

Le POM a été configuré de sorte à pouvoir générer un livrable complètement standalone pour Windows, c'est-à-dire qu'il n'y a pas besoin d'avoir un JDK ou un JRE installé sur la machine hôte.

Pour ce faire, il faut tout d'abord télécharger un JRE qui embarque les dépendances à JavaFX. Nous avons utilisé le JRE 17 produit par Liberica, en version "full", disponible [ici](https://download.bell-sw.com/java/17.0.7+7/bellsoft-jre17.0.7+7-windows-amd64-full.zip).

Dézippez le contenu du JRE, et renommez le dossier obtenu en `jre`. Puis exécutez simplement `mvn package`. Dans `target`, vous verrez un fichier ZIP qui a été généré. Il contient : le JAR de l'application, le JDK ainsi qu'un fichier exécutable pour Windows. Lorsque vous voulez lancer l'application sous Windows, il faut simplement lancer ce fichier exécutable.

## Release de l’application

Pour livrer l'application en version `1.2.3`, créer une branche `release/1.2.3` et la pousser.

Gitlab CI réalisera le process et publiera le .zip attendu dans les artefacts du build.
