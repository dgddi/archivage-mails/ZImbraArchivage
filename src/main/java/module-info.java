/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
module fr.mim_libre.archivage {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires dev.dirs;
    requires org.controlsfx.controls;
    requires org.simplejavamail;
    requires org.simplejavamail.core;
    requires jakarta.mail;
    requires org.apache.lucene.core;
    requires org.apache.lucene.analysis.common;
    requires org.apache.commons.io;
    requires org.apache.commons.lang3;
    requires org.apache.logging.log4j;
    requires java.net.http;
    requires java.xml;
    requires com.google.common;
    requires org.jsoup;
    requires com.fasterxml.jackson.dataformat.xml;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires org.apache.httpcomponents.httpclient;
    requires com.fasterxml.jackson.datatype.jdk8;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.ikonli.remixicon;
    requires org.kordamp.ikonli.core;
    requires java.desktop;
    requires org.nibor.autolink;
    requires jdk.xml.dom;

    opens fr.mim_libre.archivage to javafx.fxml;
    exports fr.mim_libre.archivage;
    exports fr.mim_libre.archivage.index;
    opens fr.mim_libre.archivage.index to javafx.fxml;
    exports fr.mim_libre.archivage.store;
    opens fr.mim_libre.archivage.store to javafx.fxml;
    exports fr.mim_libre.archivage.views;
    opens fr.mim_libre.archivage.views to javafx.fxml;
    opens fr.mim_libre.archivage.views.files to javafx.fxml;
    opens fr.mim_libre.archivage.views.mails to javafx.fxml, javafx.base;
    opens fr.mim_libre.archivage.views.components to javafx.fxml;

    opens fr.mim_libre.archivage.zimbra to com.fasterxml.jackson.databind;
    exports fr.mim_libre.archivage.views.mails to com.fasterxml.jackson.databind;
    exports fr.mim_libre.archivage.views.files to com.fasterxml.jackson.databind;
    exports fr.mim_libre.archivage.configuration;
    opens fr.mim_libre.archivage.configuration to com.fasterxml.jackson.databind, javafx.fxml;
    exports fr.mim_libre.archivage.logging to com.fasterxml.jackson.databind;
}
