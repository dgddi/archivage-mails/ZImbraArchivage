package fr.mim_libre.archivage;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.io.Resources;
import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import fr.mim_libre.archivage.configuration.ArchivageMisconfigurationException;
import fr.mim_libre.archivage.configuration.ArchivageProperties;
import fr.mim_libre.archivage.configuration.ConfigurationManager;
import fr.mim_libre.archivage.configuration.Profile;
import fr.mim_libre.archivage.index.ArchivageIndex;
import fr.mim_libre.archivage.index.IndexationRequest;
import fr.mim_libre.archivage.index.IndexationTask;
import fr.mim_libre.archivage.logging.ArchivageLogger;
import fr.mim_libre.archivage.store.Store;
import fr.mim_libre.archivage.views.AppController;
import fr.mim_libre.archivage.views.ControllerDoingSomethingOnApplicationClosing;
import fr.mim_libre.archivage.views.ControllerDoingSomethingOnApplicationShowing;
import fr.mim_libre.archivage.views.ProfileCreationController;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import fr.mim_libre.archivage.zimbra.ZimbraAuthToken;
import fr.mim_libre.archivage.zimbra.ZimbraService;
import javafx.application.Application;
import javafx.concurrent.Worker;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Cette classe est utilisée pour lancer les stages qui permettent l'affichage des vues
 */
public class MainApplication extends Application {

    private static final Logger LOGGER = LogManager.getLogger(MainApplication.class);

    public static final String VERSION;

    private static ArchivageConfiguration configuration;

    private static ArchivageService archivageService;

    private static ZimbraSessionManager zimbraSessionManager;

    private static ZimbraService zimbraService;

    private static ConfigurationManager configurationManager;

    /**
     * Le profil qui a été choisi par l’utilisateur
     */
    private static String profileId;

    /**
     * Les contrôleurs qu’il faudra notifier de la fermeture de l’application.
     */
    private static final Set<ControllerDoingSomethingOnApplicationClosing> CLOSE_LISTENERS = new LinkedHashSet<>();

    /**
     * Les contrôleurs qu’il faudra notifier de l'ouverture de l’application.
     */
    private static final Set<ControllerDoingSomethingOnApplicationShowing> SHOW_LISTENERS = new LinkedHashSet<>();

    static {
        final Runtime.Version runtimeVersion = Runtime.version();
        LOGGER.info("Version de Java utilisée : {}", runtimeVersion.toString());
        if (runtimeVersion.feature() > 17) {
            LOGGER.warn("Attention, l'application est conçue pour être exécutée avec Java 17");
        }
        try {
            VERSION = Resources.toString(
                    Objects.requireNonNull(MainApplication.class.getResource("archivage-version.txt")),
                    StandardCharsets.UTF_8
            );
            LOGGER.info("version du logiciel = " + VERSION);
        } catch (IOException e) {
            LOGGER.error("impossible de lire la version du logiciel", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Factorise la création d’une {@link IndexationTask}.
     *
     * Notamment, pour gérer tout ce qui est journalisation des erreurs.
     */
    public static IndexationTask startIndexationTask(ArchivageService archivageService, IndexationRequest indexationRequest) {
        IndexationTask indexationTask = new IndexationTask(archivageService, indexationRequest);
        indexationTask.stateProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == Worker.State.FAILED) {
                LOGGER.error("erreur pendant l’exécution de " + indexationTask, indexationTask.getException());
            }
        });
        Thread thread = new Thread(indexationTask);
        thread.setUncaughtExceptionHandler((t, e) -> LOGGER.error("erreur pendant l’exécution de " + indexationTask, e));
        thread.start();
        return indexationTask;
    }

    /**
     * Méthode principale qui sert à lancer l'application.
     * S'il n'y a aucun utilisateur enregistré, on lance la scène de création de profil.      *
     * Sinon, on lance la scène principale.
     */
    @Override
    public void start(Stage stage) throws IOException, ArchivageMisconfigurationException {

        configurationManager = ConfigurationManager.forOs();

        try {
            configuration = configurationManager.getConfiguration();
        } catch (ArchivageMisconfigurationException e) {
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
            throw e;
        }

        zimbraService = new ZimbraService(configuration);
        zimbraSessionManager = new ZimbraSessionManager(zimbraService);

        BiConsumer<String, Boolean> startMainStageForProfileCallback = (chosenProfileId, firstConnexion) -> {
            profileId = chosenProfileId;
            startMainStage(stage, firstConnexion);
        };

        stage.getIcons().add(getApplicationImage());

        switch (configurationManager.getProfileIds().size()) {
            // si pas de profil connu, on lance l’assistant de création d’un profil
            case 0 -> {
                FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("profile-creation.fxml"));
                Rectangle2D bounds = Screen.getPrimary().getBounds();
                Scene loginScene = new Scene(loginLoader.load(), bounds.getWidth() * 0.7, bounds.getHeight() * 0.7);
                loginScene.getStylesheets().add(MainApplication.class.getResource("style/app.css").toExternalForm());
                stage.setTitle("Création du profil");
                stage.setMinHeight(bounds.getHeight() * 0.4);
                stage.setMinWidth(bounds.getWidth() * 0.4);
                stage.sizeToScene();
                stage.setScene(loginScene);
                stage.show();
                ProfileCreationController profileCreationController = loginLoader.getController();
                profileCreationController.setStartMainStageForProfileCallback(startMainStageForProfileCallback);
            }
            // si un seul profil, on lance directement l'application
            case 1 -> {
                String onlyProfileId = Iterables.getOnlyElement(configurationManager.getProfileIds());
                startMainStageForProfileCallback.accept(onlyProfileId, false);
            }
            default -> throw new IllegalStateException("non géré : plusieurs profils " + configurationManager.getProfileIds());
        }
    }

    @Override
    public void stop() {
        for (ControllerDoingSomethingOnApplicationClosing closeListener : CLOSE_LISTENERS) {
            closeListener.onApplicationClose();
        }
    }

    public static void registerCloseListener(ControllerDoingSomethingOnApplicationClosing closeListener) {
        CLOSE_LISTENERS.add(closeListener);
    }

    public static void registerShowListener(ControllerDoingSomethingOnApplicationShowing showListener) {
        SHOW_LISTENERS.add(showListener);
    }

    /**
     * Lance la vue principale de l'application
     * @param firstConnexion booléen qui indique si c'est la première connexion de l'utilisateur
     */
    private void startMainStage(Stage stage, boolean firstConnexion) {
        archivageService = newArchivageService(profileId);
        archivageService.cleanLogs();
        boolean initialized = initializeZimbraMailBox(zimbraSessionManager.getSessionStatus());
        if (!initialized) {
            zimbraSessionManager.addSessionListener(new ZimbraMailBoxInitializer());
        }

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("app-view.fxml"));
            Rectangle2D bounds = Screen.getPrimary().getBounds();
            Scene scene = new Scene(fxmlLoader.load(), bounds.getWidth(), bounds.getHeight());
            // Si c'est la première connexion de l'utilisateur, on veut que le panneau de profil soit ouvert
            if (firstConnexion) {
                AppController controller = fxmlLoader.getController();
                controller.toggleProfilePane();
            }
            scene.getStylesheets().add(MainApplication.class.getResource("style/app.css").toExternalForm());
            stage.setTitle("Archivage");
            stage.setHeight(bounds.getHeight() * 0.8);
            stage.setWidth(bounds.getWidth() * 0.8);
            stage.setMinHeight(bounds.getHeight() * 0.5);
            stage.setMinWidth(bounds.getWidth() * 0.5);
            stage.setScene(scene);
            stage.show();
            for (ControllerDoingSomethingOnApplicationShowing showListener : SHOW_LISTENERS) {
                showListener.onApplicationShow();
            }
        } catch (IOException e) {
            LOGGER.error("Erreur après l'indexation :", e);
            throw new RuntimeException(e);
        }

        registerUncaughtExceptionHandler();
    }

    /**
     * Configurer le traitement des erreurs dans les threads
     *
     * Permet de faire en sorte que les exceptions non catchées qui surviennent
     * dans les Thread qu'on gère à la main (indexation, synchro...) soient
     * journalisées et finissent pas juste sur la sortie standard.
     *
     * Sans ça, on tombe dans la gestion par défaut {@link ThreadGroup#uncaughtException(Thread, Throwable)}
     * qui se contente de mettre la stacktrace sur la sortie erreur standard.
     */
    private static void registerUncaughtExceptionHandler() {
        Thread.currentThread().setUncaughtExceptionHandler((t, e) -> LOGGER.error("erreur pendant l’exécution de " + t, e));
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> LOGGER.error("erreur pendant l’exécution de " + t, e));
    }

    public static ArchivageService newArchivageService(String profileId) {
        Profile profile = configurationManager.getProfile(profileId);
        ArchivageLogger archivageLogger = new ArchivageLogger(configuration, profile.getLogsDirectoryPath());
        Store store = new Store(archivageLogger);
        Path indexPath = profile.getIndexPath();
        ArchivageIndex archivageIndex = new ArchivageIndex(indexPath);
        ArchivageService archivageService = new ArchivageService(
                zimbraService,
                store,
                archivageIndex,
                archivageLogger,
                configurationManager,
                configuration
        );
        return archivageService;
    }

    public static ArchivageService getArchivageService() {
        return archivageService;
    }

    public static boolean isProfileDefined() {
        return profileId != null;
    }

    public static Profile getProfile() {
        Preconditions.checkNotNull(profileId);
        return getConfigurationManager().getProfile(profileId);
    }

    public static ZimbraSessionManager getZimbraSessionManager() {
        return zimbraSessionManager;
    }

    public static ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    public static ArchivageConfiguration getConfiguration() {
        return configuration;
    }

    public static Image getApplicationImage() {
        // FontIcon myIcon = new FontIcon(RemixiconAL.ERROR_WARNING_FILL);
        // return myIcon.snapshot(null, null);

        ArchivageProperties archivageProperties = null;
        try {
            archivageProperties = new ArchivageProperties();
        } catch (PropertiesFileNotFoundArchivageException e) {
            throw new RuntimeException(e);
        }
        return new Image(MainApplication.class.getResource(archivageProperties.getArchivageLogo()).toExternalForm());
    }

    public static void main(String[] args) {
        LOGGER.trace("démarrage");
        launch(args);
    }

    /**
     * Écoute la session pour, à la première connexion réussie, initialiser la boîte Zimbra.
     */
    private static class ZimbraMailBoxInitializer implements Consumer<SessionStatus> {

        private boolean initialized;

        @Override
        public void accept(SessionStatus sessionStatus) {
            if (!initialized) {
                initialized = initializeZimbraMailBox(sessionStatus);
            }
        }
    }

    private static boolean initializeZimbraMailBox(SessionStatus sessionStatus) {
        boolean initialized = false;
        if (sessionStatus == SessionStatus.CONNECTED) {
            LOGGER.info("première authentification depuis le lancement, on initialise la boîte Zimbra");
            ZimbraAuthToken zimbraAuthToken = zimbraSessionManager.getZimbraAuthToken();
            try {
                archivageService.setUpZimbra(zimbraAuthToken);
                initialized = true;
            } catch (ArchivageZimbraException e) {
                Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Une erreur est survenue pendant l’initialisation de la boîte distante. " + e.getMessage(), ButtonType.OK);
                alert.showAndWait();
                throw new RuntimeException(e);
            }
        }
        return initialized;
    }
}
