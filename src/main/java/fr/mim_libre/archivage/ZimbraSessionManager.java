package fr.mim_libre.archivage;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import fr.mim_libre.archivage.zimbra.ZimbraAuthToken;
import fr.mim_libre.archivage.zimbra.ZimbraService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;

/**
 * Gestion de la session Zimbra en tant que client.
 *
 * Gère
 * <ul>
 *   <li>l’appel au backend pour l’authentification</li>
 *   <li>la conservation du jeton</li>
 *   <li>l’expiration du jeton</li>
 *   <li>la levée d’évènements en fonction de l’ouverture/fermeture de la session</li>
 * </ul>
 */
public class ZimbraSessionManager {

    private static final Logger LOGGER = LogManager.getLogger(ZimbraSessionManager.class);

    /**
     * Un time pour déclencher un évènement à l’expiration du jeton.
     */
    private final Timer timer = new Timer("zimbra-session-timer", true);

    private final ZimbraService zimbraService;

    /**
     * Le jeton d’authentification si on est authentifié, <code>null</code> sinon
     */
    private ZimbraAuthToken zimbraAuthToken;

    private final Set<Consumer<SessionStatus>> sessionListeners = new LinkedHashSet<>();

    /**
     * L’identifiant qui a été utilisé pour réalisé la dernière authentification réussie.
     */
    private String login;

    public ZimbraSessionManager(ZimbraService zimbraService) {
        this.zimbraService = zimbraService;
    }

    public SessionStatus getSessionStatus() {
        return zimbraAuthToken == null ? SessionStatus.DISCONNECTED : SessionStatus.CONNECTED;
    }

    public ZimbraAuthToken getZimbraAuthToken() {
        Preconditions.checkState(getSessionStatus() == SessionStatus.CONNECTED);
        return zimbraAuthToken;
    }

    public void login(String username, String password) throws ArchivageZimbraException {
        LOGGER.debug("tentative de connexion avec l’identifiant " + username);
        ZimbraAuthToken newZimbraAuthToken = zimbraService.login(username, password);
        LOGGER.debug("connexion avec l’identifiant " + username + " réussie");
        TimerTask logoutTask = new TimerTask() {
            @Override
            public void run() {
                logout();
            }
        };
        Duration durationToWaitBeforeExpiration = newZimbraAuthToken.lifetime().minus(Duration.ofMinutes(10));
        timer.schedule(logoutTask, durationToWaitBeforeExpiration.toMillis());
        zimbraAuthToken = newZimbraAuthToken;
        login = username;
        notifySessionStatusChange(SessionStatus.CONNECTED);
    }

    private void notifySessionStatusChange(SessionStatus sessionStatus) {
        sessionListeners.forEach(sessionStatusConsumer -> sessionStatusConsumer.accept(sessionStatus));
    }

    /**
     * Vérifie que la session est toujours OK.
     *
     * Devrait être appelée dès qu’on détecte que l’utilisateur s’apprête à utiliser une fonctionnalité
     * qui nécessite session ouverte. On vérifie qu’elle est toujours valable et sinon, on déconnecte.
     */
    public void updateStatus() throws ArchivageZimbraException {
        if (getSessionStatus() == SessionStatus.CONNECTED) {
            SessionStatus upToDateSessionStatus = zimbraService.postNoOpRequest(zimbraAuthToken);
            if (upToDateSessionStatus == SessionStatus.CONNECTED) {
                // la session était valide, elle l’est toujours. Rien à faire.
            } else {
                // la session était valide, elle ne l’est plus.
                logout();
            }
        } else {
            // on était déconnecté, on l’est toujours. Pas de raison que l’état change.
        }
    }

    public void logout() {
        login = null;
        zimbraAuthToken = null;
        notifySessionStatusChange(SessionStatus.DISCONNECTED);
    }

    public void addSessionListener(Consumer<SessionStatus> sessionListener) {
        sessionListeners.add(sessionListener);
    }

    public String getLogin() {
        return login;
    }
}
