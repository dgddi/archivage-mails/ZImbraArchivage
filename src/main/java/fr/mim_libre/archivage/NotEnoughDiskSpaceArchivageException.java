package fr.mim_libre.archivage;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * Erreur levée quand on manque d’espace-disque local.
 */
public class NotEnoughDiskSpaceArchivageException extends ArchivageException {

    private final File file;

    private final long availableSpace;

    private final long requiredSpace;

    public NotEnoughDiskSpaceArchivageException(File file, long availableSpace, long requiredSpace) {
        super(
                "L’espace disque disponible dans %s (%s) n’est pas suffisant pour y écrire %s"
                        .formatted(
                                file,
                                FileUtils.byteCountToDisplaySize(availableSpace),
                                FileUtils.byteCountToDisplaySize(requiredSpace)
                        )
        );
        this.file = file;
        this.availableSpace = availableSpace;
        this.requiredSpace = requiredSpace;
    }

    public File getFile() {
        return file;
    }

    public long getAvailableSpace() {
        return availableSpace;
    }

    public long getRequiredSpace() {
        return requiredSpace;
    }
}
