package fr.mim_libre.archivage;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import fr.mim_libre.archivage.configuration.ConfigurationManager;
import fr.mim_libre.archivage.index.ArchivageIndex;
import fr.mim_libre.archivage.index.ArchivageQuery;
import fr.mim_libre.archivage.index.IndexSearchResult;
import fr.mim_libre.archivage.index.IndexationRequest;
import fr.mim_libre.archivage.index.MissingRelevantKeywordException;
import fr.mim_libre.archivage.logging.ArchivageLogger;
import fr.mim_libre.archivage.store.ArchivageStoreException;
import fr.mim_libre.archivage.store.EmlFilePath;
import fr.mim_libre.archivage.store.EmlFileVisitor;
import fr.mim_libre.archivage.store.ErrorReadingEmlFileException;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.store.Store;
import fr.mim_libre.archivage.store.StoreScanProgressHandler;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import fr.mim_libre.archivage.zimbra.ZimbraAuthToken;
import fr.mim_libre.archivage.zimbra.ZimbraService;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Supplier;

/**
 * Service qui va organiser les opérations métiers en appelant {@link Store}, {@link ArchivageIndex}, {@link ArchivageLogger}, {@link ZimbraService}.
 */
public class ArchivageService {

    private static final Logger LOGGER = LogManager.getLogger(ArchivageService.class);

    private final ZimbraService zimbraService;

    private final Store store;

    private final ArchivageIndex archivageIndex;

    private final ArchivageLogger archivageLogger;

    private final ConfigurationManager configurationManager;

    private final ArchivageConfiguration configuration;

    public ArchivageService(ZimbraService zimbraService, Store store, ArchivageIndex archivageIndex, ArchivageLogger archivageLogger, ConfigurationManager configurationManager, ArchivageConfiguration configuration) {
        this.zimbraService = zimbraService;
        this.store = store;
        this.archivageIndex = archivageIndex;
        this.archivageLogger = archivageLogger;
        this.configurationManager = configurationManager;
        this.configuration = configuration;
    }

    /**
     * Synchronisation : téléchargement en masse de courriels depuis Zimbra et ajout l’index + journalisation.
     *
     * @param zimbraAuthToken le jeton d’authentification
     * @param queries les requêtes pour trouver les courriels à archiver
     * @param targetMountPoint le point de montage vers lequel il faut enregistrer les courriels
     * @param synchronisationProgressHandler de quoi permettre à l’appelant de suivre le déroulement
     * @param isCancelled pour savoir si l’utilisateur a demandé d’interrompre la synchro pendant son déroulement
     */
    void download(ZimbraAuthToken zimbraAuthToken, Set<ZimbraService.SearchRequest.Query> queries, MountPoint targetMountPoint, SynchronisationProgressHandler synchronisationProgressHandler, Supplier<Boolean> isCancelled) throws NotEnoughDiskSpaceArchivageException, ArchivageZimbraException {
        synchronisationProgressHandler.synchronisationPlanComputationStarted();
        Set<DownloadTask> downloadTasks = computeSynchronisationPlan(zimbraAuthToken, queries, targetMountPoint, isCancelled, synchronisationProgressHandler);
        long sizeToBeDownloaded = downloadTasks.stream()
                .mapToLong(DownloadTask::size)
                .sum();
        store.checkSpaceIsAvailable(targetMountPoint, sizeToBeDownloaded);
        synchronisationProgressHandler.synchronisationPlanComputed(downloadTasks.size(), sizeToBeDownloaded);
        for (DownloadTask downloadTask : downloadTasks) {
            if (isCancelled.get()) {
                break;
            }
            String id = downloadTask.id();
            EmlFilePath emlFilePath = downloadTask.emlFilePath();
            store.ensureDirectoryExists(emlFilePath.folder());
            if (emlFilePath.toFile().exists()) {
                archivageLogger.onEmailAlreadyExistsWhileSyncing(emlFilePath);
                synchronisationProgressHandler.onErrorOccurred();
                if (configuration.isDeletionOnSynchronizationEnabled()) {
                    zimbraService.postDeleteMsgActionRequest(zimbraAuthToken, id);
                }
            } else {
                try (InputStream inputStream = zimbraService.downloadEml(zimbraAuthToken, id)) {
                    store.write(emlFilePath, inputStream);
                    try (ArchivageIndex.Writer writer = archivageIndex.openWriter()) {
                        ArchivageEmail archivageEmail = store.readEmlFile(emlFilePath);
                        writer.add(archivageEmail);
                        archivageLogger.onEmailArchived(archivageEmail);
                        if (configuration.isDeletionOnSynchronizationEnabled()) {
                            zimbraService.postDeleteMsgActionRequest(zimbraAuthToken, id);
                        }
                    } catch (ErrorReadingEmlFileException e) {
                        archivageLogger.onErrorWhileReadingEmlFile(emlFilePath, e);
                        synchronisationProgressHandler.onErrorOccurred();
                        zimbraService.postTagMsgActionRequest(
                                zimbraAuthToken,
                                id,
                                configuration.getNonArchivableEmailTag()
                        );
                    }
                } catch (IOException | ArchivageZimbraException | ArchivageStoreException e) {
                    archivageLogger.onSynchronisationError(e.getMessage());
                    synchronisationProgressHandler.onErrorOccurred();
                    LOGGER.warn("erreur pendant la synchronisation", e);
                }
            }
            synchronisationProgressHandler.done(downloadTask);
        }
        if (
            !isCancelled.get() && isSomeEmailsAreRemainingAfterSynchronization(
                zimbraAuthToken, queries, synchronisationProgressHandler
            )
        ) {
            LOGGER.warn("on a trouvé de nouveaux messages à télécharger à la fin de la synchronisation, on boucle");
            download(zimbraAuthToken,
                    queries,
                    targetMountPoint,
                    synchronisationProgressHandler,
                    isCancelled);
        }
    }

    /**
     * À la fin d’une synchronisation, il arrive qu’il reste encore des messages que Zimbra n’avait pas remonté
     * pendant le calcul du plan initial. On relance donc les requêtes initiales pour voir s’il reste encore
     * des messages.
     */
    private boolean isSomeEmailsAreRemainingAfterSynchronization(
            ZimbraAuthToken zimbraAuthToken,
            Set<ZimbraService.SearchRequest.Query> queries,
            SynchronisationProgressHandler synchronisationProgressHandler)
            throws ArchivageZimbraException {
        ZonedDateTime now = configuration.now();

        ZimbraService.Folder zimbraRootFolder =
            zimbraService.postGetFolderRequest(zimbraAuthToken);
        Set<ZimbraService.Folder> allFolders = filterFoldersWithValidPath(
            flatFolders(zimbraRootFolder), synchronisationProgressHandler
        );
        ImmutableMap<String, ZimbraService.Folder> folderByIds =
            Maps.uniqueIndex(allFolders, ZimbraService.Folder::id);

        int batchSize = configuration.getSynchronisationBatchSize();

        for (ZimbraService.SearchRequest.Query query : queries) {
            ZimbraService.SearchRequest searchRequest =
                ZimbraService.SearchRequest.forQuery(batchSize, 0, query);
            ZimbraService.SearchResponse searchResponse =
                zimbraService.postSearchRequest(zimbraAuthToken, searchRequest);

            if (searchResponse.ms() == null || searchResponse.ms().isEmpty()) {
                continue;
            }

            for (ZimbraService.SearchResponse.M m : searchResponse.ms()) {
                Duration duration = query.olderThanDuration();
                Date date = new Date(m.d());

                if (duration != null && !date.before(Date.from(now.minus(duration).toInstant()))) {
                    continue;
                }

                if (folderByIds.containsKey(m.l())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Recherche sur Zimbra les messages à archiver et calcul une liste de tâches.
     *
     * Notamment, gère qu’un même message qui correspond à plusieurs critères ne soit télécharger qu’une fois.
     */
    private Set<DownloadTask> computeSynchronisationPlan(ZimbraAuthToken zimbraAuthToken, Set<ZimbraService.SearchRequest.Query> queries, MountPoint targetMountPoint, Supplier<Boolean> isCancelled, SynchronisationProgressHandler synchronisationProgressHandler) throws ArchivageZimbraException {
        ZonedDateTime now = configuration.now();

        ZimbraService.Folder zimbraRootFolder = zimbraService.postGetFolderRequest(zimbraAuthToken);
        Set<ZimbraService.Folder> allFolders = filterFoldersWithValidPath(flatFolders(zimbraRootFolder), synchronisationProgressHandler);
        ImmutableMap<String, ZimbraService.Folder> folderByIds = Maps.uniqueIndex(allFolders, ZimbraService.Folder::id);
        Set<DownloadTask> downloadTasks = new TreeSet<>(Comparator.comparing(DownloadTask::id));
        int batchSize = configuration.getSynchronisationBatchSize();

        for (ZimbraService.SearchRequest.Query query : queries) {
            Duration duration = query.olderThanDuration();
            boolean fetchMore;
            int offset = 0;

            if (duration != null) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(now.getZone());
                String date = formatter.format(now.toInstant().minus(duration));

                LOGGER.info("Vous avez lancé l'archivage des emails datés de plus de {} jours ({}).", duration.toDays(), date);
            }

            do {
                ZimbraService.SearchRequest searchRequest = ZimbraService.SearchRequest.forQuery(batchSize, offset, query);
                ZimbraService.SearchResponse searchResponse = zimbraService.postSearchRequest(zimbraAuthToken, searchRequest);
                if (searchResponse.ms() != null) {
                    for (ZimbraService.SearchResponse.M m : searchResponse.ms()) {
                        int size = m.s();
                        String id = m.id();
                        String subject = m.su();
                        Date date = new Date(m.d());
                        String l = m.l();
                        ZimbraService.Folder zimbraFolder = folderByIds.get(l);

                        if (zimbraFolder == null) continue;

                        if (duration != null && !date.before(Date.from(now.minus(duration).toInstant()))) {
                            LOGGER.warn("Le message {} n'est daté de plus de {} jours.", id, duration.toDays());
                            continue;
                        }

                        Path relativeFolderPath = Path.of(zimbraRootFolder.absFolderPath()).relativize(Path.of(zimbraFolder.absFolderPath()));
                        EmlFilePath emlFilePath = targetMountPoint.folder(relativeFolderPath).getEmlFilePath(id, date, subject, configuration.getMaxFileNameLength());
                        DownloadTask downloadTask = new DownloadTask(id, emlFilePath, size);
                        downloadTasks.add(downloadTask);
                    }
                }
                boolean hasMore = searchResponse.more() == 1;
                offset += batchSize;
                fetchMore = hasMore && !isCancelled.get();
            } while (fetchMore);
        }
        Set<DownloadTask> result;
        if (isCancelled.get()) {
            LOGGER.debug("annulation, on retourne un plan vide");
            result = Collections.emptySet();
        } else {
            LOGGER.debug("va télécharger " + downloadTasks.size() + " messages pour " + queries);
            result = downloadTasks;
        }
        return result;
    }

    public Set<ZimbraService.Folder> filterFoldersWithValidPath(Set<ZimbraService.Folder> folders, SynchronisationProgressHandler synchronisationProgressHandler) {
        Set<ZimbraService.Folder> foldersWithValidPath = new LinkedHashSet<>();

        if (folders != null) {
            folders.forEach(folder -> {
                try {
                    Path.of(folder.absFolderPath());
                    foldersWithValidPath.add(folder);
                } catch (InvalidPathException e) {
                    archivageLogger.onInvalidFolderPath(folder.absFolderPath());

                    LOGGER.warn(
                        "le chemin du dossier « " +
                        Objects.requireNonNullElse(folder.absFolderPath(), "") +
                        " » n'est pas compatible avec ce système d'exploitation."
                    );

                    if (synchronisationProgressHandler != null) {
                        synchronisationProgressHandler
                            .registerFolderWithInvalidPath(folder);
                    }
                }
            });
        }

        return foldersWithValidPath;
    }

    /**
     * Réindexation d’un point de montage. Écrase l’index qui existe déjà.
     */
    public void indexMountPoints(IndexationRequest indexationRequest, StoreScanProgressHandler storeScanProgressHandler) {
        LOGGER.info("va traiter " + indexationRequest);
        Set<MountPoint> mountPoints = indexationRequest.mountPoints();
        archivageLogger.onRootScanEntry(mountPoints);
        if (indexationRequest.cleaningStrategy() == IndexationRequest.CleaningStrategy.DELETE_FULL_INDEX) {
            archivageIndex.delete();
        }
        try (ArchivageIndex.Writer writer = archivageIndex.openWriter()) {
            EmlFileVisitor emlFileVisitor = new EmlFileVisitor() {
                @Override
                public void onEmlFileReadSuccess(ArchivageEmail archivageEmail) {
                    writer.add(archivageEmail);
                }

                @Override
                public void onEmlFileReadFailure(EmlFilePath emlFilePath, ErrorReadingEmlFileException errorReadingEmlFileException) {
                    archivageLogger.onErrorWhileReadingEmlFile(emlFilePath, errorReadingEmlFileException);
                }
            };
            if (indexationRequest.cleaningStrategy() == IndexationRequest.CleaningStrategy.REMOVE_EACH_MOUNT_POINT) {
                mountPoints.forEach(writer::delete);
            }
            store.scanPathAndConsumeEmlFiles(mountPoints, emlFileVisitor, storeScanProgressHandler);
        }
        archivageLogger.onRootScanExit(mountPoints);
        LOGGER.info("a traité " + indexationRequest);
    }

    public void removeMountPoint(MountPoint mountPoint) {
        try (ArchivageIndex.Writer writer = archivageIndex.openWriter()) {
            writer.delete(mountPoint);
        }
        archivageLogger.onMountPointRemoved(mountPoint);
    }

    /**
     * Recherche dans l’index pour trouver tous les courriels qui correspondent à l'ArchivageQuery passée.
     */
    public List<IndexSearchResult> browse(ArchivageQuery archivageQuery) throws MissingRelevantKeywordException {
        return archivageIndex.search(archivageQuery);
    }

    /**
     * Pour un {@link ZimbraService.Folder}, calcule tous ses descendants pour reconstituer l’arborescence.
     */
    private Set<ZimbraService.Folder> flatFolders(ZimbraService.Folder folder) {
        Set<ZimbraService.Folder> allFolders = new LinkedHashSet<>();
        if (folder.children() != null) {
            for (ZimbraService.Folder child : folder.children()) {
                Set<ZimbraService.Folder> grandChildren = flatFolders(child);
                allFolders.addAll(grandChildren);
            }
        }
        allFolders.add(folder);
        return allFolders;
    }

    /**
     * Lit un message dans le {@link Store}.
     */
    public ArchivageEmail readEmail(EmlFilePath emlFilePath) throws ErrorReadingEmlFileException {
        return store.readEmlFile(emlFilePath);
    }

    /**
     * Restauration d’un courriel local vers la Zimbra.
     */
    public void restore(ZimbraAuthToken zimbraAuthToken, EmlFilePath emlFilePath) throws ArchivageZimbraException, ArchivageStoreException {
        LOGGER.info("va restaurer " + emlFilePath);
        final String content;
        try {
            content = store.readEmlFileAsEmlString(emlFilePath);
        } catch (ArchivageStoreException e) {
            archivageLogger.onEmailRestorationFailure(emlFilePath, e);
            throw e;
        }
        try {
            String messageId = zimbraService.postAddMsgRequest(
                    zimbraAuthToken,
                    "/" + configuration.getRestorationTargetFolderName(),
                    content
            );
            LOGGER.info("a restauré " + emlFilePath + ", id attribué = " + messageId);
        } catch (ArchivageZimbraException e) {
            archivageLogger.onEmailRestorationFailure(emlFilePath, e);
            throw e;
        }
        try {
            ArchivageEmail archivageEmail = store.readEmlFile(emlFilePath);
            archivageLogger.onEmailRestored(archivageEmail);
        } catch (ErrorReadingEmlFileException e) {
            LOGGER.warn("échec de la lecture de " + emlFilePath + " alors qu’on a réussi la restauration. La restauration ne sera pas inscrite au journal utilisateur");
        }
    }

    /**
     * Enregistre une pièce jointe donnée d’un courriel local vers un fichier.
     */
    public void saveAttachment(ArchivageEmail archivageEmail, ArchivageEmail.Attachment attachment, Path path) {
        try (InputStream inputStream = attachment.dataSource().getInputStream()) {
            try (OutputStream outputStream = Files.newOutputStream(path, StandardOpenOption.CREATE_NEW)) {
                IOUtils.copy(inputStream, outputStream);
            } catch (IOException e) {
                throw new RuntimeException("impossible d’écrire le fichier " + path, e);
            }
        } catch (IOException e) {
            throw new RuntimeException("ne peut pas lire le contenu de la pièce jointe " + attachment.name() + " du fichier " + archivageEmail.emlFilePath(), e);
        }
        archivageLogger.onAttachmentSaved(archivageEmail, path);
    }

    /**
     * Lance une synchronisation.
     */
    public void startSynchronisation(ZimbraAuthToken zimbraAuthToken,
                                     Set<ZimbraService.SearchRequest.Query> queries,
                                     MountPoint targetMountPoint,
                                     String profileId,
                                     SynchronisationProgressHandler synchronisationProgressHandler,
                                     Supplier<Boolean> isCancelled) {
        try {
            ZonedDateTime start = configuration.now();
            download(zimbraAuthToken, queries, targetMountPoint, synchronisationProgressHandler, isCancelled);
            configurationManager.setLastSynchronisation(profileId, start);
            synchronisationProgressHandler.terminated(start);
        } catch (ArchivageException e) {
            archivageLogger.onCancelledSynchronisation(e);
            synchronisationProgressHandler.onCancelledSynchronisation(e);
        }
    }

    public void setUpZimbra(ZimbraAuthToken zimbraAuthToken) throws ArchivageZimbraException {
        zimbraService.postCreateFolderRequest(zimbraAuthToken, configuration.getRestorationTargetFolderName());
        createArchivageTagsIfNecessary(zimbraAuthToken);
    }

    /**
     * Création du tag Archiver dans le compte Zimbra, dans le cas où il n'existe pas.
     */
    private void createArchivageTagsIfNecessary(ZimbraAuthToken zimbraAuthToken) throws ArchivageZimbraException {
        Set<String> requiredTags = Set.of(
                configuration.getEmailToBeArchivedTag(),
                configuration.getEmailToIgnoreWhileArchivingTag(),
                configuration.getNonArchivableEmailTag()
        );
        ImmutableSet<String> existingTags;
        try {
            existingTags = zimbraService.postGetTagsRequest(zimbraAuthToken);
        } catch (ArchivageZimbraException e) {
            throw new ArchivageZimbraException("impossible de télécharger la liste des tags depuis Zimbra", e);
        }
        Set<String> toCreateTags = Sets.difference(requiredTags, existingTags);
        for (String tagToCreate : toCreateTags) {
            LOGGER.info("Création du tag {}", tagToCreate);
            try {
                zimbraService.postCreateTagRequest(zimbraAuthToken, tagToCreate);
            } catch (ArchivageZimbraException e) {
                throw new ArchivageZimbraException("impossible de créer le tag " + tagToCreate + " sur Zimbra", e);
            }
        }
    }

    /**
     * Enregistrer toutes les pièces jointes d’un courriel
     *
     * @param archivageEmail le courriel
     * @param destinationPath le répertoire où il faut enregistrer tous les fichiers
     */
    public void saveAllAttachments(ArchivageEmail archivageEmail, Path destinationPath) {
        archivageEmail.attachments().forEach(attachment -> {
            Path attachmentPath = destinationPath.resolve(attachment.name());
            if (attachmentPath.toFile().exists()) {
                LOGGER.warn("on a pas réécrit " + attachmentPath + " car il existe déjà au moment d’enregistrer toutes les pièces jointes de " + archivageEmail.emlFilePath());
            } else {
                saveAttachment(archivageEmail, attachment, attachmentPath);
            }
        });
    }

    public void cleanLogs() {
        archivageLogger.clean();
    }

    /**
     * Permet de suivre la progression d’une synchronisation, les méthodes sont appelées aux différentes étapes.
     */
    public interface SynchronisationProgressHandler {

        void synchronisationPlanComputationStarted();

        void synchronisationPlanComputed(int toBeDone, long sizeToBeDownloaded);

        void done(DownloadTask downloadTask);

        void terminated(ZonedDateTime start);

        void onErrorOccurred();

        void onCancelledSynchronisation(ArchivageException e);

        void registerFolderWithInvalidPath(ZimbraService.Folder folder);
    }

    /**
     * Tâche de téléchargement d’un courriel sur Zimbra
     *
     * @param id l’identifiant technique sur Zimbra
     * @param emlFilePath le chemin destination où il faut enregistrer le message
     * @param size la taille du téléchargement
     */
    public record DownloadTask(String id, EmlFilePath emlFilePath, int size) {

    }

    public Set<Folder> getFolders(MountPoint mountPoint) throws ArchivageStoreException {
        return store.getExistingFolders(mountPoint);
    }
}
