package fr.mim_libre.archivage.zimbra;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import fr.mim_libre.archivage.SessionStatus;
import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service pour échanger avec le serveur Zimbra : apporte de la sémantique sur les échanges SOAP et ReST.
 */
public class ZimbraService {

    private static final Logger LOGGER = LogManager.getLogger(ZimbraService.class);

    /**
     * Pour accéder à Zimbra via SOAP
     */
    private final Soap soap;

    /**
     * Pour accéder à Zimbra via ReST
     */
    private final Rest rest;

    private final ArchivageConfiguration configuration;

    public ZimbraService(ArchivageConfiguration configuration) {
        this.soap = new Soap(configuration);
        this.rest = new Rest(configuration);
        this.configuration = configuration;
    }


    /**
     * S’authentifier sur Zimbra avec les identifiants passés.
     */
    public ZimbraAuthToken login(String username, String password) throws ArchivageZimbraException {
        String body = """
                <AuthRequest xmlns='urn:zimbraAccount'>
                  <account by='name'>%s</account>
                  <password><![CDATA[%s]]></password>
                </AuthRequest>
                """;
        String innerBody = body.formatted(username, password);
        try {
            SoapResponse soapResponse = soap.newZimbraQuery()
                    .withoutAuthentication()
                    .bodyIsEnvelopeContent(innerBody);
            switch (soapResponse.getStatusCode()) {
                case HttpURLConnection.HTTP_OK -> {
                    String authToken = soapResponse.getString("Envelope/Body/AuthResponse/authToken");
                    String lifetime = soapResponse.getString("Envelope/Body/AuthResponse/lifetime");
                    return new ZimbraAuthToken(authToken, Duration.ofMillis(Long.parseLong(lifetime)));
                }
                case HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                    String host = configuration.getZimbraSoapUri().getHost();
                    String faultReasonText = soapResponse.getString("Envelope/Body/Fault/Reason/Text");
                    String faultDetailErrorCode = soapResponse.getString("Envelope/Body/Fault/Detail/Error/Code");
                    throw new AuthenticationFailedArchivageException(faultReasonText, faultDetailErrorCode, host);
                }
                default -> throw new IllegalStateException("code erreur " + soapResponse.getStatusCode());
            }
        } catch (IOException | InterruptedException | NoSuchAlgorithmException | KeyManagementException e) {
            throw new ArchivageZimbraException("Erreur de connexion avec Zimbra", e);
        }
    }

    /**
     * Récupère tous les dossiers existants dans la boîte distante.
     *
     * En fait, on remonte le dossier racine, il faut aller regarder les enfants.
     */
    public Folder postGetFolderRequest(ZimbraAuthToken zimbraAuthToken) throws ArchivageZimbraException {
        String body = """
                <GetFolderRequest xmlns='urn:zimbraMail' />
                """;
        SoapResponse soapResponse = doSoap(zimbraAuthToken, body);

        // On doit ignorer les balises link et search
        soapResponse.removeNodesWithTags(List.of("link", "search"));

        // Récupération du dossier racine de la boîte mail, qui contient donc tous les autres dossiers.
        Folder folder = soapResponse.deserialize("Envelope/Body/GetFolderResponse/folder", Folder.class);
        return folder;
    }

    /**
     * Cherche la liste de tous les tags du compte Zimbra.
     *
     * @param zimbraAuthToken le token d'authentification zimbra
     * @return les tags existants sur Zimbra
     */
    public ImmutableSet<String> postGetTagsRequest(ZimbraAuthToken zimbraAuthToken) throws ArchivageZimbraException {
        String body = """
                <GetTagRequest xmlns="urn:zimbraMail" />
                """;
        SoapResponse soapResponse = doSoap(zimbraAuthToken, body);
        GetTagResponse getTagResponse = soapResponse.deserialize("Envelope/Body/GetTagResponse", GetTagResponse.class);
        List<Tag> tags = getTagResponse.tags();
        if (tags == null) {
            tags = new ArrayList<Tag>();
        }
        ImmutableSet<String> existingTags = tags.stream()
                .map(Tag::name)
                .collect(ImmutableSortedSet.toImmutableSortedSet(Comparator.naturalOrder()));
        return existingTags;
    }

    /**
     * Crée un tag dans le compte Zimbra.
     * @param zimbraAuthToken le token d'authentification zimbra
     * @param tagName le nom du tag à créer
     */
    public void postCreateTagRequest(ZimbraAuthToken zimbraAuthToken, String tagName) throws ArchivageZimbraException {
        String body = """
                <CreateTagRequest xmlns="urn:zimbraMail">
                    <tag name="%s" />
                </CreateTagRequest>
                """.formatted(tagName);
        SoapResponse soapResponse = doSoap(zimbraAuthToken, body);
        if (soapResponse.getStatusCode() == HttpURLConnection.HTTP_OK) {
            CreateTagResponse createTagResponse = soapResponse.deserialize("Envelope/Body/CreateTagResponse", CreateTagResponse.class);
            if (createTagResponse.tag() == null) {
                LOGGER.error("Erreur lors de la création du tag {} : {}", tagName, soapResponse);
                throw new ArchivageZimbraException("Erreur lors de la création du tag " + tagName);
            }
        } else {
            LOGGER.error("Erreur lors de la création du tag {} : {}", tagName, soapResponse);
            String errorMessage = soapResponse.getString("Envelope/Body/Fault/Reason/Text");
            throw new ArchivageZimbraException("Erreur lors de la création du tag " + tagName + ". Message du serveur : " + errorMessage);
        }
    }

    /**
     * Recherche de courriels selon les filtres passés.
     */
    public SearchResponse postSearchRequest(ZimbraAuthToken zimbraAuthToken, SearchRequest searchRequest) throws ArchivageZimbraException {
        String bodyQuery = toSearchRequestQueryString(searchRequest.query());
        String body = """
                <SearchRequest xmlns='urn:zimbraMail' types='message' limit='%d' offset='%d'>
                    <query>%s</query>
                    <locale>fr_FR</locale>
                </SearchRequest>
                """.formatted(searchRequest.limit(), searchRequest.offset(), bodyQuery);
        LOGGER.debug("SearchRequest = " + body);
        SoapResponse soapResponse = doSoap(zimbraAuthToken, body);
        LOGGER.debug("SearchResponse = " + soapResponse);
        return soapResponse.deserialize("Envelope/Body/SearchResponse", SearchResponse.class);
    }

    /**
     * Effectuer une action parmi {@link MsgActionRequestOperation} sur un message donné.
     */
    private String postMsgActionRequest(ZimbraAuthToken zimbraAuthToken, MsgActionRequestParameter msgActionRequestParameter) throws ArchivageZimbraException {
        String envelopeContent = """
                <MsgActionRequest xmlns='urn:zimbraMail'>
                    <action id='%s' op='%s'>%s</action>
                </MsgActionRequest>""".formatted(
                        msgActionRequestParameter.id(),
                        msgActionRequestParameter.operation().name(),
                        msgActionRequestParameter.body().orElse(""));
        SoapResponse soapResponse = doSoap(zimbraAuthToken, envelopeContent);
        switch (soapResponse.getStatusCode()) {
            case HttpURLConnection.HTTP_OK -> {
                String successId = soapResponse.getString("Envelope/Body/MsgActionResponse/action/@id");
                Preconditions.checkState(!Strings.isNullOrEmpty(successId), "Aucun identifiant suite à l'opération " + msgActionRequestParameter.operation() + " sur le message " + msgActionRequestParameter.id());
                return successId;
            }
            case HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                String errorMessage = soapResponse.getString("Envelope/Body/Fault/Reason/Text");
                String errorCode = soapResponse.getString("Envelope/Body/Fault/Detail/Error/Code");
                throw new ArchivageZimbraException("Erreur pendant l’opération " + msgActionRequestParameter.operation() + " sur le message " + msgActionRequestParameter.id() + ". Message d’erreur : " + errorMessage + ". Code erreur : " + errorCode);
            }
            default -> throw new IllegalStateException("code de retour inattendu à l’opération " + msgActionRequestParameter + ". Réponse HTTP : " + soapResponse);
        }
    }

    /**
     * Poser un tag sur le message passé.
     */
    public void postTagMsgActionRequest(ZimbraAuthToken zimbraAuthToken, String id, String tag) throws ArchivageZimbraException {
        LOGGER.info("va tagguer le message " + id + " avec " + tag);
        MsgActionRequestParameter msgActionRequestParameter =
                new MsgActionRequestParameter(
                        id,
                        MsgActionRequestOperation.tag,
                        Optional.of("<tn>%s</tn>".formatted(tag))
                );
        String successId = postMsgActionRequest(zimbraAuthToken, msgActionRequestParameter);
        LOGGER.info("message " + msgActionRequestParameter.id() + " taggué " + tag + " avec succès " + successId);
    }

    /**
     * Supprimer un message de la boîte.
     */
    public void postDeleteMsgActionRequest(ZimbraAuthToken zimbraAuthToken, String id) throws ArchivageZimbraException {
        LOGGER.debug("va supprimer le message " + id);
        MsgActionRequestParameter msgActionRequestParameter =
                new MsgActionRequestParameter(
                        id,
                        MsgActionRequestOperation.delete,
                        Optional.empty()
                );
        String successId = postMsgActionRequest(zimbraAuthToken, msgActionRequestParameter);
        LOGGER.info("message " + msgActionRequestParameter.id() + " supprimé avec succès " + successId);
    }

    /**
     * Permet de faire un essai d’opération SOAP à vide et donc de vérifier que le jeton passé est toujours valide.
     *
     * @return {@link SessionStatus#CONNECTED} si le jeton est valide, {@link SessionStatus#DISCONNECTED} s’il ne
     * l’est plus ou a expiré.
     */
    public SessionStatus postNoOpRequest(ZimbraAuthToken zimbraAuthToken) throws ArchivageZimbraException {
        String envelopeContent = """
                <NoOpRequest xmlns="urn:zimbraMail"/>
                """;
        SoapResponse soapResponse = doSoap(zimbraAuthToken, envelopeContent);
        SessionStatus sessionStatus;
        switch (soapResponse.getStatusCode()) {
            case HttpURLConnection.HTTP_OK -> sessionStatus = SessionStatus.CONNECTED;
            case HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                String errorMessage = soapResponse.getString("Envelope/Body/Fault/Reason/Text");
                String errorCode = soapResponse.getString("Envelope/Body/Fault/Detail/Error/Code");
                boolean tokenIsWrong = "service.AUTH_REQUIRED".equals(errorCode)
                                    && "no valid authtoken present".equals(errorMessage);
                boolean tokenIsExpired = "service.AUTH_EXPIRED".equals(errorCode)
                                      && "auth credentials have expired".equals(errorMessage);
                if (tokenIsWrong || tokenIsExpired) {
                    sessionStatus = SessionStatus.DISCONNECTED;
                } else {
                    throw new IllegalStateException("Code de retour " + HttpURLConnection.HTTP_INTERNAL_ERROR + " sur NoOpRequest mais réponse inattendue " + soapResponse);
                }
            }
            default -> throw new IllegalStateException("code de retour inattendu pour NoOpRequest : " + soapResponse);
        }
        return sessionStatus;
    }

    /**
     * Les différentes opérations possibles pour une commande SOAP <a href="https://files.zimbra.com/docs/soap_api/8.8.15/api-reference/zimbraMail/MsgAction.html">MsgAction</a>
     */
    private enum MsgActionRequestOperation {
        delete,
        read,
        flag,
        tag,
        move,
        update,
        spam,
        trash
    }

    /**
     * Envelopper la requête SOAP passée puis l’envoyer au serveur pour obtenir la réponse.
     */
    private SoapResponse doSoap(ZimbraAuthToken zimbraAuthToken, String envelopeContent) throws ArchivageZimbraException {
        try {
            return soap.newZimbraQuery()
                    .withAuthentication(zimbraAuthToken)
                    .bodyIsEnvelopeContent(envelopeContent);
        } catch (IOException | InterruptedException | NoSuchAlgorithmException | KeyManagementException e) {
            throw new ArchivageZimbraException("Erreur de connexion avec Zimbra", e);
        }
    }

    /**
     * Créer un dossier dans la boîte distante.
     */
    public String postCreateFolderRequest(ZimbraAuthToken zimbraAuthToken, String folderName) throws ArchivageZimbraException {
        String body = """
            <CreateFolderRequest xmlns='urn:zimbraMail'>
                <folder name='%s' fie='1' l='1' view='message'/>
            </CreateFolderRequest>
            """.formatted(folderName);
        SoapResponse soapResponse = doSoap(zimbraAuthToken, body);
        switch (soapResponse.getStatusCode()) {
            case HttpURLConnection.HTTP_OK -> {
                String folderId = soapResponse.getString("Envelope/Body/CreateFolderResponse/folder/@id");
                Preconditions.checkState(!Strings.isNullOrEmpty(folderId), "Aucun identifiant renvoyé suite à la création du dossier");
                return folderId;
            }
            case HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                String errorMessage = soapResponse.getString("Envelope/Body/Fault/Reason/Text");
                String errorCode = soapResponse.getString("Envelope/Body/Fault/Detail/Error/Code");
                throw new ArchivageZimbraException("Erreur pendant la création d’un dossier " + folderName + ". Message d’erreur : " + errorMessage + ". Code erreur : " + errorCode);
            }
            default -> throw new IllegalStateException("code de retour inattendu à la création du dossier " + folderName + ". Réponse HTTP : " + soapResponse);
        }
    }

    /**
     * Transforme les filtres de recherche d’un message passé en une requête au format SOAP.
     *
     * La requête retournée suit la <a href="http://docs.zimbra.com/desktop/help/en_US/Search/query_language_description.htm">syntaxe du langage de requêtes Zimbra</a>.
     */
    private String toSearchRequestQueryString(SearchRequest.Query searchRequestQuery) {
        Set<String> queryElements = new LinkedHashSet<>();
        if (searchRequestQuery.olderThanDuration() != null) {
            ZonedDateTime now = configuration.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy").withZone(now.getZone());
            String queryElement = "before:" + formatter.format(now.toInstant().minus(searchRequestQuery.olderThanDuration()));
            queryElements.add(queryElement);
        }
        if (searchRequestQuery.inFolderName() != null) {
            queryElements.add(String.format("in:\"%s\"", searchRequestQuery.inFolderName()));
        }
        if (searchRequestQuery.inFolderId() != null) {
            queryElements.add(String.format("inid:%s", searchRequestQuery.inFolderId()));
        }
        if (searchRequestQuery.tagged() != null) {
            queryElements.add(String.format("tag:\"%s\"", searchRequestQuery.tagged()));
        }
        queryElements.add(String.format("not tag:\"%s\"", configuration.getEmailToIgnoreWhileArchivingTag()));
        queryElements.add(String.format("not tag:\"%s\"", configuration.getNonArchivableEmailTag()));
        queryElements.add(String.format("not in:\"%s\"", configuration.getRestorationTargetFolderName()));
        String bodyQuery = String.join(" and ", queryElements);
        return bodyQuery;
    }

//    public String postGetMsgRequest(ZimbraAuthToken zimbraAuthToken, String id) throws ArchivageZimbraException {
//        String body = """
//                <GetMsgRequest xmlns='urn:zimbraMail'>
//                    <m id='%s' max='1' />
//                </GetMsgRequest>""".formatted(id);
//        return doSoap(zimbraAuthToken, body)
//                .toString();
//    }

    /**
     * Télécharger un courriel qui se trouve dans la boîte distante au format eml.
     */
    public InputStream downloadEml(ZimbraAuthToken zimbraAuthToken, String id) throws ArchivageZimbraException {
        return rest.downloadEml(zimbraAuthToken, id);
    }

    /**
     * Téléverser un courriel vers la boîte Zimbra.
     *
     * @param targetFolder le dossier dans la boîte distante dans lequel téléverser le courriel
     * @param content le contenu du courriel, au format MIME RFC 822
     * @return l’identifiant technique du courriel sur Zimbra
     */
    public String postAddMsgRequest(ZimbraAuthToken zimbraAuthToken, String targetFolder, String content) throws ArchivageZimbraException {
        String body = """
            <AddMsgRequest xmlns="urn:zimbraMail">
                <m l="%s">
                    <content><![CDATA[%s]]></content>
                </m>
            </AddMsgRequest>""".formatted(targetFolder, content);
        SoapResponse soapResponse = doSoap(zimbraAuthToken, body);
        switch (soapResponse.getStatusCode()) {
            case HttpURLConnection.HTTP_OK -> {
                String messageId = soapResponse.getString("Envelope/Body/AddMsgResponse/m/@id");
                Preconditions.checkState(!Strings.isNullOrEmpty(messageId), "Aucun identifiant renvoyé suite à la restauration du message");
                return messageId;
            }
            case HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                String errorMessage = soapResponse.getString("Envelope/Body/Fault/Reason/Text");
                String errorCode = soapResponse.getString("Envelope/Body/Fault/Detail/Error/Code");
                throw new ArchivageZimbraException("Erreur pendant la restauration d’un courriel vers " + targetFolder + ". Message d’erreur : " + errorMessage + ". Code erreur : " + errorCode);
            }
            default -> throw new IllegalStateException("code de retour inattendu à la restauration d’un courriel " + soapResponse);
        }
    }

    /**
     * Une requête de recherche de messages dans une boîte Zimbra.
     *
     * @param limit pagination : taille de la page
     * @param offset pagination : index du premier élément
     * @param query les critères de recherche
     */
    public record SearchRequest(
            int limit,
            int offset,
            Query query
    ) {

        public static SearchRequest forQuery(int limit, int offset, Query query) {
            return new SearchRequest(limit, offset, query);
        }

        /**
         * Une requête de recherche de courriels dans une boîte, chaque champs correspond à un filtre (null = pas de filtrage sur ce critère)
         *
         * @param olderThanDuration les courriels plus vieux que x (jours ?)
         * @param inFolderName les courriels qui se trouvent dans le dossier ayant pour nom
         * @param inFolderId les courriels qui se trouvent dans le dossier ayant pour identifiant technique
         * @param tagged les courriels qui sont étiquettés avec l’étiquette passée
         */
        public record Query(
                Duration olderThanDuration,
                String inFolderName,
                String inFolderId,
                String tagged) {

            public static Query empty() {
                return new Query(null, null, null, null);
            }

            public static Query searchOlderThanDuration(Duration duration) {
                return new Query(duration, null, null, null);
            }

            public static Query searchInFolderName(String folderName) {
                return new Query(null, folderName, null, null);
            }

            public static Query searchInFolderId(String folderId) {
                return new Query(null, null, folderId, null);
            }

            public static Query searchTagged(String tag) {
                return new Query(null, null, null, tag);
            }
        }
    }

    public record SearchResponse(
            @JacksonXmlProperty(localName = "m")
            @JacksonXmlElementWrapper(useWrapping = false)
            List<M> ms,
            int more) {

        public record M(int s, String id, long d, String l, String su) {

        }
    }

    @JacksonXmlRootElement
    public record Folder(String absFolderPath,
                         String id,
                         String name,
                         @JacksonXmlProperty(localName = "folder")
                         @JacksonXmlElementWrapper(useWrapping = false)
                         Set<Folder> children) {
    }

    @JacksonXmlRootElement
    public record GetTagResponse(
        @JacksonXmlProperty(localName = "tag")
        @JacksonXmlElementWrapper(useWrapping = false)
        List<Tag> tags) {
    }

    @JacksonXmlRootElement
    public record CreateTagResponse(Tag tag) {
    }

    @JacksonXmlRootElement
    public record Tag(String name) {
    }

    private record MsgActionRequestParameter(String id, MsgActionRequestOperation operation, Optional<String> body) {
    }
}
