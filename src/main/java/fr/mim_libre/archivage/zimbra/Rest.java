package fr.mim_libre.archivage.zimbra;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.zip.GZIPInputStream;

/**
 * Pour interagir avec l'API ReST de Zimbra.
 */
public class Rest {

    private static final Logger LOGGER = LogManager.getLogger(Rest.class);

    private final URI zimbraRestUri;

    private final ZimbraHttpClient zimbraHttpClient = new ZimbraHttpClient();

    public Rest(ArchivageConfiguration configuration) {
        zimbraRestUri = configuration.getZimbraRestUri();
    }

    /**
     * Faire un appel <code>GET</code> et obtenir le résultat comme chaîne.
     */
    private String getString(ZimbraAuthToken zimbraAuthToken, URI uri) throws ArchivageZimbraException {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(uri)
                .setHeader("Cookie", "ZM_AUTH_TOKEN=" + zimbraAuthToken.value())
                .build();
        try {
            HttpResponse<String> response = zimbraHttpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != HttpURLConnection.HTTP_OK) {
                throw new ArchivageZimbraException("Erreur lors de l’échange avec Zimbra en appellant " + uri + ", code HTTP retourné = " + response.statusCode());
            }
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new ArchivageZimbraException("Erreur pendant le téléchargement de " + uri, e);
        }
    }

    /**
     * Faire un appel <code>GET</code> et obtenir le résultat comme flux.
     *
     * Demande de compresser la réponse HTTP si possible.
     */
    private InputStream getStream(ZimbraAuthToken zimbraAuthToken, URI uri) throws ArchivageZimbraException {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(uri)
                .setHeader("Cookie", "ZM_AUTH_TOKEN=" + zimbraAuthToken.value())
                .setHeader("Accept-Encoding", "gzip")
                .build();
        try {
            HttpResponse<InputStream> response = zimbraHttpClient.send(httpRequest, HttpResponse.BodyHandlers.ofInputStream());
            if (response.statusCode() != HttpURLConnection.HTTP_OK) {
                throw new ArchivageZimbraException("Erreur lors de l’échange avec Zimbra en appellant " + uri + ", code HTTP retourné = " + response.statusCode());
            }
            String contentEncoding = response.headers().firstValue("Content-Encoding").orElse("");
            InputStream body = response.body();
            InputStream result;
            if (contentEncoding.equals("gzip")) {
                result = new GZIPInputStream(body);
            } else {
                result = body;
            }
            return result;
        } catch (IOException | InterruptedException e) {
            throw new ArchivageZimbraException("Erreur pendant le téléchargement de " + uri, e);
        }
    }

    /**
     * Télécharger un courriel au format <code>.eml</code>
     */
    public InputStream downloadEml(ZimbraAuthToken zimbraAuthToken, String id) throws ArchivageZimbraException {
        String path = zimbraRestUri.toString() + "/home/~/";
        URI uri;
        try {
            uri = new URIBuilder(path)
                    .addParameter("id", id)
                    .build();
        } catch (URISyntaxException e) {
            throw new ArchivageZimbraException("impossible de former une URL valide avec " + path, e);
        }
        return getStream(zimbraAuthToken, uri);
    }
}
