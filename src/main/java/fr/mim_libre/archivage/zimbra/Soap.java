package fr.mim_libre.archivage.zimbra;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Service pour faire des appels SOAP sur Zimbra : gère l’authent HTTP, la génération de l’enveloppe, le timeout...
 */
class Soap {

    private static final Logger LOGGER = LogManager.getLogger(Soap.class);

    private static final Transformer ESCAPING_TRANSFORMER;

    private final URI zimbraSoapUri;

    private final ZimbraHttpClient zimbraHttpClient = new ZimbraHttpClient();

    public Soap(ArchivageConfiguration archivageConfiguration) {
        this.zimbraSoapUri = archivageConfiguration.getZimbraSoapUri();
    }

    static {
        try {
            ESCAPING_TRANSFORMER = TransformerFactory.newInstance().newTransformer();
            ESCAPING_TRANSFORMER.setOutputProperty("omit-xml-declaration", "yes");
        } catch (TransformerConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Fabriquer un Builder de requête SOAP.
     */
    AuthenticationStep newZimbraQuery() {
        return new AuthenticationStep();
    }

    class AuthenticationStep {

        /**
         * Choisir de ne pas s’authentifier.
         */
        public BodyStep withoutAuthentication() {
            return new BodyStep(null);
        }

        /**
         * S’authentifier avec le jeton passé.
         */
        public BodyStep withAuthentication(ZimbraAuthToken zimbraAuthToken) {
            return new BodyStep(zimbraAuthToken);
        }
    }

    class BodyStep {

        private final ZimbraAuthToken zimbraAuthToken;

        public BodyStep(ZimbraAuthToken zimbraAuthToken) {
            this.zimbraAuthToken = zimbraAuthToken;
        }

        /**
         * Envoyer la chaîne passée comme corps du message SOAP.
         */
        public SoapResponse bodyIsString(String body) throws IOException, InterruptedException {
            return post(zimbraAuthToken, HttpRequest.BodyPublishers.ofString(body));
        }

        /**
         * Envoyer le contenu du fichier dont le chemin est passé comme corps du message SOAP.
         */
        public SoapResponse bodyIsFile(Path path) throws IOException, InterruptedException {
            try {
                return post(zimbraAuthToken, HttpRequest.BodyPublishers.ofFile(path));
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        /**
         * Envelopper le corps de message SOAP passé puis l'envoyer comme message SOAP.
         */
        public SoapResponse bodyIsEnvelopeContent(String envelopeContent) throws IOException, InterruptedException, NoSuchAlgorithmException, KeyManagementException {
            String body = """
                    <?xml version='1.0' ?>
                    <soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope'>
                      <soap:Body>%s</soap:Body>
                    </soap:Envelope>""".formatted(envelopeContent);
            return bodyIsString(body);
        }

        /**
         * Envoyer un <code>POST</code> sur l'API SOAP.
         */
        private SoapResponse post(ZimbraAuthToken token, HttpRequest.BodyPublisher bodyPublisher) throws IOException, InterruptedException {
            HttpRequest httpRequest = toHttpPostRequest(token, bodyPublisher);
            return send(httpRequest);
        }
    }

    private HttpRequest toHttpPostRequest(ZimbraAuthToken token, HttpRequest.BodyPublisher bodyPublisher) {
        HttpRequest.Builder builder = HttpRequest.newBuilder();
        builder.POST(bodyPublisher);
        builder.uri(zimbraSoapUri);
        builder.setHeader("Content-Type", "application/soap+xml; charset=utf-8");
        if (token != null) {
            builder.setHeader("Cookie", "ZM_AUTH_TOKEN=" + token.value());
        }
        HttpRequest httpRequest = builder.build();
        return httpRequest;
    }

    private SoapResponse send(HttpRequest httpRequest) throws IOException, InterruptedException {
        HttpResponse<String> httpResponse = zimbraHttpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        SoapResponse soapResponse = SoapResponse.wrap(httpResponse);
        return soapResponse;
    }
}
