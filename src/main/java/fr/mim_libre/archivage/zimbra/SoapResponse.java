package fr.mim_libre.archivage.zimbra;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.common.base.MoreObjects;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.http.HttpResponse;
import java.util.List;

/**
 * Permet de faciliter l’interprétation d’une réponse envoyée par l’API SOAP en facilitant l’extraction du XML
 */
public class SoapResponse {

    private static final DocumentBuilder DOCUMENT_BUILDER;

    private static final XPathFactory X_PATH_FACTORY = XPathFactory.newInstance();

    /**
     * Pour transformer un document XML (en mémoire) en sa représentation textuelle.
     */
    private static final Transformer TRANSFORMER;

    /**
     * Pour désérialiser une réponse XML vers un objet.
     */
    private static final XmlMapper XML_MAPPER;

    /**
     * Le code HTTP de la réponse SOAP.
     */
    private final int statusCode;

    /**
     * Le contenu de la réponse SOAP sous forme de document XML.
     */
    private final Document document;

    private final XPath xPath;

    static {
        try {
            DOCUMENT_BUILDER = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", 2);
            TRANSFORMER = transformerFactory.newTransformer();
            TRANSFORMER.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            TRANSFORMER.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            TRANSFORMER.setOutputProperty(OutputKeys.INDENT, "yes");
        } catch (TransformerConfigurationException e) {
            throw new RuntimeException(e);
        }
        XML_MAPPER = XmlMapper.builder()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .build();
    }

    public static SoapResponse wrap(HttpResponse<String> httpResponse) {
        String xmlResponse = httpResponse.body();
        int statusCode = httpResponse.statusCode();
        return forXml(statusCode, xmlResponse);
    }

    static SoapResponse forXml(int statusCode, String xmlResponse) {
        try {
            InputSource inputSource = new InputSource(new StringReader(xmlResponse));
            Document doc = DOCUMENT_BUILDER.parse(inputSource);
            XPath xPath = X_PATH_FACTORY.newXPath();
            return new SoapResponse(statusCode, doc, xPath);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private SoapResponse(int statusCode, Document document, XPath xPath) {
        this.statusCode = statusCode;
        this.document = document;
        this.xPath = xPath;
    }

    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Extraire de la réponse XML la chaîne obtenue comme résultat de la requête XPath passée.
     */
    public String getString(String xPathExpression) {
        try {
            XPathExpression compiled = this.xPath.compile(xPathExpression);
            String str = compiled.evaluate(document);
            return str;
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Extraire de la réponse XML un nœud comme résultat de la requête XPath passée.
     */
    private Node getNode(String xPathExpression) {
        try {
            XPathExpression compiled = this.xPath.compile(xPathExpression);
            Object node = compiled.evaluate(document, XPathConstants.NODE);
            return (Node) node;
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        String unsafeXml = toXml(document);
        String safeXml = unsafeXml;
        safeXml = safeXml.replaceAll("<password>(.+?)</password>", "<password>###-###-###-###</password>");
        safeXml = safeXml.replaceAll("<authToken>(.+?)</authToken>", "<authToken>###-###-###-###</authToken>");
        return MoreObjects.toStringHelper(this)
                .add("statusCode", statusCode)
                .add("safeXml", safeXml)
                .toString();
    }

    /**
     * Convertit un nœud d’un document XML en sa représentation textuelle.
     */
    private String toXml(Node node) {
        try {
            StringWriter stringWriter = new StringWriter();
            TRANSFORMER.transform(new DOMSource(node), new StreamResult(stringWriter));
            String xml = stringWriter.toString();
            return xml;
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Dans la réponse XML, aller chercher un nœud via une requête XPath puis le désérialiser.
     */
    public <T> T deserialize(String xPathExpression, Class<T> clazz) {
        Node node = getNode(xPathExpression);
        String xml = toXml(node);
        try {
            T t = XML_MAPPER.readValue(xml, clazz);
            return t;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public void removeNodesWithTags(final List<String> tagsToRemove) {
        for (final String tagToIgnore : tagsToRemove) {
            final NodeList links = this.document.getElementsByTagName(tagToIgnore);
            final int nodesCount = links.getLength();
            for (int i = 0; i < nodesCount; i++) {
                /*
                 * L'indice est ici mis intentionnellement à 0. Quand on retire un nœud, la NodeList est mise à jour en conséquence.
                 * Si on a 2 nœuds à supprimer, lors de la première itération la liste aura 2 éléments, lors de la deuxième
                 * itération il n'y aura plus qu'un seul nœud dans la liste, donc on ne peut pas itérer comme on le ferait
                 * sur une liste normale.
                 */
                final Node node = links.item(0);
                node.getParentNode().removeChild(node);
            }
        }
    }
}
