package fr.mim_libre.archivage.zimbra;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.math.IntMath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpTimeoutException;
import java.time.Duration;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Décore {@link HttpClient} pour gérer un serveur Zimbra indisponible (retourne des 50x).
 */
class ZimbraHttpClient {

    private static final Logger LOGGER = LogManager.getLogger(ZimbraHttpClient.class);

    /**
     * Les codes de retours HTTP pour lesquels il faut considérer le serveur indisponible.
     */
    private static final Set<Integer> STATUS_CODE_THAT_SHOULD_LEAD_TO_RETRY = Set.of(
            HttpURLConnection.HTTP_BAD_GATEWAY,
            HttpURLConnection.HTTP_UNAVAILABLE,
            HttpURLConnection.HTTP_GATEWAY_TIMEOUT
    );

    private static final int MAX_TRY = 5;

    private final HttpClient httpClient;

    private static HttpClient newHttpClient() {
        return HttpClient.newBuilder()
                .connectTimeout(Duration.ofMinutes(1))
                .build();
    }

    ZimbraHttpClient() {
        this(newHttpClient());
    }

    ZimbraHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    /**
     * Voir {@link HttpClient#send(HttpRequest, HttpResponse.BodyHandler)}
     */
    <T> HttpResponse<T> send(HttpRequest request, HttpResponse.BodyHandler<T> responseBodyHandler) throws IOException, InterruptedException {
        int retried = 0;
        HttpResponse<T> httpResponse;
        do {
            if (retried > MAX_TRY) {
                throw new HttpTimeoutException("impossible d’envoyer la requête " + request + " malgré " + MAX_TRY + " essais");
            }
            httpResponse = httpClient.send(request, responseBodyHandler);
            if (retried > 0) {
                int secondsToSleep = IntMath.checkedPow(3, retried);
                LOGGER.warn("retente {} dans {} secondes après {} essais", request, secondsToSleep, retried);
                TimeUnit.SECONDS.sleep(secondsToSleep);
            }
            retried += 1;
        } while (STATUS_CODE_THAT_SHOULD_LEAD_TO_RETRY.contains(httpResponse.statusCode()));
        return httpResponse;
    }
}
