package fr.mim_libre.archivage.index;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * La façon dont il faut utiliser le mot clé saisi par l’utilisateur qui recherche des courriels.
 */
public enum SearchKeywordStrategy {

    /**
     * On ignore la recherche par mot-clé, ce n’est pas un critère retenu
     */
    IGNORED("Non filtré"),

    /**
     * Le mot-clé doit être présent dans le sujet du courriel.
     */
    SUBJECT("Sujet"),

    /**
     * Le mot-clé doit être présent dans le corps du courriel.
     */
    BODY("Corps"),

    /**
     * Le mot-clé doit être présent dans le sujet ou le corps du courriel.
     */
    SUBJECT_OR_BODY("Sujet, Corps");

    private final String label;

    SearchKeywordStrategy(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }
}
