package fr.mim_libre.archivage.index;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.mim_libre.archivage.ArchivageService;
import fr.mim_libre.archivage.store.ArchivageStoreException;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.store.StoreScanProgressHandler;
import javafx.concurrent.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Une tâche d’indexation d’un point de montage donné.
 */
public class IndexationTask extends Task<Void> implements StoreScanProgressHandler {

    private static final Logger LOGGER = LogManager.getLogger(IndexationTask.class);

    private final IndexationRequest indexationRequest;

    private final ArchivageService archivageService;

    private int numberOfEmlFilesFound;

    private int numberOfEmlFilesRead;

    private final Map<MountPoint, String> cancellationMessages = new LinkedHashMap<>();

    public IndexationTask(ArchivageService archivageService, IndexationRequest indexationRequest) {
        this.archivageService = archivageService;
        this.indexationRequest = indexationRequest;
    }

    @Override
    public void onFilesListed(int numberOfEmlFilesFound) {
        this.numberOfEmlFilesFound = numberOfEmlFilesFound;
        updateProgress(0, numberOfEmlFilesFound);
    }

    @Override
    public void onOneFileRead() {
        numberOfEmlFilesRead += 1;
        String message = "%d/%d messages lus".formatted(numberOfEmlFilesRead, numberOfEmlFilesFound);
        LOGGER.info(message);
        updateMessage(message);
        updateProgress(numberOfEmlFilesRead, numberOfEmlFilesFound);
    }

    @Override
    public void onScanFailure(MountPoint mountPoint, ArchivageStoreException archivageStoreException) {
        cancellationMessages.put(mountPoint, archivageStoreException.getMessage());
    }

    @Override
    protected Void call() {
        LOGGER.info("commence la tâche " + this);
        archivageService.indexMountPoints(indexationRequest, this);
        succeeded();
        return null;
    }

    public Map<MountPoint, String> getCancellationMessages() {
        return cancellationMessages;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("indexationRequest", indexationRequest)
                .toString();
    }
}
