package fr.mim_libre.archivage.index;

import fr.mim_libre.archivage.store.MountPoint;

import java.util.Set;

/**
 * Représente une demande de (ré-)indexation d’un ou plusieurs points de montages.
 */
public record IndexationRequest(Set<MountPoint> mountPoints, String description, CleaningStrategy cleaningStrategy) {

    /**
     * Comment il faut nettoyer l’index avant d’y ajouter les données concernant les points de montages passés.
     */
    public enum CleaningStrategy {

        /**
         * Effacer tout l’index.
         */
        DELETE_FULL_INDEX,

        /**
         * Retirer de l’index uniquement les données concernant les points de montages passés.
         */
        REMOVE_EACH_MOUNT_POINT,

        /**
         * Ne rien retirer de l’index : pertinent quand on ajoute un nouveau dossier
         */
        DO_NOTHING
    }
}