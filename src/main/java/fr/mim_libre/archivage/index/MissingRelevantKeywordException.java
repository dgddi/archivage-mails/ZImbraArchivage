package fr.mim_libre.archivage.index;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSortedSet;
import fr.mim_libre.archivage.ArchivageException;

/**
 * Quand l’utilisateur a soumis une recherche mais qu’elle ne contient aucun mot pertinent.
 */
public class MissingRelevantKeywordException extends ArchivageException {

    private final String keyword;

    private final ImmutableSortedSet<String> stopWords;

    MissingRelevantKeywordException(String keyword, ImmutableSortedSet<String> stopWords) {
        super("Aucun mot pertinent dans la recherche « %s »".formatted(keyword));
        this.keyword = keyword;
        this.stopWords = stopWords;
    }

    public String getKeyword() {
        return keyword;
    }

    public ImmutableSortedSet<String> getStopWords() {
        return stopWords;
    }
}
