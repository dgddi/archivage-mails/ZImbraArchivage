package fr.mim_libre.archivage.index;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Représente les options de recherche d’un courriel selon la présence d'une pièce-jointe.
 */
public enum HasAnyAttachment {
    /**
     * Il n’est pas précisé si le courriel recherché doit ou non avoir une pièce jointe
     */
    IGNORED,
    /**
     * Le courriel n’a pas de pièce jointe.
     */
    NO_ATTACHEMENT,
    /**
     * Le courriel a au moins une pièce jointe.
     */
    SOME_ATTACHMENT
}
