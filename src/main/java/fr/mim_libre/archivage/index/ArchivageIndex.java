package fr.mim_libre.archivage.index;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSortedSet;
import fr.mim_libre.archivage.ArchivageEmail;
import fr.mim_libre.archivage.store.EmlFilePath;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.store.MountPoint;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.StoredFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.QueryBuilder;
import org.jsoup.Jsoup;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.text.Collator;
import java.text.ParseException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Gestion de l'indexation des courriels : alimenter l'index, le supprimer...
 *
 * Cette implémentation est basée sur Apache Lucene. Chaque courriel indexé
 * correspond à un document. Les données, sont stockées en fichiers.
 */
public class ArchivageIndex {

    private static final Logger LOGGER = LogManager.getLogger(ArchivageIndex.class);

    private static final int EXPECTED_SCHEMA_VERSION = 1;

    /**
     * Les différents champs qu’on peut mettre dans un document Lucene pour indexer un courriel.
     */
    private enum DocumentField {

        /**
         * Le chemin complet vers le fichier <code>.eml</code>, sert aussi d’idenfitiant technique du document.
         */
        EML_FILE_PATH,

        /**
         * Le point de montage dans lequel se trouve le courriel, au sens {@link MountPoint}
         */
        MOUNT_POINT,

        /**
         * Le dossier dans lequel se trouve le courriel au sein du point de montage, au sens {@link Folder}.
         */
        FOLDER,

        /**
         * Le nom du fichier <code>.eml</code>.
         */
        EML_FILE_NAME,

        /**
         * Le sujet du courriel que le document représente.
         */
        SUBJECT,

        /**
         * Le corps du message. On y mélange le texte et le HTML car, à la recherche c'est le corps en général qui compte
         */
        BODY,

        /**
         * L’expéditeur du couriel
         */
        FROM,

        /**
         * Les destinataires du courriel.
         */
        TOS,

        /**
         * Les destinataires en copie du courriel.
         */
        CCS,

        /**
         * Si le courriel à des pièces jointes ou non, voir {@link HasAnyAttachment}
         */
        HAS_ANY_ATTACHMENT,

        /**
         * La date d’expédition du courriel
         */
        SENT_DATE
    }

    private static final FrenchAnalyzer ANALYZER = new FrenchAnalyzer();

    private static final ImmutableSortedSet<String> STOP_WORDS = ANALYZER.getStopwordSet().stream()
            .map(o -> (char[]) o)
            .map(String::new)
            .collect(ImmutableSortedSet.toImmutableSortedSet(
                    Comparator.comparing(
                            Function.identity(),
                            Collator.getInstance(Locale.FRANCE)
                    )
            ));

    private static final QueryBuilder QUERY_BUILDER = new QueryBuilder(ANALYZER);

    /**
     * Le répertoire dans lequel sont stockées les données de l’index.
     *
     * On y gère rien : c’est Lucene qui ce qu'il veut.
     */
    private final Path indexPath;

    public ArchivageIndex(Path indexPath) {
        this.indexPath = indexPath.resolve("lucene");
        File schemaVersionFile = indexPath.resolve("schema-version").toFile();
        try {
            if (schemaVersionFile.exists()) {
                String versionAsString = FileUtils.readFileToString(schemaVersionFile, StandardCharsets.UTF_8);
                int existingIndexSchemaVersion = Integer.parseInt(versionAsString);
                Preconditions.checkState(
                        existingIndexSchemaVersion == EXPECTED_SCHEMA_VERSION,
                        "L’index existant est dans une version %d alors que cette version du logiciel a besoin d’une version %d".formatted(existingIndexSchemaVersion, EXPECTED_SCHEMA_VERSION)
                );
            } else {
                FileUtils.write(schemaVersionFile, String.valueOf(EXPECTED_SCHEMA_VERSION), StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            throw new RuntimeException("Impossible de lire " + schemaVersionFile, e);
        }
    }

    /**
     * Ouvre l'index en écriture pour y faire des modifications.
     *
     * @return un {@link Writer} qu'il faudra fermer une fois les modifications terminées.
     */
    public Writer openWriter() {
        try {
            Directory directory = FSDirectory.open(indexPath);
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(ANALYZER);
            boolean create = true;
            if (false) {
                // Create a new index in the directory, removing any
                // previously indexed documents:
                indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            } else {
                // Add new documents to an existing index:
                indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            }
            IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);
            return new Writer(indexPath, directory, indexWriter);
        } catch (IOException e) {
            throw new RuntimeException("impossible de créer l'index dans " + indexPath, e);
        }
    }

    public List<IndexSearchResult> search(ArchivageQuery archivageQuery) throws MissingRelevantKeywordException {
        final int pageSize = 100;
        Query luceneQuery = toLuceneQuery(archivageQuery);
        try (FSDirectory fsDirectory = FSDirectory.open(indexPath);
             DirectoryReader reader = DirectoryReader.open(fsDirectory)) {
            IndexSearcher searcher = new IndexSearcher(reader);
            StoredFields storedFields = searcher.storedFields();
            List<IndexSearchResult> searchResults = new LinkedList<>();
            TopDocs documents = searcher.search(luceneQuery, pageSize);
            while (documents.scoreDocs.length > 0) {
                for (ScoreDoc scoreDoc : documents.scoreDocs) {
                    Document document = storedFields.document(scoreDoc.doc);
                    EmlFilePath emlFilePath = getEmlFilePath(document);
                    try {
                        IndexSearchResult indexSearchResult =
                                new IndexSearchResult(
                                        emlFilePath,
                                        document.get(DocumentField.SUBJECT.name()),
                                        document.get(DocumentField.FROM.name()),
                                        DateTools.stringToDate(document.get(DocumentField.SENT_DATE.name())),
                                        HasAnyAttachment.valueOf(document.get(DocumentField.HAS_ANY_ATTACHMENT.name())
                                ));
                        searchResults.add(indexSearchResult);
                    } catch (ParseException e) {
                        LOGGER.warn("le fichier " + emlFilePath + " ne sera pas parmi les résultats car on n’arrive pas à lire les données de l’index", e);
                    }
                }

                // récupérer la prochaine "page" de résultats
                ScoreDoc lastDoc = documents.scoreDocs[documents.scoreDocs.length - 1];
                documents = searcher.searchAfter(lastDoc, luceneQuery, pageSize);
            }
            LOGGER.trace("une recherche avec " + archivageQuery + " donne " + searchResults.size() + " résultats");
            return searchResults;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Transforme la requête métier passée en une requête Lucene faite pour interroger l’index.
     */
    private Query toLuceneQuery(ArchivageQuery archivageQuery) throws MissingRelevantKeywordException {
        BooleanQuery.Builder booleanQueryBuilder = new BooleanQuery.Builder();
        if (archivageQuery.from() != null) {
            booleanQueryBuilder.add(keywordToQuery(DocumentField.FROM, archivageQuery.from()), BooleanClause.Occur.MUST);
        }
        if (archivageQuery.to() != null) {
            booleanQueryBuilder.add(keywordToQuery(DocumentField.TOS, archivageQuery.to()), BooleanClause.Occur.MUST);
        }
        if (archivageQuery.cc() != null) {
            booleanQueryBuilder.add(keywordToQuery(DocumentField.CCS, archivageQuery.cc()), BooleanClause.Occur.MUST);
        }
        if (archivageQuery.hasAnyAttachment() != HasAnyAttachment.IGNORED) {
            booleanQueryBuilder.add(new TermQuery(new Term(DocumentField.HAS_ANY_ATTACHMENT.name(), archivageQuery.hasAnyAttachment().name())), BooleanClause.Occur.MUST);
        }
        switch (archivageQuery.searchKeywordStrategy()) {
            case IGNORED -> {}
            case SUBJECT -> booleanQueryBuilder.add(keywordToQuery(DocumentField.SUBJECT, archivageQuery.keyword()), BooleanClause.Occur.MUST);
            case BODY -> booleanQueryBuilder.add(keywordToQuery(DocumentField.BODY, archivageQuery.keyword()), BooleanClause.Occur.MUST);
            case SUBJECT_OR_BODY -> {
                booleanQueryBuilder.setMinimumNumberShouldMatch(1);
                booleanQueryBuilder.add(keywordToQuery(DocumentField.SUBJECT, archivageQuery.keyword()), BooleanClause.Occur.SHOULD);
                booleanQueryBuilder.add(keywordToQuery(DocumentField.BODY, archivageQuery.keyword()), BooleanClause.Occur.SHOULD);
            }
            default -> throw new IllegalStateException("non géré " + archivageQuery.searchKeywordStrategy());
        }
        if (archivageQuery.sentDate() != null) {
            String lowerBound;
            if (archivageQuery.sentDate().hasLowerBound()) {
                lowerBound = DateTools.dateToString(archivageQuery.sentDate().lowerEndpoint(), DateTools.Resolution.SECOND);
            } else {
                lowerBound = null;
            }
            String upperBound;
            if (archivageQuery.sentDate().hasUpperBound()) {
                upperBound = DateTools.dateToString(archivageQuery.sentDate().upperEndpoint(), DateTools.Resolution.SECOND);
            } else {
                upperBound = null;
            }
            TermRangeQuery dateSentQuery = TermRangeQuery.newStringRange(
                    DocumentField.SENT_DATE.name(),
                    lowerBound,
                    upperBound,
                    true,
                    true
            );
            booleanQueryBuilder.add(dateSentQuery, BooleanClause.Occur.MUST);
        }
        if (archivageQuery.mountPoint() != null) {
            MountPoint mountPoint = archivageQuery.mountPoint();
            booleanQueryBuilder.add(new TermQuery(new Term(DocumentField.MOUNT_POINT.name(), mountPoint.root().toString())), BooleanClause.Occur.MUST);
        }
        if (archivageQuery.folder() != null) {
            Folder folder = archivageQuery.folder();
            booleanQueryBuilder.add(new TermQuery(new Term(DocumentField.MOUNT_POINT.name(), folder.mountPoint().root().toString())), BooleanClause.Occur.MUST);
            booleanQueryBuilder.add(new TermQuery(new Term(DocumentField.FOLDER.name(), folder.path().toString())), BooleanClause.Occur.MUST);
        }
        BooleanQuery booleanQuery = booleanQueryBuilder.build();
        return booleanQuery;
    }

    private Query keywordToQuery(DocumentField documentField, String keyword) throws MissingRelevantKeywordException {
        Query query = QUERY_BUILDER.createPhraseQuery(documentField.name(), keyword);
        if (query == null) {
            throw new MissingRelevantKeywordException(keyword, STOP_WORDS);
        }
        return query;
    }

    /**
     * Fabrique un document Lucene qui peut représenter une entrée de l’index pour le courriel passé.
     */
    private Document toDocument(ArchivageEmail email) {
        Document document = new Document();
        EmlFilePath emlFilePath = email.emlFilePath();
        Folder folder = emlFilePath.folder();
        document.add(new StringField(DocumentField.EML_FILE_PATH.name(), emlFilePath.toFullPath().toString(), Field.Store.YES));
        document.add(new StringField(DocumentField.MOUNT_POINT.name(), folder.mountPoint().root().toString(), Field.Store.YES));
        document.add(new StringField(DocumentField.FOLDER.name(), folder.path().toString(), Field.Store.YES));
        document.add(new StringField(DocumentField.EML_FILE_NAME.name(), emlFilePath.fileName().toString(), Field.Store.YES));
        if (email.subject() != null) {
            document.add(new TextField(DocumentField.SUBJECT.name(), email.subject(), Field.Store.YES));
        }
        ArchivageEmail.Recipient from = email.from();
        document.add(new TextField(DocumentField.FROM.name(), toFieldValue(from), Field.Store.YES));
        if (!email.tos().isEmpty()) {
            String tosAsString = email.tos().stream()
                    .map(this::toFieldValue)
                    .collect(Collectors.joining(" "));
            document.add(new TextField(DocumentField.TOS.name(), tosAsString, Field.Store.YES));
        }
        if (!email.ccs().isEmpty()) {
            String ccsAsString = email.ccs().stream()
                    .map(this::toFieldValue)
                    .collect(Collectors.joining(" "));
            document.add(new TextField(DocumentField.CCS.name(), ccsAsString, Field.Store.YES));
        }
        document.add(new StringField(DocumentField.HAS_ANY_ATTACHMENT.name(), email.hasAnyAttachment().name(), Field.Store.YES));

        // pour le corps, on bourre le contenu texte et HTML car une recherche se fera sur les deux
        StringBuilder bodyBuilder = new StringBuilder();
        if (email.plainText() != null) {
            bodyBuilder.append(email.plainText());
        }
        if (email.htmlText() != null) {
            String strippedHtml = Jsoup.parse(email.htmlText()).text();
            bodyBuilder.append(strippedHtml);
        }
        String body = bodyBuilder.toString();
        document.add(new TextField(DocumentField.BODY.name(), body, Field.Store.YES));

        // conformément à la doc Lucene, la Date est stockée au format dans un format texte qui permet la recherche
        document.add(new StringField(DocumentField.SENT_DATE.name(), DateTools.dateToString(email.sentDate(), DateTools.Resolution.SECOND), Field.Store.YES));

        return document;
    }

    /**
     * Pour une personne liée à un couriel, la chaîne à indexer pour la recherche
     */
    private String toFieldValue(ArchivageEmail.Recipient recipient) {
        return Stream.of(recipient.name(), recipient.address())
                .filter(Objects::nonNull)
                .collect(Collectors.joining(" "));
    }

    /**
     * Pour un document donné, on retrouve son {@link EmlFilePath}.
     */
    private EmlFilePath getEmlFilePath(Document document) {
        MountPoint mountPoint = new MountPoint(Path.of(document.get(DocumentField.MOUNT_POINT.name())));
        Folder folder = mountPoint.folder(Path.of(document.get(DocumentField.FOLDER.name())));
        EmlFilePath emlFilePath = folder.getEmlFilePath(Path.of(document.get(DocumentField.EML_FILE_NAME.name())));
        return emlFilePath;
    }

    /**
     * Supprimer l’index. Étape préalable à la réindexation.
     */
    public void delete() {
        try {
            FileUtils.deleteDirectory(indexPath.toFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Si l’index a été créé (sinon, c'est qu’il n’a jamais été créer)
     */
    public boolean exists() {
        try {
            File indexDir = indexPath.toFile();
            return indexDir.exists() && !FileUtils.isEmptyDirectory(indexDir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public class Writer implements Closeable {

        private final Path indexPath;

        private final Directory directory;

        private final IndexWriter indexWriter;

        private Writer(Path indexPath, Directory directory, IndexWriter indexWriter) {
            this.indexPath = indexPath;
            this.directory = directory;
            this.indexWriter = indexWriter;
        }

        /**
         * Ajoute un courriel à l’index.
         */
        public void add(ArchivageEmail archivageEmail) {
            Document document = toDocument(archivageEmail);
            try {
                switch (indexWriter.getConfig().getOpenMode()) {
                    case CREATE -> indexWriter.addDocument(document);
                    case CREATE_OR_APPEND, APPEND ->
                            indexWriter.updateDocument(new Term(DocumentField.EML_FILE_PATH.name(), getEmlFilePath(document).toString()), document);
                    default -> throw new IllegalStateException("non géré " + indexWriter.getConfig().getOpenMode());
                }
            } catch (IOException e) {
                throw new RuntimeException("impossible d'écrire dans l'index " + indexPath, e);
            }
        }

        /**
         * Supprime de l’index tout le contenu lié à au point de montage passé.
         */
        public void delete(MountPoint mountPoint) {
            LOGGER.info("efface l’index de " + mountPoint);
            try {
                ArchivageQuery archivageQuery = ArchivageQuery.forMountPoint(mountPoint);
                Query luceneQuery = toLuceneQuery(archivageQuery);
                indexWriter.deleteDocuments(luceneQuery);
            } catch (MissingRelevantKeywordException e) {
                throw new IllegalStateException("ne devrait jamais arriver", e);
            } catch (IOException e) {
                throw new RuntimeException("impossible d'écrire dans l'index " + indexPath, e);
            }
        }

        @Override
        public void close() {
            try {
                indexWriter.close();
                directory.close();
            } catch (IOException e) {
                throw new RuntimeException("impossible de fermer " + indexPath, e);
            }
        }
    }
}
