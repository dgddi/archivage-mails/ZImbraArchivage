package fr.mim_libre.archivage.index;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Range;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.store.MountPoint;

import java.util.Date;

/**
 * Représente une requête de recherche selon les critères de l’utilisateur
 *
 * @param from                  Expéditeur (email complète ou partielle)
 * @param to                    Destinataire A (email complète ou partielle)
 * @param cc                    Destinataire Cc (email complète ou partielle)
 * @param sentDate              Date
 * @param hasAnyAttachment      Avec ou sans pièce jointe
 * @param mountPoint            Le point de montage dans lequel doit se trouver le message
 * @param folder                Le dossier dans lequel doit se trouver le message
 * @param searchKeywordStrategy Comment le mot-clé recherché doit être utilisé comme critère
 * @param keyword               Le mot-clé recherché
 */
public record ArchivageQuery(
        String from,
        String to,
        String cc,
        Range<Date> sentDate,
        HasAnyAttachment hasAnyAttachment,
        MountPoint mountPoint,
        Folder folder,
        SearchKeywordStrategy searchKeywordStrategy,
        String keyword) {

    /**
     * {@return une requête qui permet d’afficher l’intégralité du contenu d’un dossier}
     */
    public static ArchivageQuery forFolder(Folder folder) {
        return new ArchivageQuery(
                null,
                null,
                null,
                null,
                HasAnyAttachment.IGNORED,
                null,
                folder,
                SearchKeywordStrategy.IGNORED,
                null
        );
    }

    /**
     * {@return une requête qui permet de rechercher l’intégralité du contenu d’un point de montage}
     */
    public static ArchivageQuery forMountPoint(MountPoint mountPoint) {
        return new ArchivageQuery(
                null,
                null,
                null,
                null,
                HasAnyAttachment.IGNORED,
                mountPoint,
                null,
                SearchKeywordStrategy.IGNORED,
                null
        );
    }
}
