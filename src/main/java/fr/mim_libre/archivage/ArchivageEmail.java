package fr.mim_libre.archivage;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.mim_libre.archivage.index.HasAnyAttachment;
import fr.mim_libre.archivage.store.EmlFilePath;

import java.util.Date;
import java.util.Set;

/**
 * Représentation en mémoire d’un courriel souce, contenu et méta-données.
 *
 * @param emlFilePath le fichier qui a été lu
 * @param subject le sujet
 * @param from l’expéditeur
 * @param tos les destinataires
 * @param ccs les destinataires en copie
 * @param hasAnyAttachment s’il y a des pièces jointes
 * @param plainText la partie texte brut du corps du message
 * @param htmlText la partie HTML du corps du message
 * @param sentDate la date d’expéditions
 * @param attachments les pièces jointes
 * @param emlSource le contenu du fichier source (en .eml)
 */
public record ArchivageEmail(
        EmlFilePath emlFilePath,
        String subject,
        Recipient from,
        Set<Recipient> tos,
        Set<Recipient> ccs,
        HasAnyAttachment hasAnyAttachment,
        String plainText,
        String htmlText,
        Date sentDate,
        Set<Attachment> attachments,
        String emlSource) {

    /**
     * Représente un contact dans un message
     *
     * @param name le nom d’affichage
     * @param address l’adresse courriel
     */
    public record Recipient(
            String name,
            String address
    ) {

        /**
         * {@return pour affichage dans l’interface}
         */
        public String toHumanReadableString() {
            String str;
            if (Strings.isNullOrEmpty(name())) {
                str = address();
            } else {
                str = String.format("%s <%s>", name(), address());
            }
            return str;
        }
    }

    /**
     * Une pièce jointe
     *
     * @param name un nom de fichier
     * @param dataSource un moyen d’accéder au contenu du fichier
     */
    public record Attachment(
        String name,

        jakarta.activation.DataSource dataSource) {

    }
}
