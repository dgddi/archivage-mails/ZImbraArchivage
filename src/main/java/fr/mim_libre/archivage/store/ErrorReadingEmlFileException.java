package fr.mim_libre.archivage.store;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Une erreur est survenue en essayant de lire un fichier <code>.eml</code> stocké localement
 */
public class ErrorReadingEmlFileException extends ArchivageStoreException {

    private final EmlFilePath emlFilePath;

    public ErrorReadingEmlFileException(EmlFilePath emlFilePath, String message) {
        super("Erreur à la lecture de " + emlFilePath.toFullPath() + ". " + message);
        this.emlFilePath = emlFilePath;
    }

    public ErrorReadingEmlFileException(EmlFilePath emlFilePath, Throwable cause) {
        super("Erreur à la lecture de " + emlFilePath.toFullPath(), cause);
        this.emlFilePath = emlFilePath;
    }

    public EmlFilePath getEmlFilePath() {
        return emlFilePath;
    }
}
