package fr.mim_libre.archivage.store;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.nio.file.Path;

/**
 * Le point de montage est un dossier, sur l’ordinateur local, dans lequel on va stocker tous les fichiers courriels.
 *
 * Il est définit par le répertoire dans lequel on stocke tout. Il peut contenir des sous dossiers.
 *
 * @param root le répertoire de stockage
 */
public record MountPoint(Path root) {

    /**
     * {@return un {@link Folder} dont le nom est passé et qui se trouve dans le point de montage représenté par this}
     */
    public Folder folder(String path) {
        return new Folder(this, Path.of(path));
    }

    /**
     * {@return un {@link Folder} dont le nom est passé et qui se trouve dans le point de montage représenté par this}
     */
    public Folder folder(Path path) {
        return new Folder(this, path);
    }

    /**
     * {@return le {@link Folder} qui correspond au répertoire racine du point de montage}
     */
    public Folder rootFolder() {
        return new Folder(this, Path.of(""));
    }

    /**
     * Étant donné un chemin vers un fichier contenu dans ce point de montage, on redécompose Folder et fichier pour retrouver un {@link EmlFilePath}
     */
    public EmlFilePath parseEmlFilePath(Path emlFilePath) {
        Path fileName = emlFilePath.getFileName();
        Path parent = emlFilePath.getParent();
        Path folderPath = root().relativize(parent);
        return folder(folderPath).getEmlFilePath(fileName);
    }
}
