package fr.mim_libre.archivage.store;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSetMultimap;
import fr.mim_libre.archivage.ArchivageEmail;
import fr.mim_libre.archivage.NotEnoughDiskSpaceArchivageException;
import fr.mim_libre.archivage.index.HasAnyAttachment;
import fr.mim_libre.archivage.logging.ArchivageLogger;
import jakarta.mail.Message;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.file.AccumulatorPathVisitor;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.simplejavamail.MailException;
import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.email.Recipient;
import org.simplejavamail.converter.EmailConverter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * La gestion du stockage de fichiers <code>.eml</code> sur le FS.
 *
 * Contient les algos pour lire/écrire des emails, parcourrir récursivement les dossiers d’un point de montage.
 */
public class Store {

    private static final Logger LOGGER = LogManager.getLogger(Store.class);

    private final ArchivageLogger archivageLogger;

    public Store(ArchivageLogger archivageLogger) {
        this.archivageLogger = archivageLogger;
    }

    /**
     * Tous les {@link Folder} existants dans le point de montage passé.
     */
    public Set<Folder> getExistingFolders(MountPoint mountPoint) throws ArchivageStoreException {
        Path pathToScan = mountPoint.root();
        ScanResult scanResult = scan(pathToScan);
        return scanResult.directories().stream()
                .map(pathToScan::relativize)
                .map(mountPoint::folder)
                .collect(Collectors.toUnmodifiableSet());
    }

    public void checkSpaceIsAvailable(MountPoint mountPoint, long sizeToBeDownloaded) throws NotEnoughDiskSpaceArchivageException {
        long freeSpaceInMountPoint = mountPoint.root().toFile().getFreeSpace();
        boolean notEnoughSpace = freeSpaceInMountPoint - 10 * FileUtils.ONE_MB < sizeToBeDownloaded;
        if (notEnoughSpace) {
            throw new NotEnoughDiskSpaceArchivageException(mountPoint.root().toFile(), freeSpaceInMountPoint, sizeToBeDownloaded);
        }
    }

    /**
     * Le résultat d’un scan d’un répertoire
     *
     * @param emlPaths les fichiers .eml trouvés
     * @param directories les dossiers trouvés
     */
    private record ScanResult(List<Path> emlPaths, List<Path> directories) {

        private int getNumberOfFilesFound() {
            return emlPaths().size();
        }
    }

    /**
     * Scanner un {@link Folder} et de consommer les fichiers .eml rencontrés tout en suivant la progression.
     */
    public void scanPathAndConsumeEmlFiles(Set<MountPoint> mountPoints, EmlFileVisitor consumer, StoreScanProgressHandler storeScanProgressHandler) {
        Queue<EmlFilePath> allToIndex = new LinkedList<>();
        for (MountPoint mountPoint : mountPoints) {
            Folder folder = mountPoint.rootFolder();
            try {
                ScanResult scanResult = scan(folder.toFullPath());
                scanResult.emlPaths().stream()
                        .map(mountPoint::parseEmlFilePath)
                        .forEach(allToIndex::add);
            } catch (ArchivageStoreException e) {
                LOGGER.warn("Erreur pendant l’indexation", e);
                storeScanProgressHandler.onScanFailure(mountPoint, e);
            }
        }
        int numberOfEmlFilesFound = allToIndex.size();
        storeScanProgressHandler.onFilesListed(numberOfEmlFilesFound);
        allToIndex.forEach(emlFilePath -> {
            try {
                ArchivageEmail archivageEmail = readEmlFile(emlFilePath);
                consumer.onEmlFileReadSuccess(archivageEmail);
            } catch (ErrorReadingEmlFileException e) {
                consumer.onEmlFileReadFailure(emlFilePath, e);
            }
            storeScanProgressHandler.onOneFileRead();
        });
    }

    /**
     * Scanne un répertoire local et retourne un {@link ScanResult} listant dossiers et fichiers .eml.
     */
    private ScanResult scan(Path pathToScan) throws ArchivageStoreException {
        AccumulatorPathVisitor visitor =
                AccumulatorPathVisitor.withLongCounters(
                        FileFilterUtils.suffixFileFilter(".eml"),
                        FileFilterUtils.directoryFileFilter()
                );
        try {
            Files.walkFileTree(pathToScan, visitor);
            ScanResult scanResult = new ScanResult(visitor.getFileList(), visitor.getDirList());
            return scanResult;
        } catch (AccessDeniedException e) {
            throw new ArchivageStoreException("impossible de parcourir " + pathToScan + " car on ne peut pas accéder à " + e.getFile(), e);
        } catch (IOException e) {
            throw new ArchivageStoreException("impossible de parcourir " + pathToScan, e);
        }
    }

    /**
     * Lecture d’un fichier au format .eml, retourne une représentation mémoire ou lève une erreur si fichier incorrect.
     */
    public ArchivageEmail readEmlFile(EmlFilePath emlFilePath) throws ErrorReadingEmlFileException {
        try {
            Email email = EmailConverter.emlToEmail(emlFilePath.toFile());
            String eml = EmailConverter.emailToEML(email);
            if (email.getSentDate() == null) {
                LOGGER.warn("on ignore un message sans date d’expédition " + emlFilePath);
                archivageLogger.onEmailMissingSentDate(emlFilePath);
                throw new EmlFileMissingSentDateException(emlFilePath);
            } else {
                ImmutableSetMultimap<Message.RecipientType, ArchivageEmail.Recipient> allRecipients =
                        email.getRecipients().stream()
                                .collect(ImmutableSetMultimap.toImmutableSetMultimap(Recipient::getType, this::toArchivageRecipient));
                HasAnyAttachment hasAnyAttachment = email.getAttachments().isEmpty() ? HasAnyAttachment.NO_ATTACHEMENT : HasAnyAttachment.SOME_ATTACHMENT;
                Set<ArchivageEmail.Attachment> attachments = email.getAttachments().stream()
                        .map(attachmentResource -> new ArchivageEmail.Attachment(attachmentResource.getName(), attachmentResource.getDataSource()))
                        .collect(Collectors.toUnmodifiableSet());
                ArchivageEmail archivageEmail = new ArchivageEmail(
                        emlFilePath,
                        email.getSubject(),
                        toArchivageRecipient(email.getFromRecipient()),
                        allRecipients.get(Message.RecipientType.TO),
                        allRecipients.get(Message.RecipientType.CC),
                        hasAnyAttachment,
                        email.getPlainText(),
                        email.getHTMLText(),
                        email.getSentDate(),
                        attachments,
                        eml
                );
                return archivageEmail;
            }
        } catch (MailException e) {
            LOGGER.warn("erreur à la lecture de " + emlFilePath, e);
            throw new ErrorReadingEmlFileException(emlFilePath, e);
        } catch (RuntimeException e) {
            // en particulier, on se retrouve à géré MailException mais encapsulé
            // cela n'arrive que sur Windows pour une raison inexpliquée
            // 2023/10/30 13:23:40 ERROR (MainApplication.java:129) lambda$startIndexationTask$0 erreur pendant lexécution de IndexationTask{indexationRequest=IndexationRequest[mountPoints=[MountPoint[root=C:\Users\mdesecot-zextra\Desktop\ArchivageMailFredJ]], description=Première indexation du dossier de travail, cleaningStrategy=DO_NOTHING]}
            //com.pivovarit.function.exception.WrappedException: Error decoding text
            //	at com.pivovarit.function.ThrowingFunction.lambda$uncheck$4(ThrowingFunction.java:86) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at java.util.stream.ReferencePipeline$3$1.accept(Unknown Source) ~[?:?]
            //	at java.util.Spliterators$ArraySpliterator.forEachRemaining(Unknown Source) ~[?:?]
            //	at java.util.stream.AbstractPipeline.copyInto(Unknown Source) ~[?:?]
            //	at java.util.stream.AbstractPipeline.wrapAndCopyInto(Unknown Source) ~[?:?]
            //	at java.util.stream.AbstractPipeline.evaluate(Unknown Source) ~[?:?]
            //	at java.util.stream.AbstractPipeline.evaluateToArrayNode(Unknown Source) ~[?:?]
            //	at java.util.stream.ReferencePipeline.toArray(Unknown Source) ~[?:?]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.lambda$retrieveRecipients$3(MimeMessageParser.java:496) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at java.util.Optional.map(Unknown Source) ~[?:?]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.retrieveRecipients(MimeMessageParser.java:494) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.parseCcAddresses(MimeMessageParser.java:475) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.parseMimeMessage(MimeMessageParser.java:149) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.EmailConverter.mimeMessageToEmailBuilder(EmailConverter.java:138) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.EmailConverter.mimeMessageToEmailBuilder(EmailConverter.java:125) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.EmailConverter.emlToEmailBuilder(EmailConverter.java:359) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.EmailConverter.emlToEmail(EmailConverter.java:343) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.EmailConverter.emlToEmail(EmailConverter.java:335) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at fr.mim_libre.archivage.store.Store.readEmlFile(Store.java:158) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at fr.mim_libre.archivage.store.Store.lambda$scanPathAndConsumeEmlFiles$0(Store.java:124) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at java.lang.Iterable.forEach(Unknown Source) ~[?:?]
            //	at fr.mim_libre.archivage.store.Store.scanPathAndConsumeEmlFiles(Store.java:122) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at fr.mim_libre.archivage.ArchivageService.indexMountPoints(ArchivageService.java:207) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at fr.mim_libre.archivage.index.IndexationTask.call(IndexationTask.java:82) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at fr.mim_libre.archivage.index.IndexationTask.call(IndexationTask.java:40) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at javafx.concurrent.Task$TaskCallable.call(Task.java:1426) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at java.util.concurrent.FutureTask.run(Unknown Source) ~[?:?]
            //	at java.lang.Thread.run(Unknown Source) [?:?]
            //Caused by: org.simplejavamail.converter.internal.mimemessage.MimeMessageParseException: Error decoding text
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.decodeText(MimeMessageParser.java:545) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.decodePersonalName(MimeMessageParser.java:516) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.lambda$null$1(MimeMessageParser.java:495) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at com.pivovarit.function.ThrowingFunction.lambda$uncheck$4(ThrowingFunction.java:84) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	... 27 more
            //Caused by: java.io.UnsupportedEncodingException: UNKNOWN
            //	at java.lang.String.lookupCharset(Unknown Source) ~[?:?]
            //	at java.lang.String.<init>(Unknown Source) ~[?:?]
            //	at jakarta.mail.internet.MimeUtility.decodeWord(MimeUtility.java:901) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at jakarta.mail.internet.MimeUtility.decodeText(MimeUtility.java:603) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.decodeText(MimeMessageParser.java:543) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.decodePersonalName(MimeMessageParser.java:516) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at org.simplejavamail.converter.internal.mimemessage.MimeMessageParser.lambda$null$1(MimeMessageParser.java:495) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	at com.pivovarit.function.ThrowingFunction.lambda$uncheck$4(ThrowingFunction.java:84) ~[archivage-0.4.2-0.4.2-dirty.jar:0.4.2]
            //	... 27 more
            LOGGER.error("erreur à la lecture de " + emlFilePath, e);
            throw new ErrorReadingEmlFileException(emlFilePath, e);
        }
    }

    private ArchivageEmail.Recipient toArchivageRecipient(Recipient recipient) {
        return new ArchivageEmail.Recipient(recipient.getName(), recipient.getAddress());
    }

    /**
     * Enregistre un fichier courriel sur le disque.
     *
     * @param emlFilePath déterminera lechemin où il faut l'enregistrer
     * @param inputStream le contenu
     * @throws ArchivageStoreException si on arrive pas à écrire
     */
    public void write(EmlFilePath emlFilePath, InputStream inputStream) throws ArchivageStoreException {
        Path filePathToStoreEmlFile = emlFilePath.toFullPath();
        LOGGER.debug("va écrire " + filePathToStoreEmlFile);
        try (OutputStream outputStream = Files.newOutputStream(filePathToStoreEmlFile, StandardOpenOption.CREATE_NEW)) {
            IOUtils.copy(inputStream, outputStream);
        } catch (IOException e) {
            throw new ArchivageStoreException("impossible d’écrire le fichier " + filePathToStoreEmlFile, e);
        }
    }

    /**
     * Assure qu‘un dossier local physique correspondant au {@link Folder} logique existe.
     */
    public void ensureDirectoryExists(Folder folder) {
        boolean createdFolder = folder.toFullPath().toFile().mkdirs();
        if (createdFolder) {
            LOGGER.info("a créé un dossier pour stocker " + folder);
        }
    }

    /**
     * {@return le contenu du .eml du courriel passé}
     */
    public String readEmlFileAsEmlString(EmlFilePath emlFilePath) throws ArchivageStoreException {
        try {
            return Files.readString(emlFilePath.toFullPath());
        } catch (IOException e) {
            throw new ArchivageStoreException("impossible de lire le fichier " + emlFilePath, e);
        }
    }
}
