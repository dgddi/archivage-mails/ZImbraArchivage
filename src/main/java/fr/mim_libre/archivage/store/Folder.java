package fr.mim_libre.archivage.store;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;

import java.nio.file.Path;
import java.text.Collator;
import java.util.Date;
import java.util.Locale;

/**
 * Représente la notion de dossier logique : il se trouve dans un point de montage et contient des fichiers courriels.
 *
 * Il n’a pas de notion de sous-dossier : une instance peut avoir pour chemin "a/b/c" et une autre instance "a/b".
 *
 * @param mountPoint le point de montage dans lequel ce dossier se trouve
 * @param path le chemin que représente ce dossier à partir de la racine du point de montage
 */
public record Folder(MountPoint mountPoint, Path path) {

    private static final String INBOX_FOLDER_NAME = "Inbox";

    private static final String SENT_FOLDER_NAME = "Sent";

    private static final String DRAFTS_FOLDER_NAME = "Drafts";

    private static final String JUNK_FOLDER_NAME = "Junk";

    private static final String TRASH_FOLDER_NAME = "Trash";

    /**
     * Les noms des dossiers principaux, dans l’ordre dans lequel il faut les afficher.
     */
    private static final ImmutableList<String> TOP_FOLDER_NAMES =
            ImmutableList.of(
                    INBOX_FOLDER_NAME,
                    SENT_FOLDER_NAME,
                    JUNK_FOLDER_NAME,
                    DRAFTS_FOLDER_NAME,
                    TRASH_FOLDER_NAME
            );

    /**
     * Un algo de tri pour classer les noms des dossiers principaux dans l’ordre dans lequel ils doivent apparaître.
     */
    private static final Ordering<String> TOP_FOLDER_NAMES_ORDERING =
            Ordering.explicit(TOP_FOLDER_NAMES);

    /**
     * Un algo de tri pour classer les dossiers principaux dans l’ordre dans lequel ils doivent apparaître.
     */
    private static final Ordering<Folder> TOP_FOLDERS_ORDERING =
            TOP_FOLDER_NAMES_ORDERING.onResultOf(Folder::getName);

    /**
     * Un algo de tri pour classer les dossiers dans l’ordre alphabétique de leur libellé.
     */
    private static final Ordering<Folder> HUMAN_READABLE_NAME_ORDERING =
            Ordering.from(Collator.getInstance(Locale.FRANCE))
                    .onResultOf(Folder::getHumanReadableName);

    /**
     * Un algo de tri des dossiers qui place les dossier principaux en premier dans l'ordre prévu,
     * puis les autres par ordre alphabétique.
     */
    public static final Ordering<Folder> FOLDERS_ORDERING = new Ordering<>() {

        @Override
        public int compare(Folder left, Folder right) {
            final int compare;
            if (left.isTopFolder() && right.isTopFolder()) {
                // deux dossiers principaux, on trie en respectant l'ordre arbitraire
                compare = TOP_FOLDERS_ORDERING.compare(left, right);
            } else if (left.isTopFolder()) {
                // on place les dossiers principaux avant les autres
                compare = -1;
            } else if (right.isTopFolder()) {
                // on place les autres dossiers après les dossiers principaux
                compare = 1;
            } else {
                // les autres dossiers sont triés par ordre alphabétique
                compare = HUMAN_READABLE_NAME_ORDERING.compare(left, right);
            }
            return compare;
        }
    };

    /**
     * Certains dossiers ont, par convention, des noms anglaids : il faut les afficher en français.
     */
    private static final ImmutableMap<String, String> TRANSLATIONS =
            ImmutableMap.<String, String>builder()
                    .put(INBOX_FOLDER_NAME, "Réception")
                    .put(SENT_FOLDER_NAME, "Envoyé")
                    .put(DRAFTS_FOLDER_NAME, "Brouillons")
                    .put(JUNK_FOLDER_NAME, "Spam")
                    .put(TRASH_FOLDER_NAME, "Corbeille")
                    .build();

    public Folder {
        Preconditions.checkArgument(mountPoint.root().resolve(path).startsWith(mountPoint.root()), "Le chemin " + path + " ne doit pas nous amener à sortir du point de montage " + mountPoint);
    }

    /**
     * Définit le nom du fichier où doit être stocké un fichier .eml selon les méta-données du courriel.
     */
    public EmlFilePath getEmlFilePath(String id, Date date, String subject, int maxFileNameLength) {
        return EmlFilePath.generateFileName(this, id, date, subject, maxFileNameLength);
    }

    /**
     * Étant donné un nom de fichier .eml, retoune un {@link EmlFilePath} pour ce fichier qui serait stocké dans le dossier représenté par cette instance.
     */
    public EmlFilePath getEmlFilePath(String emlFileName) {
        return getEmlFilePath(Path.of(emlFileName));
    }

    /**
     * Étant donné un nom de fichier .eml, retoune un {@link EmlFilePath} pour ce fichier qui serait stocké dans le dossier représenté par cette instance.
     */
    public EmlFilePath getEmlFilePath(Path emlFileName) {
        return new EmlFilePath(this, emlFileName);
    }

    /**
     * Le chemin absolu vers ce dossier sur le FS.
     */
    public Path toFullPath() {
        Path mountPointPath = mountPoint.root();
        Path fullPath = mountPointPath.resolve(path);
        return fullPath;
    }

    /**
     * {@return un nom pour affiche cette instance dans l’interface}
     */
    public String getHumanReadableName() {
        String folderName;
        if (isRoot()) {
            folderName = "racine";
        } else {
            String name = getName();
            folderName = TRANSLATIONS.getOrDefault(name, name);
        }
        return folderName;
    }

    /**
     * Le dossier parent de ce dossier.
     *
     * Par exemple, <code>a/b/c</code> donnera <code>a/b</code>.
     *
     * Un garde-fou évite de remonter au delà du dossier racine.
     */
    public Folder getParent() {
        Preconditions.checkArgument(!isRoot());
        Path parent = path().getParent();
        Folder parentFolder;
        if (parent == null) {
            parentFolder = mountPoint.rootFolder();
        } else {
            parentFolder = new Folder(mountPoint(), parent);
        }
        return parentFolder;
    }

    public boolean isRoot() {
        return equals(mountPoint().rootFolder());
    }

    /**
     * {@return le nom du dossier (sans les ancêtres)}
     *
     * Par exemple, <code>a/b/c</code> retourne <code>c</code>
     */
    public String getName() {
        return path().getFileName().toString();
    }

    /**
     * {@return si c’est un dossier principal}
     */
    public boolean isTopFolder() {
        return TOP_FOLDER_NAMES.contains(getName());
    }
}
