package fr.mim_libre.archivage.store;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.mim_libre.archivage.MainApplication;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Représente une référence vers un courriel stocké dans Archivage.
 *
 * Il est identifié par le nom du fichier et le {@link Folder} dans lequel il se trouve.
 */
public record EmlFilePath(Folder folder, Path fileName) {

    /**
     * Longueur maximale d’un nom de fichier.
     *
     * Voir https://stackoverflow.com/questions/265769/maximum-filename-length-in-ntfs-windows-xp-and-windows-vista?rq=3
     */
    static final int MAX_FILE_NAME_LENGTH = 255;

    public EmlFilePath {
        Preconditions.checkArgument(
                fileName.toString().length() <= MAX_FILE_NAME_LENGTH,
                "'%s' est trop long comme nom de fichier. Longueur maximale : %d",
                fileName.toString(),
                MAX_FILE_NAME_LENGTH
        );
    }

    /**
     * Génère le nom du fichier où doit être stocké un fichier .eml selon les méta-données du courriel.
     */
    static EmlFilePath generateFileName(Folder folder, String id, Date date, String subject, int maxFileNameLength) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = simpleDateFormat.format(date);
        String prefix = "%s_".formatted(formattedDate);
        String suffix = "_%s.eml".formatted(id);
        String escapedSubject = subject.replaceAll("[^0-9a-zA-Z ]", "_");
        int availableLengthForSubject = maxFileNameLength - prefix.length() - suffix.length();
        String truncatedSubject = StringUtils.truncate(escapedSubject, availableLengthForSubject);
        String emlFileName = prefix + truncatedSubject + suffix;
        return new EmlFilePath(folder, Path.of(emlFileName));
    }

    /**
     * {@return le chemin absolu vers le fichier <code>.eml</code>}
     */
    public Path toFullPath() {
        Path folderPath = folder.toFullPath();
        Path fullPath = folderPath.resolve(fileName);
        return fullPath;
    }

    public File toFile() {
        return toFullPath().toFile();
    }
}
