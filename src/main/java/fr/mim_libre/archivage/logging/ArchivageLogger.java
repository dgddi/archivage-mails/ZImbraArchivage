package fr.mim_libre.archivage.logging;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Throwables;
import fr.mim_libre.archivage.ArchivageEmail;
import fr.mim_libre.archivage.ArchivageException;
import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import fr.mim_libre.archivage.store.ArchivageStoreException;
import fr.mim_libre.archivage.store.EmlFilePath;
import fr.mim_libre.archivage.store.ErrorReadingEmlFileException;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import org.apache.commons.io.file.AccumulatorPathVisitor;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Gestion de la journalisation utilisateur.
 *
 * La journalisation utilisateur écrit dans un fichier journalier les opérations qui
 * ont eu lieu afin d’avoir une traçabilité.
 *
 * On gère une durée de rétention fixée selon le profil utilisateur.
 */
public class ArchivageLogger {

    private static final Logger LOGGER = LogManager.getLogger(ArchivageLogger.class);

    private final ArchivageConfiguration configuration;

    private final Path logsDirectoryPath;

    public ArchivageLogger(ArchivageConfiguration configuration, Path logsDirectoryPath) {
        this.configuration = configuration;
        this.logsDirectoryPath = logsDirectoryPath;
    }

    private ZonedDateTime now() {
        return configuration.now();
    }

    public void onRootScanEntry(Set<MountPoint> mountPoints) {
        String message = mountPoints.stream()
                .map(MountPoint::root)
                .map(Path::toString)
                .collect(Collectors.joining(",", "début d’indexation des dossiers ", ""));
        write(message);
    }

    public void onRootScanExit(Set<MountPoint> mountPoints) {
        String message = mountPoints.stream()
                .map(MountPoint::root)
                .map(Path::toString)
                .collect(Collectors.joining(",", "fin d’indexation des dossiers ", ""));
        write(message);
    }

    public void onEmailArchived(ArchivageEmail archivageEmail) {
        String message = newMessageAboutEmail(archivageEmail, "Archivage");
        write(message);
    }

    public void onFolderArchived(Path remoteFolderPath) {
        write("Demande d’archivage du dossier " + remoteFolderPath);
    }

    public void onEmailRestored(ArchivageEmail archivageEmail) {
        String message = newMessageAboutEmail(archivageEmail, "Restauration");
        write(message);
    }

    public void onAttachmentSaved(ArchivageEmail archivageEmail, Path savedAttachmentPath) {
        String message = newMessageAboutEmail(archivageEmail, "Enregistrement de la pièce jointe dans " + savedAttachmentPath);
        write(message);
    }

    public void onInvalidFolderPath(String path) {
        String message = "le chemin du dossier « " + path + " » n'est pas compatible avec ce système d'exploitation.";
        write(message);
    }

    void write(String message) {
        LocalDate now = now().toLocalDate();
        Path todayLogPath = getTodayLogPath(now);
        LOGGER.debug("les journaux du jour sont écrits dans " + todayLogPath);
        String fullMessage =
                MessageFormat.format(
                        "{0} {1}{2}",
                        now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")),
                        message,
                        System.lineSeparator()
                );
        try {
            Files.writeString(
                    todayLogPath,
                    fullMessage,
                    StandardCharsets.UTF_8,
                    StandardOpenOption.APPEND,
                    StandardOpenOption.CREATE
            );
        } catch (IOException e) {
            LOGGER.warn("n’a pas pu écrire dans le journal fichier " + todayLogPath + " message " + message, e);
        }
    }

    /**
     * Le chemin vers le fichier dans lequel est écrit le journal du jour passé.
     */
    private Path getTodayLogPath(LocalDate day) {
        String format = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(day);
        String todayLogFileName = String.format("archivage_journal_%s.txt", format);
        Path todayLogPath = logsDirectoryPath.resolve(todayLogFileName);
        return todayLogPath;
    }

    public String getLogContent() {
        LocalDate now = now().toLocalDate();
        return getLogContent(now);
    }

    private String getLogContent(LocalDate day) {
        Path logPath = getTodayLogPath(day);
        try {
            return Files.readString(logPath);
        } catch (IOException e) {
            throw new RuntimeException("impossible de lire " + logPath, e);
        }
    }

    /**
     * Supprime les fichiers de journaux qui sont obsolètes par rapport à la durée de rétention.
     */
    public void clean() {
        List<Path> toBeDeletedFiles = getToBeDeletedLogFiles();
        if (toBeDeletedFiles.isEmpty()) {
            LOGGER.info("pas de journaux obsolètes à supprimer");
        } else {
            LOGGER.info("va supprimer " + toBeDeletedFiles.size() + " journaux obsolètes");
        }
        for (Path toBeDeletedFile : toBeDeletedFiles) {
            try {
                Files.delete(toBeDeletedFile);
                LOGGER.warn("a supprimé " + toBeDeletedFile);
            } catch (IOException e) {
                LOGGER.warn("impossible de supprimer " + toBeDeletedFile);
            }
        }
    }

    @VisibleForTesting
    List<Path> getToBeDeletedLogFiles() {
        Date findFilesBeforeDate = getDateToFindLogFilesToDelete();
        List<Path> toBeDeletedFiles = getToBeDeletedLogFiles(findFilesBeforeDate);
        return toBeDeletedFiles;
    }

    /**
     * Lister les fichiers plus vieux que la date passée.
     */
    private List<Path> getToBeDeletedLogFiles(Date findFilesBeforeDate) {
        try {
            AccumulatorPathVisitor visitor =
                    AccumulatorPathVisitor.withLongCounters(
                            FileFilterUtils.ageFileFilter(findFilesBeforeDate),
                            FileFilterUtils.trueFileFilter()
                    );
            Files.walkFileTree(logsDirectoryPath, visitor);
            return visitor.getFileList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Date getDateToFindLogFilesToDelete() {
        ZonedDateTime findFilesBeforeLocalDateTime = now().minusDays(configuration.getLoggingDaysOfRetention());
        Date findFilesBeforeDate = Date.from(findFilesBeforeLocalDateTime.toInstant());
        LOGGER.debug("date avant laquelle les journaux seront à supprimer " + findFilesBeforeDate);
        return findFilesBeforeDate;
    }

    private String newMessageAboutEmail(ArchivageEmail archivageEmail, String actionName) {
        String message = String.join(System.lineSeparator(),
                "Action : " + actionName,
                "Expéditeur : " + archivageEmail.from().address(),
                "À : " + archivageEmail.tos().stream().map(ArchivageEmail.Recipient::address).collect(Collectors.joining(", ")),
                "CC : " + archivageEmail.ccs().stream().map(ArchivageEmail.Recipient::address).collect(Collectors.joining(", ")),
                "Sujet : " + archivageEmail.subject(),
                "Date/heure : " + LocalDateTime.ofInstant(archivageEmail.sentDate().toInstant(), configuration.getZoneId()).format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"))
        );
        return message;
    }

    public void onEmailMissingSentDate(EmlFilePath emlFilePath) {
        write("ignore le fichier " + emlFilePath.toFullPath() + " car il n’y a pas de date d’expédition dans le message");
    }

    public void onErrorWhileReadingEmlFile(EmlFilePath emlFilePath, ErrorReadingEmlFileException errorReadingEmlFileException) {
        Throwable rootCause = Throwables.getRootCause(errorReadingEmlFileException);
        String errorMessage;
        if (rootCause instanceof java.io.UnsupportedEncodingException) {
            errorMessage = "encodage non géré (" + rootCause.getMessage() + ")";
        } else {
            errorMessage = rootCause.getMessage();
        }
        write("impossible de lire le fichier " + emlFilePath.toFullPath() + ". Erreur : " + errorMessage);
    }

    public void onEmailAlreadyExistsWhileSyncing(EmlFilePath emlFilePath) {
        write("Pendant la synchronisation, le fichier " + emlFilePath.toFullPath() + " existant déjà, il n’a pas été téléchargé");
    }

    public void onCancelledSynchronisation(ArchivageException e) {
        write(e.getMessage());
    }

    public void onSynchronisationError(String message) {
        write("Erreur pendant la synchronisation : " + message);
    }

    public void onEmailRestorationFailure(EmlFilePath emlFilePath, ArchivageZimbraException e) {
        write("Une erreur survenue pendant l’échange avec Zimbra a empêcher la restauration de " + emlFilePath.toFullPath() + ". Erreur : " + e.getMessage());
    }

    public void onEmailRestorationFailure(EmlFilePath emlFilePath, ArchivageStoreException e) {
        write("Une erreur survenue pendant la lecture du fichier a empêcher la restauration de " + emlFilePath.toFullPath() + ". Erreur : " + e.getCause().getMessage());
    }

    public void onMountPointRemoved(MountPoint mountPoint) {
        write(mountPoint.root() + " n’est plus un dossier en consultation");
    }
}
