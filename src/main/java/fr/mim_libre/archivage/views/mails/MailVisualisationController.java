package fr.mim_libre.archivage.views.mails;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.mim_libre.archivage.ArchivageEmail;
import fr.mim_libre.archivage.ArchivageService;
import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.store.EmlFilePath;
import fr.mim_libre.archivage.store.ErrorReadingEmlFileException;
import fr.mim_libre.archivage.views.EmailRenderingFormat;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.components.FakeTextField;
import fr.mim_libre.archivage.views.utils.NodeUtils;
import javafx.collections.FXCollections;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.apache.commons.io.FileSystem;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.remixicon.RemixiconAL;
import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.html.HTMLAnchorElement;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;

/**
 * Classe qui contrôle la visualisation d'un mail (contenu, destinataires, pièces jointes, etc.)
 */
public class MailVisualisationController {

    private static final Logger LOGGER = LogManager.getLogger(MailVisualisationController.class);

    private DateTimeFormatter dayFormatter;

    private DateTimeFormatter timeFormatter;

    @FXML
    private TextField fromLabel;

    @FXML
    private HBox tosContainer;

    @FXML
    private FlowPane tosTextFlow;

    @FXML
    private HBox ccsContainer;

    @FXML
    private FlowPane ccsTextFlow;

    @FXML
    private TextField subjectText;

    @FXML
    private TextArea bodyText;

    @FXML
    private WebView htmlBodyWebView;

    @FXML
    private Label sentDateDayText;

    @FXML
    private Label sentDateTimeText;

    @FXML
    private FlowPane attachmentsFlowPane;

    @FXML
    private Button saveAllAttachmentsButton;

    @FXML
    private Window window;

    @FXML
    private Button restoreButton;

    @FXML
    private Button fullDisplayButton;

    private Consumer<EmlFilePath> restoreEmailHandler;

    private Consumer<EmlFilePath> fullDisplayCallback;

    @FXML
    private ChoiceBox<EmailRenderingFormat> formatChoiceBox;

    private ArchivageEmail archivageEmail;

    /**
     * Initialise la vue avec les valeurs par défaut
     */
    @FXML
    public void initialize() {
        ZoneId zoneId = MainApplication.getConfiguration().getZoneId();
        dayFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(zoneId);
        timeFormatter = DateTimeFormatter.ofPattern("HH:mm").withZone(zoneId);
        formatChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                updateBodyView(newValue);
            }
        });
        htmlBodyWebView.getStyleClass().add("mail-content");
        makeWebViewOpenLinksInDesktopBrowser(htmlBodyWebView);
    }

    /**
     * Pour la {@link WebView} passée, intercepte désormais les clics sur des liens pour les ouvrir dans le navigateur.
     */
    private void makeWebViewOpenLinksInDesktopBrowser(WebView webView) {
        WebEngine engine = webView.getEngine();
        engine.getLoadWorker().stateProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue == Worker.State.SUCCEEDED) {
                Document document = engine.getDocument();
                if (document != null) {
                    NodeList nodeList = document.getElementsByTagName("a");
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        Node node = nodeList.item(i);
                        EventTarget eventTarget = (EventTarget) node;
                        eventTarget.addEventListener("click", evt -> {
                            EventTarget target = evt.getCurrentTarget();
                            HTMLAnchorElement anchorElement = (HTMLAnchorElement) target;
                            String href = anchorElement.getHref();
                            LOGGER.debug("a détecté un clic utilisateur sur un lien, va ouvrir " + href);
                            try {
                                URL url = new URL(href);
                                openUrlInBrowser(url);
                            } catch (URISyntaxException | MalformedURLException e) {
                                LOGGER.error("ne peut pas ouvrir l’URL cliquée par l’utilisateur " + href, e);
                            }
                            evt.preventDefault();
                        }, false);
                    }
                }
            }
        });
    }

    /**
     * Ouvre l’URL passée dans le navigateur de l’utilisateur.
     */
    private void openUrlInBrowser(URL url) throws URISyntaxException {
        try {
            FileSystem fileSystem = FileSystem.getCurrent();
            Runtime runtime = Runtime.getRuntime();
            switch (fileSystem) {
                case LINUX -> runtime.exec("xdg-open " + url.toExternalForm());
                case WINDOWS -> runtime.exec("rundll32 url.dll,FileProtocolHandler " + url.toExternalForm());
                default -> {
                    if (Desktop.isDesktopSupported()) {
                        if (Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                            Desktop.getDesktop().browse(url.toURI());
                        } else {
                            throw new RuntimeException("action " + Desktop.Action.BROWSE + " non gérée sur " + fileSystem);
                        }
                    } else {
                        throw new RuntimeException("API Desktop non gérée sur " + fileSystem);
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error("ne peut pas ouvrir l’URL cliquée par l’utilisateur " + url, e);
        }
    }

    /**
     * Met à jour l'affichage du corps de mail en fonction du format choisi.
     */
    private void updateBodyView(EmailRenderingFormat emailRenderingFormat) {
        switch (emailRenderingFormat) {
            case TEXT -> htmlBodyWebView.getEngine().loadContent(renderPlainTextAsHtml(archivageEmail.plainText()));
            case HTML -> htmlBodyWebView.getEngine().loadContent(archivageEmail.htmlText());
            case EML -> bodyText.setText(archivageEmail.emlSource());
            default -> throw new IllegalArgumentException("emailRenderingFormat = " + emailRenderingFormat);
        }
        NodeUtils.showNode(bodyText, emailRenderingFormat == EmailRenderingFormat.EML);
        NodeUtils.showNode(htmlBodyWebView, emailRenderingFormat != EmailRenderingFormat.EML);
    }

    /**
     * Prépare le contenu d’un message en texte brut pour être rendu dans une vue HTML.
     */
    private String renderPlainTextAsHtml(String plainText) {
        String linkified = linkify(plainText);
        String paragraphs = "<p>" + linkified.replaceAll("\\r?\\n\\r?\\n", "</p><p>") + "</p>";
        String withLineReturns = paragraphs.replaceAll("\\r?\\n", "<br>");
        return withLineReturns;
    }

    /**
     * Repère les liens dans le corps texte brut d’un message et remplace par des liens HTML.
     */
    public String linkify(String plainText) {
        LinkExtractor linkExtractor = LinkExtractor.builder()
                .linkTypes(EnumSet.of(LinkType.URL, LinkType.WWW))
                .build();
        Iterable<LinkSpan> linkSpans = linkExtractor.extractLinks(plainText);
        StringBuilder sb = new StringBuilder();
        int lastIndex = 0;
        for (LinkSpan linkSpan : linkSpans) {
            sb.append(plainText, lastIndex, linkSpan.getBeginIndex());
            String str = plainText.substring(linkSpan.getBeginIndex(), linkSpan.getEndIndex());
            String href = switch (linkSpan.getType()) {
                        case URL -> str;
                        case WWW -> "https://" + str;
                        default -> throw new IllegalStateException("type de lien " + linkSpan.getType() + " non géré détecté pour " + str);
            };
            String html = String.format("<a href=\"%s\">%s</a>", href, str);
            sb.append(html);
            lastIndex = linkSpan.getEndIndex();
        }
        if (lastIndex < plainText.length()) {
            sb.append(plainText, lastIndex, plainText.length());
        }
        String result = sb.toString();
        return result;
    }

    /**
     * Met à jour l'emlFilePath et affiche le mail
     */
    public void updatePane(EmlFilePath emlFilePath) {
        LOGGER.trace("afficher " + emlFilePath);
        if (emlFilePath == null) {
            resetView();
        } else {
            ArchivageService archivageService = MainApplication.getArchivageService();
            try {
                ArchivageEmail archivageEmail = archivageService.readEmail(emlFilePath);
                show(archivageEmail, true);
            } catch (ErrorReadingEmlFileException e) {
                resetView();
                throw new IllegalStateException("on ne devrait pas demander à afficher " + emlFilePath, e);
            }
        }
    }

    /**
     * Reset la vue en enlevant tout le contenu.
     */
    private void resetView() {
        fromLabel.setText("");
        tosTextFlow.getChildren().clear();
        ccsTextFlow.getChildren().clear();
        subjectText.setText("");
        sentDateDayText.setText("");
        sentDateTimeText.setText("");
        saveAllAttachmentsButton.setDisable(true);
        attachmentsFlowPane.getChildren().clear();
        restoreButton.setDisable(true);
        fullDisplayButton.setDisable(true);
        formatChoiceBox.setItems(FXCollections.emptyObservableList());
        formatChoiceBox.setDisable(true);
        htmlBodyWebView.getEngine().loadContent("");
        bodyText.setText("");
        NodeUtils.showNode(bodyText, true);
        NodeUtils.showNode(htmlBodyWebView, false);
    }

    /**
     * Rempli tous les champs de la vue à partir du mail sélectionné
     */
    public void show(ArchivageEmail archivageEmail, boolean showActionsMenu) {
        this.archivageEmail = archivageEmail;
        resetView();
        String from = archivageEmail.from().toHumanReadableString();
        fromLabel.setText(from);

        if (archivageEmail.tos().isEmpty()) {
            NodeUtils.showNode(tosContainer, false);
        } else {
            NodeUtils.showNode(tosContainer, true);
            for (ArchivageEmail.Recipient to : archivageEmail.tos()) {
                tosTextFlow.getChildren().add(new FakeTextField(to.toHumanReadableString(), 12));
            }
        }

        if (archivageEmail.ccs().isEmpty()) {
            NodeUtils.showNode(ccsContainer, false);
        } else {
            NodeUtils.showNode(ccsContainer, true);
            for (ArchivageEmail.Recipient to : archivageEmail.ccs()) {
                ccsTextFlow.getChildren().add(new FakeTextField(to.toHumanReadableString(), 12));
            }
        }

        subjectText.setText(archivageEmail.subject());
        Date sendDate = archivageEmail.sentDate();
        sentDateDayText.setText("le " + dayFormatter.format(sendDate.toInstant()));
        sentDateTimeText.setText(" à " + timeFormatter.format(sendDate.toInstant()));

        int attachmentsCount = archivageEmail.attachments().size();
        if (attachmentsCount == 0) {
            Text noAttachmentText = new Text("Aucune pièce jointe");
            attachmentsFlowPane.getChildren().add(noAttachmentText);
            saveAllAttachmentsButton.setDisable(true);
        } else {
            saveAllAttachmentsButton.setDisable(false);
        }
        archivageEmail.attachments().forEach(attachment -> {
            Button button = new Button(attachment.name());
            button.getStyleClass().addAll("rectangle-button-outline", "attachment-button");
            FontIcon attachmentIcon = new FontIcon(RemixiconAL.FILE_DOWNLOAD_LINE);
            button.setGraphic(attachmentIcon);
            // FlowPane.setMargin(button, new Insets(10));
            attachmentsFlowPane.getChildren().add(button);
            button.setOnAction(event -> {
                LOGGER.debug("il faut enregistrer " + attachment);
                saveAttachment(archivageEmail, attachment);
            });
        });

        if (showActionsMenu) {
            restoreButton.setDisable(false);
            fullDisplayButton.setDisable(false);
        } else {
            NodeUtils.showNode(restoreButton, false);
            NodeUtils.showNode(fullDisplayButton, false);
        }

        List<EmailRenderingFormat> emailRenderingFormats = new ArrayList<>();
        if (archivageEmail.htmlText() != null) {
            emailRenderingFormats.add(EmailRenderingFormat.HTML);
        }
        if (archivageEmail.plainText() != null) {
            emailRenderingFormats.add(EmailRenderingFormat.TEXT);
        }
        emailRenderingFormats.add(EmailRenderingFormat.EML);
        formatChoiceBox.setItems(FXCollections.observableArrayList(emailRenderingFormats));
        formatChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                updateBodyView(newValue);
            }
        });
        formatChoiceBox.getSelectionModel().selectFirst();
        formatChoiceBox.setDisable(emailRenderingFormats.size() == 1);
    }

    /**
     * Ouvre une fenêtre de navigation permettant d'enregistrer la pièce jointe
     */
    private void saveAttachment(ArchivageEmail archivageEmail, ArchivageEmail.Attachment attachment) {
        FileChooser chooser = new FileChooser();
        Path defaultAttachmentsSavingPath = MainApplication.getProfile().getDefaultAttachmentsSavingPath();
        chooser.setInitialDirectory(defaultAttachmentsSavingPath.toFile());
        chooser.setInitialFileName(attachment.name());
        String extension = FilenameUtils.getExtension(attachment.name());
        if (Strings.isNullOrEmpty(extension)) {
            // le fichier n'a pas d'extension, on ne peut pas déterminer un fichier à
            // choisir
        } else {
            chooser.getExtensionFilters()
                    .add(new FileChooser.ExtensionFilter("Fichier ." + extension, "*." + extension));
        }
        File file = chooser.showSaveDialog(window);
        if (file != null) {
            MainApplication.getArchivageService().saveAttachment(archivageEmail, attachment, file.toPath());
        }
    }

    public void setRestoreEmailHandler(Consumer<EmlFilePath> restoreEmailHandler) {
        this.restoreEmailHandler = restoreEmailHandler;
    }

    public void setFullDisplayCallback(Consumer<EmlFilePath> fullDisplayCallback) {
        this.fullDisplayCallback = fullDisplayCallback;
    }

    public void saveAllAttachments() {
        Path defaultAttachmentsSavingPath = MainApplication.getProfile().getDefaultAttachmentsSavingPath();
        File directory = defaultAttachmentsSavingPath.toFile();

        ButtonType btnCancel = new ButtonType("Annuler", ButtonBar.ButtonData.LEFT);
        ButtonType btnConfirm = new ButtonType("Confirmer");
		ButtonType btnChoose = new ButtonType("Choisir un autre dossier");

        String notificationConfirm = "Confirmer l'extraction des pièces jointes dans votre dossier " + directory;
        Alert toto = new CustomAlert(Alert.AlertType.INFORMATION, notificationConfirm, btnCancel, btnConfirm, btnChoose);
        Optional<ButtonType> option = toto.showAndWait();

        if (option.get() == btnChoose) {
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Choix d'un dossier d'enregistrement des pièces jointes");
            chooser.setInitialDirectory(defaultAttachmentsSavingPath.toFile());
            directory = chooser.showDialog(window);
        } else if (option.get() != btnConfirm) {
            return;
        }

        if (directory != null) {
            MainApplication.getArchivageService().saveAllAttachments(archivageEmail, directory.toPath());

            String notificationDone = "Vos pièces jointes ont été extraites dans votre dossier " + directory;
            Alert alert = new CustomAlert(Alert.AlertType.INFORMATION, notificationDone, ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML
    public void fullDisplay() {
        fullDisplayCallback.accept(archivageEmail.emlFilePath());
    }

    @FXML
    public void restore() {
        restoreEmailHandler.accept(archivageEmail.emlFilePath());
    }
}
