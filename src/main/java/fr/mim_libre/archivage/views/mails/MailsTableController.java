package fr.mim_libre.archivage.views.mails;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Iterables;
import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.configuration.ConfigurationFileContent;
import fr.mim_libre.archivage.configuration.Profile;
import fr.mim_libre.archivage.index.ArchivageQuery;
import fr.mim_libre.archivage.index.HasAnyAttachment;
import fr.mim_libre.archivage.index.IndexSearchResult;
import fr.mim_libre.archivage.index.MissingRelevantKeywordException;
import fr.mim_libre.archivage.store.EmlFilePath;
import fr.mim_libre.archivage.views.ControllerDoingSomethingOnApplicationClosing;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.remixicon.RemixiconAL;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Classe qui contrôle l'affichage du tableau de mails.
 */
public class MailsTableController implements ControllerDoingSomethingOnApplicationClosing {

    private static final Logger LOGGER = LogManager.getLogger(MailsTableController.class);

    @FXML
    private TableView<Model> mailsTableView;

    @FXML
    private Label mailsListTitleText;

    @FXML
    private TableColumn<Model, HasAnyAttachment> hasAnyAttachmentTableColumn;

    @FXML
    private TableColumn<Model, String> subjectTableColumn;

    @FXML
    private TableColumn<Model, String> fromTableColumn;

    @FXML
    private TableColumn<Model, Date> sentDateTableColumn;

    private BiMap<MailTableColumn, TableColumn<Model, ?>> tableColumns;

    private Consumer<EmlFilePath> mainActionHandler;

    /**
     * Initialise la vue avec ses colonnes, ses cellFactorys, ses préférences
     */
    @FXML
    public void initialize() {
        ObservableList<Model> items = FXCollections.observableList(new ArrayList<>());
        mailsTableView.setItems(items);
        tableColumns = ImmutableBiMap.<MailTableColumn, TableColumn<Model, ?>>builder()
                .put(MailTableColumn.FROM, fromTableColumn)
                .put(MailTableColumn.SUBJECT, subjectTableColumn)
                .put(MailTableColumn.SENT_DATE, sentDateTableColumn)
                .build();
        sentDateTableColumn.setCellFactory(getDateTimeCellFactory());
        hasAnyAttachmentTableColumn.setCellFactory(getHasAnyAttachmentTimeCellFactory());
        Profile.MailTableSortPreference mailTableSortPreference = MainApplication.getProfile().getMailTableSortPreference();
        if (mailTableSortPreference != null) {
            LOGGER.trace("conformément au profil utilisateur, on affiche par défaut la table triée selon " + mailTableSortPreference);
            MailTableColumn column = mailTableSortPreference.getColumn();
            mailsTableView.getSortOrder().add(tableColumns.get(column));
            mailsTableView.getSortOrder().get(0).setSortType(mailTableSortPreference.getSortType());
            mailsTableView.sort();
        }
        mailsTableView.setRowFactory(tv -> {
            TableRow<Model> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    Model rowData = row.getItem();
                    mainActionHandler.accept(rowData.emlFilePath);
                }
            });
            return row;
        });
        mailsTableView.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                Model selectedItem = mailsTableView.getSelectionModel().getSelectedItem();
                mainActionHandler.accept(selectedItem.emlFilePath);
            }
        });
        MainApplication.registerCloseListener(this);
    }

    /**
     * Méthode utilisée pour créer la cellFactory permettant d'afficher la date et l'heure d'envoi du mail,
     * formattée par nos soins.
     */
    private Callback<TableColumn<Model, Date>, TableCell<Model, Date>> getDateTimeCellFactory() {
        ZoneId zoneId = MainApplication.getConfiguration().getZoneId();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        Callback<TableColumn<Model, Date>, TableCell<Model, Date>> cellFactory = column -> {
            TableCell<Model, Date> cell = new TableCell<>() {
                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty || item == null) {
                        setText(null);
                    } else {
                        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(item.toInstant(), zoneId);
                        this.setText(dateTimeFormatter.format(zonedDateTime));
                    }
                }
            };
            return cell;
        };
        return cellFactory;
    }

    /**
     * Méthode utilisée pour créer la cellFactory permettant d'afficher une icône de pièce jointe si le mail en contient
     */
    private Callback<TableColumn<Model, HasAnyAttachment>, TableCell<Model, HasAnyAttachment>> getHasAnyAttachmentTimeCellFactory() {
        Callback<TableColumn<Model, HasAnyAttachment>, TableCell<Model, HasAnyAttachment>> cellFactory = column -> {
            TableCell<Model, HasAnyAttachment> cell = new TableCell<>() {
                @Override
                protected void updateItem(HasAnyAttachment item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty && item == HasAnyAttachment.SOME_ATTACHMENT) {
                        setGraphic(new FontIcon(RemixiconAL.ATTACHMENT_LINE));
                        setText(null);
                        getStyleClass().add("attachment-cell");
                    } else {
                        setText(null);
                        setGraphic(null);
                    }
                }
            };
            return cell;
        };
        return cellFactory;
    }

    /**
     * Permet de persister l’ordre de tri préféré de l'utilisateur à la fermeture de l’application
     */
    @Override
    public void onApplicationClose() {
        ObservableList<TableColumn<Model, ?>> orderColumns = mailsTableView.getSortOrder();
        ConfigurationFileContent.Profile.MailTableSortPreference mailTableSortPreference;
        if (orderColumns.isEmpty()) {
            LOGGER.trace("pas de colonne de tri");
            mailTableSortPreference = null;
        } else {
            TableColumn<Model, ?> tableColumn = Iterables.getOnlyElement(orderColumns);
            TableColumn.SortType sortType = tableColumn.getSortType();
            MailTableColumn mailTableColumn = tableColumns.inverse().get(tableColumn);
            LOGGER.trace("on a changé le tri pour " + mailTableColumn + " " + sortType);
            mailTableSortPreference = new ConfigurationFileContent.Profile.MailTableSortPreference();
            mailTableSortPreference.setColumn(mailTableColumn);
            mailTableSortPreference.setSortType(sortType);
        }
        String profileId = MainApplication.getProfile().getProfileId();
        MainApplication.getConfigurationManager().saveMailTableSortPreference(profileId, mailTableSortPreference);
    }

    /**
     * Méthode utilisée dans AppController pour transmettre l'information de l'EmlFilePath du mail sélectionné
     * au MailVisualisationController.
     */
    public void addEmailSelectedListener(Consumer<EmlFilePath> listener) {
        mailsTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            EmlFilePath emlFilePath = Optional.ofNullable(newValue)
                    .map(model -> model.emlFilePath)
                    .orElse(null);
            LOGGER.trace("on a sélectionné " + emlFilePath);
            listener.accept(emlFilePath);
        });
    }

    public void updateTableContent(String title, ArchivageQuery archivageQuery) throws MissingRelevantKeywordException {
        clearTableContent();
        ObservableList<Model> items = mailsTableView.getItems();
        mailsListTitleText.setText(title);
        try {
            MainApplication.getArchivageService().browse(archivageQuery).stream()
                    .map(Model::fromSearchResult)
                    .forEach(items::add);
            mailsTableView.sort();
            mailsTableView.getSelectionModel().selectFirst();
        } catch (MissingRelevantKeywordException e) {
            throw e;
        }
    }

    public void clearTableContent() {
        mailsTableView.getItems().clear();
    }

    public void setMainActionHandler(Consumer<EmlFilePath> mainActionHandler) {
        this.mainActionHandler = mainActionHandler;
    }

    public static class Model {

        private final EmlFilePath emlFilePath;

        private final SimpleStringProperty subject;

        private final SimpleStringProperty from;

        private final SimpleObjectProperty<Date> sentDate;

        private final SimpleObjectProperty<HasAnyAttachment> hasAnyAttachment;

        public Model(EmlFilePath emlFilePath, String subject, String from, Date sentDate, HasAnyAttachment hasAnyAttachment) {
            this.emlFilePath = emlFilePath;
            this.subject = new SimpleStringProperty(subject);
            this.from = new SimpleStringProperty(from);
            this.sentDate = new SimpleObjectProperty<>(sentDate);
            this.hasAnyAttachment = new SimpleObjectProperty<>(hasAnyAttachment);
        }

        public static Model fromSearchResult(IndexSearchResult indexSearchResult) {
            return new Model(
                    indexSearchResult.emlFilePath(),
                    indexSearchResult.subject(),
                    indexSearchResult.from(),
                    indexSearchResult.sentDate(),
                    indexSearchResult.hasAnyAttachment()
            );
        }

        public String getSubject() {
            return subject.get();
        }

        public String getFrom() {
            return from.get();
        }

        public Date getSentDate() {
            return sentDate.get();
        }

        public HasAnyAttachment getHasAnyAttachment() {
            return hasAnyAttachment.get();
        }
    }
}
