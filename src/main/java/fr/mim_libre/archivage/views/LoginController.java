package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.SessionStatus;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.utils.NodeUtils;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import fr.mim_libre.archivage.zimbra.AuthenticationFailedArchivageException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.remixicon.RemixiconAL;

/**
 * Classe qui contrôle l'affichage du composant de login et mot de passeŝ
 */
public class LoginController {

    private static final Logger LOGGER = LogManager.getLogger(LoginController.class);

    private String password = "";

    private Boolean passwordVisible = false;

    private FontIcon eyeOpen = new FontIcon(RemixiconAL.EYE_LINE);

    private FontIcon eyeClose = new FontIcon(RemixiconAL.EYE_OFF_LINE);

    @FXML
    private TextField loginTextField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button loginButton;

    @FXML
    private Label loginProfilIcon;

    @FXML
    private TextField passwordTextField;

    @FXML
    private Button showPasswordButton;

    @FXML
    public void initialize() {
        onSessionStatusChange(SessionStatus.DISCONNECTED);
        MainApplication.getZimbraSessionManager().addSessionListener(this::onSessionStatusChange);

        if (MainApplication.isProfileDefined()) {
            loginTextField.setText(MainApplication.getProfile().getLogin());
            loginTextField.setDisable(true);
        }

        passwordField.textProperty().addListener((obs, oldValue, newValue) -> {
            setPasswordFields(newValue, passwordTextField);
        });
        passwordTextField.textProperty().addListener((obs, oldValue, newValue) -> {
            setPasswordFields(newValue, passwordField);
        });
        NodeUtils.showNode(passwordTextField, false);
        showPasswordButton.setGraphic(eyeOpen);

    }

    /**
     * Met à jour la vue en fonction du statut de connexion
     */
    private void onSessionStatusChange(SessionStatus sessionStatus) {
        switch (sessionStatus) {
            case CONNECTED -> {
                loginButton.setDisable(true);
                showPasswordField(false);
                passwordField.setDisable(true);
                showPasswordButton.setDisable(true);
                loginProfilIcon.getStyleClass().remove("not-connected");
            }
            case DISCONNECTED ->  {
                loginButton.setDisable(false);
                passwordField.setDisable(false);
                showPasswordButton.setDisable(false);
                loginProfilIcon.getStyleClass().add("not-connected");
            }
            default -> throw new IllegalStateException("non-géré " + sessionStatus);
        }
    }

    /**
     * Authentification à la session zimbra
     */
    public void login() {
        try {
            String login = loginTextField.getText();
            MainApplication.getZimbraSessionManager().login(login, password);
        } catch (AuthenticationFailedArchivageException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Erreur d’authentification sur le serveur " + e.getHost() + " : " + e.getFaultReasonText(), ButtonType.OK);
            alert.showAndWait();
        } catch (ArchivageZimbraException e) {
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Impossible d’échanger avec Zimbra : " + e.getMessage(), ButtonType.OK);
            alert.showAndWait();
            LOGGER.warn("impossible d’échanger avec Zimbra", e);
        }
    }

    private void setPasswordFields(String newPassword, TextField textFieldToUpdate) {
        this.password = newPassword;
        textFieldToUpdate.setText(newPassword);
    }

    @FXML
    private void showPassword() {
        passwordVisible = !passwordVisible;
        showPasswordField(passwordVisible);
    }

    private void showPasswordField(Boolean show) {
        if (show) {
            NodeUtils.showNode(passwordField, false);
            NodeUtils.showNode(passwordTextField, true);
            showPasswordButton.setGraphic(eyeClose);
        } else {
            NodeUtils.showNode(passwordField, true);
            NodeUtils.showNode(passwordTextField, false);
            showPasswordButton.setGraphic(eyeOpen);
        }
    }
}
