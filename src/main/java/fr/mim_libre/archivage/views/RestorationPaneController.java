package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.SessionStatus;
import fr.mim_libre.archivage.store.ArchivageStoreException;
import fr.mim_libre.archivage.store.EmlFilePath;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.components.InfoLabel;
import fr.mim_libre.archivage.views.components.PaneController;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import fr.mim_libre.archivage.zimbra.ZimbraAuthToken;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Classe qui contrôle l'affichage du panneau latéral de restauration
 */
public class RestorationPaneController extends PaneController {

    private static final Logger LOGGER = LogManager.getLogger(RestorationPaneController.class);

    @FXML
    private Node restorationPane;

    @FXML
    private Button restoreButton;

    @FXML
    private Pane restorationMessagesContainer;

    private EmlFilePath emlFilePath;

    @FXML
    public void initialize() {
        updateRestoreButtonStatus(SessionStatus.DISCONNECTED);
        MainApplication.getZimbraSessionManager().addSessionListener(this::updateRestoreButtonStatus);
    }

    public void setEmlFilePath(EmlFilePath emlFilePath) {
        this.emlFilePath = emlFilePath;
    }

    /**
     * Méthode permettant d'activer/désactiver le bouton de restauration en fonction du statut de connexion
     */
    private void updateRestoreButtonStatus(SessionStatus sessionStatus) {
        restoreButton.setDisable(sessionStatus == SessionStatus.DISCONNECTED);
    }

    @Override
    public void cancel() {
        this.setEmlFilePath(null);
        super.cancel();
    }

    @Override
    public void show() {
        try {
            MainApplication.getZimbraSessionManager().updateStatus();
            super.show();
        } catch (ArchivageZimbraException e) {
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Impossible d’échanger avec Zimbra : " + e.getMessage(), ButtonType.OK);
            alert.showAndWait();
            LOGGER.warn("impossible d’échanger avec Zimbra", e);
        }
    }

    @Override
    public void hide() {
        this.setEmlFilePath(null);
        restorationMessagesContainer.getChildren().clear();
        super.hide();
    }

    public void restore() {
        restoreButton.setDisable(true);
        try {
            ZimbraAuthToken zimbraAuthToken = MainApplication.getZimbraSessionManager().getZimbraAuthToken();
            MainApplication.getArchivageService().restore(zimbraAuthToken, emlFilePath);
            InfoLabel errorLabel = new InfoLabel(InfoLabel.InfoLabelType.VALID, "Restauration terminée");
            restorationMessagesContainer.getChildren().add(errorLabel);
            restoreButton.setDisable(false);
        } catch (ArchivageZimbraException | ArchivageStoreException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Erreur pendant la restauration. Vous pouvez consulter les journaux pour en savoir plus.", ButtonType.OK);
            alert.showAndWait();
        }
    }

    @Override
    public Node getNode() {
        return restorationPane;
    }
}
