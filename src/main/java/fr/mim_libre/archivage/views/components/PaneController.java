package fr.mim_libre.archivage.views.components;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javafx.scene.Node;
import java.util.function.Consumer;

/**
 * Cette classe abstraite permet de mutualiser le code entre les différents panneaux latéraux de l'application.
 * Elle permet de les ouvrir, les fermer et leur passer une action à réaliser à ces moments-là.
 */
public abstract class PaneController {
    private Consumer<Boolean> onShowCb;

    /**
     * À implémenter par chaque enfant
     * @return le noeud racine du panneau
     */
    public abstract Node getNode();

    /**
     * Permet de définir l'action à réaliser au moment de la fermeture/ouverture du panneau
     * @param onShowCb : l'action à réaliser. Consomme un booléen indiquant si le panneau s'ouvre ou non
     */
    public void setOnShowCb(Consumer<Boolean> onShowCb) {
        this.onShowCb = onShowCb;
    }

    public boolean isVisible() {
        return this.getNode().isVisible();
    }

    public void show() {
        this.getNode().setVisible(true);
        onShowCb.accept(true);
    }

    public void hide() {
        this.getNode().setVisible(false);
        onShowCb.accept(false);
    }

    public void cancel() {
        this.hide();
    }
}
