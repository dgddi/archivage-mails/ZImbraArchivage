package fr.mim_libre.archivage.views.components;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.views.utils.NodeUtils;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.remixicon.RemixiconAL;


/**
 * Cette classe surcharge l'Alert native pour lui appliquer le style correspondant à l'application
 * Enlève également le header
 */
public class CustomAlert extends Alert {
    public CustomAlert(AlertType alertType) {
        super(alertType);
        this.setStyle(alertType, "");
    }

    public CustomAlert(AlertType alertType, String contentText, ButtonType... buttons) {
        super(alertType, contentText, buttons);
        this.setStyle(alertType, contentText);

    }

    private void setStyle(AlertType alertType, String contentText) {
        this.setHeaderText(null);
        this.setGraphic(null);
        DialogPane dialogPane = this.getDialogPane();
        dialogPane.getStylesheets().add(MainApplication.class.getResource("style/app.css").toExternalForm());
        dialogPane.getStyleClass().add("alert");

        NodeUtils.setAlertHeightAndWidth(dialogPane);

        Label textContainer = new Label(contentText);
        textContainer.getStyleClass().addAll("alert-text-container", "bigger-text");

        Label iconContainer = new Label("", this.getIconAccordingToAlertType(alertType));

        HBox dialogContent = new HBox(iconContainer, textContainer);
        dialogContent.getStyleClass().add("alert-content");
        dialogPane.setContent(dialogContent);

        for (ButtonType buttonType : dialogPane.getButtonTypes()) {
            dialogPane.lookupButton(buttonType).getStyleClass().add("rectangle-button");
        }
    }

    private FontIcon getIconAccordingToAlertType(AlertType alertType) {
        Ikon iconCode = null;
        String additionalStyle = "";

        switch (alertType) {
            case WARNING -> {
                iconCode = RemixiconAL.ERROR_WARNING_FILL;
                additionalStyle = "warning";
            }
            case ERROR -> {
                iconCode = RemixiconAL.CLOSE_CIRCLE_FILL;
                additionalStyle = "error";
            }
            default -> {
                iconCode = RemixiconAL.INFORMATION_FILL;
            }
        }
        FontIcon myIcon = new FontIcon(iconCode);
        myIcon.getStyleClass().addAll("alert-icon", additionalStyle);
        return myIcon;
    }
}
