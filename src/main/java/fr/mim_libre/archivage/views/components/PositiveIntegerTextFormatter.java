package fr.mim_libre.archivage.views.components;

import javafx.scene.control.TextFormatter;
import javafx.util.converter.IntegerStringConverter;

import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

/**
 * Un {@link TextFormatter} pour imposer à l’utilisateur de saisir un entier positif.
 */
public class PositiveIntegerTextFormatter extends TextFormatter<Integer> {

    private static final Pattern PATTERN = Pattern.compile("[1-9][0-9]*");

    private static final UnaryOperator<Change> POSITIVE_INTEGER_FILTER = change -> {
        String newText = change.getControlNewText();
        if (PATTERN.matcher(newText).matches()) {
            return change;
        }
        return null;
    };

    public PositiveIntegerTextFormatter() {
        super(new IntegerStringConverter(), null, POSITIVE_INTEGER_FILTER);
    }
}
