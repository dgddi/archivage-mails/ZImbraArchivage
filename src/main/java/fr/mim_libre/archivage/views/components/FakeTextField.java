package fr.mim_libre.archivage.views.components;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Window;

/**
 * Cette classe permet de créer du texte sélectionnable et cliquable.
 */
public class FakeTextField extends TextField {
    public FakeTextField(String newText, int fontSize) {
        super();

        getStyleClass().add("fake-text-field");
        setEditable(false);

        // Le faux text field ne prend pas la largeur du contenu
        // Pour l'instant c'est donc un hack qui calcule la largeur que prendrait le texte
        // et attribue cette taille au composant
        Text text = new Text(newText);
        text.setFont(new Font("Marianne", fontSize));
        this.setPrefWidth(text.getLayoutBounds().getWidth());
        this.setPrefHeight(text.getLayoutBounds().getHeight());
        setText(newText);
    }
}
