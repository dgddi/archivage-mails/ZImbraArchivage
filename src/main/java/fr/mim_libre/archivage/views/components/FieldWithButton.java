package fr.mim_libre.archivage.views.components;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.HBox;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.javafx.FontIcon;

/**
 * Cette classe n'est plus utilisée dans l'application.
 * Elle permet la création d'un Label associé à un bouton
 */
public class FieldWithButton extends HBox {
    public FieldWithButton(String text, Ikon iconCode, FieldWithButtonAction actionType) {
        super();
        Label label = new Label(text);
        FontIcon icon = new FontIcon(iconCode);
        Button button =new Button();
        button.setGraphic(icon);
        switch (actionType) {
            case COPY -> button.setOnAction(actionEvent -> copyToClipboard(text));
        }
        getStyleClass().add("field-with-button");
        getChildren().addAll(label, button);
    }

    public enum FieldWithButtonAction {
        COPY
    }

    /**
     * La fonction appelée si le type d'action choisie est COPY
     * @param textToCopy le texte à copier dans le presse-papier
     */
    private void copyToClipboard(String textToCopy) {
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putString(textToCopy);
        clipboard.setContent(content);
    }
}
