package fr.mim_libre.archivage.views.components;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.views.utils.NodeUtils;
import javafx.concurrent.Task;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import org.controlsfx.dialog.ProgressDialog;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.remixicon.RemixiconMZ;

/**
 * Cette classe surcharge le Dialogue de Progression pour lui appliquer le style correspondant à l'application
 */
public class CustomProgressDialog extends ProgressDialog {

    private final Task<Void> task;

    public CustomProgressDialog(Task<Void> task, String dialogSubtitle) {
        super(task);
        this.task = task;
        this.setTitle("Tâche en cours");
        this.setContentText("Cette action peut prendre un peu de temps, merci de ne pas fermer l'application.");
        DialogPane dialogPane = this.getDialogPane();
        dialogPane.getStylesheets().add(MainApplication.class.getResource("style/app.css").toExternalForm());
        dialogPane.getStyleClass().add("alert");
        dialogPane.getContent().getStyleClass().add("alert-content");

        NodeUtils.setAlertHeightAndWidth(dialogPane);

        FontIcon myIcon = new FontIcon(RemixiconMZ.STACK_FILL);
        myIcon.getStyleClass().add("alert-icon");
        Label subtitleContainer = new Label(dialogSubtitle);
        subtitleContainer.getStyleClass().addAll("title", "alert-header");
        subtitleContainer.setGraphic(myIcon);
        dialogPane.setHeader(subtitleContainer);
    }

    public void addCancelButton() {
        getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        Button button = (Button) getDialogPane().lookupButton(ButtonType.CANCEL);
        button.setOnAction(event -> task.cancel(false));
    }
}
