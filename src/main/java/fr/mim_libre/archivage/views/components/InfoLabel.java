package fr.mim_libre.archivage.views.components;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javafx.scene.control.Label;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.remixicon.RemixiconAL;

/**
 * Créé un Label associé à une icône.
 * Le style du label et de l'icône associée dépendent dy type choisi
 */
public class InfoLabel extends Label {
    public enum InfoLabelType {
        ERROR,
        WARNING,
        VALID,
        INFORMATION
    }

    public InfoLabel(String text) {
        super(text);
        this.setStyleAccordingToType(InfoLabelType.INFORMATION);
    }

    public InfoLabel(InfoLabelType infoLabelType, String text) {
        super(text);
        this.setStyleAccordingToType(infoLabelType);
    }
    private void setStyleAccordingToType(InfoLabelType infoLabelType) {
        Ikon iconCode = null;
        String additionalStyle = "";

        switch (infoLabelType) {
            case WARNING -> {
                iconCode = RemixiconAL.ERROR_WARNING_LINE;
                additionalStyle = "warning";
            }
            case ERROR -> {
                iconCode = RemixiconAL.CLOSE_CIRCLE_LINE;
                additionalStyle = "error";
            }
            case VALID -> {
                iconCode = RemixiconAL.CHECK_LINE;
                additionalStyle= "valid";
            }
            default -> {
                iconCode = RemixiconAL.INFORMATION_LINE;
            }
        }

        FontIcon myIcon = new FontIcon(iconCode);
        myIcon.getStyleClass().addAll("alert-icon", additionalStyle);
        this.getStyleClass().addAll("info-label", additionalStyle);
        this.setGraphic(myIcon);
    }
}

