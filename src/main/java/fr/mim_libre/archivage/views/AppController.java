package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.mim_libre.archivage.ArchivageEmail;
import fr.mim_libre.archivage.ArchivageService;
import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.configuration.ConfigurationManager;
import fr.mim_libre.archivage.index.ArchivageQuery;
import fr.mim_libre.archivage.index.IndexationRequest;
import fr.mim_libre.archivage.index.IndexationTask;
import fr.mim_libre.archivage.index.MissingRelevantKeywordException;
import fr.mim_libre.archivage.store.EmlFilePath;
import fr.mim_libre.archivage.store.ErrorReadingEmlFileException;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.components.CustomProgressDialog;
import fr.mim_libre.archivage.views.components.PaneController;
import fr.mim_libre.archivage.views.files.FilesManagerController;
import fr.mim_libre.archivage.views.mails.MailVisualisationController;
import fr.mim_libre.archivage.views.mails.MailsTableController;
import fr.mim_libre.archivage.views.utils.FilesManagerView;
import fr.mim_libre.archivage.views.utils.NodeUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kordamp.ikonli.javafx.StackedFontIcon;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * Classe qui contrôle la vue racine de l'application principale
 */
public class AppController extends GridPane implements ControllerThatCanShowMailTable, ControllerDoingSomethingOnApplicationClosing, ControllerDoingSomethingOnApplicationShowing {

    private static final Logger LOGGER = LogManager.getLogger(AppController.class);

    @FXML
    private ImageView logoImageView;

    @FXML
    private SplitPane splitPane;

    @FXML
    private MailsTableController mailsTableController;

    @FXML
    private MailVisualisationController mailVisualisationController;

    @FXML
    private FilesManagerController filesManagerController;

    @FXML
    private SynchronisationPaneController synchronisationPaneController;

    @FXML
    private RestorationPaneController restorationPaneController;

    @FXML
    private ProfilePaneController profilePaneController;

    @FXML
    private SettingsPaneController settingsPaneController;

    @FXML
    private Label lastSynchronisationDayText;

    @FXML
    private Label lastSynchronisationTimeText;

    @FXML
    private VBox menuProfileItem;

    @FXML
    private VBox menuSettingsItem;

    @FXML
    private VBox menuSynchroItem;

    @FXML
    private StackedFontIcon synchroButton;

    @FXML
    private SplitPane mainSectionsContainer;

    @FXML
    private VBox titleContainer;

    /**
     * Dernier dossier sélectionné par l’utilisateur.
     *
     * Utile pour gérer le fait que quand l’utilisateur bascule de la recherche vers l’arborescence,
     * on peut réafficher le dossier qui était sélectionné.
     */
    private Folder lastFolderSelected;

    @FXML
    public void initialize() {
        logoImageView.setImage(MainApplication.getApplicationImage());

        // Pour que l'événement de clic passe jusqu'au bouton sous les items
        menuSynchroItem.setMouseTransparent(true);
        menuSettingsItem.setMouseTransparent(true);
        menuProfileItem.setMouseTransparent(true);

        // Passe le callback permettant de mettre à jour l'item sélectionné du menu
        settingsPaneController.setOnShowCb(show -> updateSelectedItemAndFocusPane(show, menuSettingsItem));
        synchronisationPaneController.setOnShowCb(show -> updateSelectedItemAndFocusPane(show, menuSynchroItem));
        profilePaneController.setOnShowCb(show -> updateSelectedItemAndFocusPane(show, menuProfileItem));
        restorationPaneController.setOnShowCb(show -> updateSelectedItemAndFocusPane(show, null));

        profilePaneController.setMountPointsListener(filesManagerController);
        profilePaneController.setIndexationRequestHandler(this::handleIndexationRequest);
        mailVisualisationController.setRestoreEmailHandler(this::toggleRestorationPane);
        mailVisualisationController.setFullDisplayCallback(this::fullDisplay);
        mailsTableController.addEmailSelectedListener(mailVisualisationController::updatePane);
        mailsTableController.setMainActionHandler(this::fullDisplay);

        synchronisationPaneController.setOnSynchronisationTerminatedListener((mountPoint, lastSynchronisation) -> {
            updateLastSynchronisationTexts(lastSynchronisation);
            filesManagerController.onMountPointContentUpdated(mountPoint);
        });
        filesManagerController.setControllerThatCanShowSearchResultsInTableView(this);
        filesManagerController.setIndexationRequestHandler(this::handleIndexationRequest);
        filesManagerController.selectFirstItem();

        
        setApplicationTitles();

        ZonedDateTime lastSynchronisation = MainApplication.getProfile().getLastSynchronisation();
        updateLastSynchronisationTexts(lastSynchronisation);

        MainApplication.registerCloseListener(this);
        MainApplication.registerShowListener(this);
    }

    private void handleIndexationRequest(IndexationRequest indexationRequest) {
        ArchivageService archivageService = MainApplication.getArchivageService();
        IndexationTask indexationTask = MainApplication.startIndexationTask(archivageService, indexationRequest);
        CustomProgressDialog progressDialog = new CustomProgressDialog(indexationTask, indexationRequest.description());
        progressDialog.showAndWait();
        indexationTask.getCancellationMessages().forEach((mountPoint, errorMessage) -> {
            Alert alert = new CustomAlert(
                    Alert.AlertType.ERROR,
                    "L’indexation du dossier " + mountPoint + " a échoué. Erreur : " + errorMessage,
                    ButtonType.OK
            );
            alert.showAndWait();
        });
        indexationRequest.mountPoints().forEach(filesManagerController::onMountPointContentUpdated);
    }

    private void fullDisplay(EmlFilePath emlFilePath) {
        ArchivageService archivageService = MainApplication.getArchivageService();
        try {
            ArchivageEmail archivageEmail = archivageService.readEmail(emlFilePath);
            FXMLLoader fxmlLoader = new FXMLLoader(MailVisualisationController.class.getResource("/fr/mim_libre/archivage/mails/mail-visualisation-view.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            MailVisualisationController controller = fxmlLoader.getController();
            controller.show(archivageEmail, false);
            scene.getStylesheets().add(MainApplication.class.getResource("style/app.css").toExternalForm());
            Stage stage = new Stage();
            stage.setTitle(archivageEmail.subject());
            stage.setMaximized(true);
            Rectangle2D bounds = Screen.getPrimary().getBounds();
            stage.setHeight(bounds.getHeight() * 0.8);
            stage.setWidth(bounds.getWidth() * 0.8);
            stage.setMinHeight(bounds.getHeight() * 0.5);
            stage.setMinWidth(bounds.getWidth() * 0.5);
            stage.getIcons().add(MainApplication.getApplicationImage());
            stage.setScene(scene);
            stage.show();
        } catch (ErrorReadingEmlFileException e) {
            throw new IllegalStateException("on ne devrait pas demander à afficher " + emlFilePath, e);
        } catch (IOException e) {
            LOGGER.error("Erreur après l'ouverture de la popup d'agrandissement :", e);
            throw new RuntimeException(e);
        }
    }

    @FXML
    private void setApplicationTitles() {
        String applicationName = MainApplication.getConfiguration().getApplicationName();
        Label applicationLabel = new Label(applicationName);

        String specificClientName = MainApplication.getConfiguration().getSpecificClientName();
        if (StringUtils.isEmpty(specificClientName)) {
            applicationLabel.getStyleClass().addAll("side-menu-title", "bigger-text");
        } else {
            Label label = new Label(specificClientName);
            label.getStyleClass().add("side-menu-title");
            titleContainer.getChildren().add(label);
            applicationLabel.getStyleClass().addAll("side-menu-subtitle", "bigger-text");
        }

        titleContainer.getChildren().add(applicationLabel);
    }

    @FXML
    public void toggleProfilePane() {
        this.togglePane(profilePaneController);
    }

    @FXML
    private void toggleSynchronisationPane() {
        this.togglePane(synchronisationPaneController);
    }

    @FXML
    private void toggleSettingsPane() {
        this.togglePane(settingsPaneController);
    }

    private void toggleRestorationPane(EmlFilePath emlFilePath) {
        restorationPaneController.setEmlFilePath(emlFilePath);
        this.togglePane(restorationPaneController);
    }

    /**
     * Méthode permettant d'afficher/cacher un panneau latéral
     * @param pane controller du panneau à afficher/cacher
     */
    private void togglePane(PaneController pane) {
        Set<PaneController> allPanes = Set.of(profilePaneController,
                settingsPaneController,
                synchronisationPaneController,
                restorationPaneController);
        allPanes.stream()
                .filter(aPane -> !aPane.equals(pane))
                .forEach(PaneController::hide);
        if (pane.isVisible()) {
            pane.hide();
        } else {
            pane.show();
        }
    }

    /**
     * Méthode permettant de sélectionner/désélectionner un item du menu. Et de disable/enable le reste de l'application.
     * À l'ouverture d'un panneau, le reste de l'application ne doit pas pouvoir être focusable ou sélectionnable.
     * @param show boolean indiquant si le panneau est ouvert
     * @param menuItem item du menu concerné por l'ouverture/fermeture du panneau
     */
    private void updateSelectedItemAndFocusPane(Boolean show, VBox menuItem) {
        mainSectionsContainer.setDisable(show);
        if (menuItem != null) {
            NodeUtils.toggleStyleClass(menuItem, "selected", show);
        }
    }

    /**
     * Met à jour les informations autour de la dernière synchronisation
     */
    private void updateLastSynchronisationTexts(ZonedDateTime lastSynchronisation) {
        synchroButton.getStyleClass().clear();
        synchroButton.getStyleClass().addAll("round-button", "synchro-button");

        // S'il n'y a jamais de synchronisation, on l'indique
        if (lastSynchronisation == null) {
            lastSynchronisationDayText.setText("Jamais");
            lastSynchronisationTimeText.setText("");
            synchroButton.getStyleClass().add("obsolete");
        } else {
            lastSynchronisationDayText.setText(lastSynchronisation.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            lastSynchronisationTimeText.setText(lastSynchronisation.format(DateTimeFormatter.ofPattern("HH:mm")));
            ZonedDateTime now = MainApplication.getConfiguration().now();
            // Si la synchronisation commence à dater, on applique le style qui va bien
            if (now.isAfter(lastSynchronisation.plusDays(MainApplication.getConfiguration().maxDaysForUpToDateSynchronization()))
                    && now.isBefore(lastSynchronisation.plusDays(MainApplication.getConfiguration().maxDaysForAgingSynchronization()))) {
                synchroButton.getStyleClass().add("aging");
            }
            // Si la synchronisation est trop vieille, on applique le style qui va bien
            else if (now.isAfter(lastSynchronisation.plusDays(MainApplication.getConfiguration().maxDaysForAgingSynchronization()))) {
                synchroButton.getStyleClass().add("obsolete");
            }
        }
    }

    @Override
    public void showFolderContentInTableView(Folder folder) {
        lastFolderSelected = folder;
        LOGGER.debug("il faut maintenant montrer le contenu de " + folder);
        String title = folder.isRoot() ?
                "Dossier « " + folder.mountPoint().root().toString() + " »"
                : "Dossier « " + folder.getHumanReadableName() + " »";
        ArchivageQuery archivageQuery = ArchivageQuery.forFolder(folder);
        try {
            mailsTableController.updateTableContent(title, archivageQuery);
        } catch (MissingRelevantKeywordException e) {
            LOGGER.error("ne devrait jamais arriver", e);
            throw new IllegalStateException("ne devrait jamais arrivé", e);
        }
    }

    @Override
    public void showSearchResultsInTableView(ArchivageQuery archivageQuery) throws MissingRelevantKeywordException {
        mailsTableController.updateTableContent("Résultats de la recherche", archivageQuery);
    }

    @Override
    public void changeView(FilesManagerView chosenView) {
        switch (chosenView) {
            case TREE -> {
                if (lastFolderSelected == null) {
                    // possible si dès l'ouverture de l'appli, on a jamais sélectionné de dossier
                    mailsTableController.clearTableContent();
                } else {
                    showFolderContentInTableView(lastFolderSelected);
                }
            }
            case SEARCH -> mailsTableController.clearTableContent();
            default -> throw new IllegalStateException("Unexpected value: " + chosenView);
        }
    }

    @Override
    public void onApplicationClose() {
        String profileId = MainApplication.getProfile().getProfileId();
        ConfigurationManager configurationManager = MainApplication.getConfigurationManager();
        double dividerPosition = splitPane.getDividerPositions()[0];
        configurationManager.saveTableToVisualisationSplitPaneDividerPosition(profileId, dividerPosition);
        double mainDividerPosition = mainSectionsContainer.getDividerPositions()[0];
        configurationManager.saveFilesManagerToMailsSplitPaneDividerPosition(profileId, mainDividerPosition);
    }

    @Override
    public void onApplicationShow() {
        // restauration de la position du séparateur tel qu’il était à la fermeture de l’application
        double tableToVisualisationSplitPaneDividerPosition = MainApplication.getProfile().getTableToVisualisationSplitPaneDividerPosition();
        splitPane.setDividerPositions(tableToVisualisationSplitPaneDividerPosition);

        double filesManagerToMailsSplitPaneDividerPosition = MainApplication.getProfile().getFilesManagerToMailsSplitPaneDividerPosition();
        mainSectionsContainer.setDividerPositions(filesManagerToMailsSplitPaneDividerPosition);
    }
}
