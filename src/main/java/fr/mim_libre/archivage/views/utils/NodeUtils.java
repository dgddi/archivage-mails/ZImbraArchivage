package fr.mim_libre.archivage.views.utils;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;

public class NodeUtils {
    /**
     * Méthode utilitaire permettant de rendre un noeud invisible
     */
    static public void showNode(Node node, boolean show) {
        if (show) {
            node.setVisible(true);
            node.setManaged(true);
        } else {
            node.setVisible(false);
            node.setManaged(false);
        }
    }

    /**
     * Méthode utilitaire permettant d'ajouter ou retirer une classe
     *
     */
    static public void toggleStyleClass(Node node, String className, boolean add) {
        if (add) {
            node.getStyleClass().add(className);
        } else {
            node.getStyleClass().remove(className);
        }
    }

    /**
     * Méthode utilitaire permettant de mutualiser l'attribution taille des popups
     *
     */
    static public void setAlertHeightAndWidth(Pane dialogPane) {
        Rectangle2D bounds = Screen.getPrimary().getBounds();
        double boundsWidth = bounds.getWidth();
        double boundsHeight = bounds.getHeight();
        dialogPane.setMinWidth(boundsWidth * 0.2);
        dialogPane.setMinHeight(boundsHeight * 0.2);
        dialogPane.setMaxWidth(boundsWidth * 0.6);
        dialogPane.setMaxHeight(boundsHeight * 0.6);
    }
}
