package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.ArchivageException;
import fr.mim_libre.archivage.ArchivageService;
import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.SessionStatus;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.components.CustomProgressDialog;
import fr.mim_libre.archivage.views.components.PaneController;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import fr.mim_libre.archivage.zimbra.ZimbraAuthToken;
import fr.mim_libre.archivage.zimbra.ZimbraService;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * Classe qui contrôle l'affichage du panneau latéral de synchronisation
 */
public class SynchronisationPaneController extends PaneController {

    private static final Logger LOGGER = LogManager.getLogger(SynchronisationPaneController.class);

    @FXML
    private Node synchronisationPane;

    @FXML
    private CheckBox archiveAfterDaysCountCheckBox;

    @FXML
    private CheckBox taggedCheckBox;

    @FXML
    private Button startSynchronisationButton;

    private BiConsumer<MountPoint,  ZonedDateTime> onSynchronisationTerminatedListener;

    @FXML
    public void initialize() {
        updateTexts();

        // À l'initialisation on commence par désactiver les actions possibles
        startSynchronisationButton.setDisable(true);
        archiveAfterDaysCountCheckBox.setDisable(true);
        taggedCheckBox.setDisable(true);

        ChangeListener<Boolean> enableStartSynchronisation = (observable, oldValue, newValue) -> updateStartSynchronisationButtonEnabledState(MainApplication.getZimbraSessionManager().getSessionStatus());

        archiveAfterDaysCountCheckBox.selectedProperty().addListener(enableStartSynchronisation);
        taggedCheckBox.selectedProperty().addListener(enableStartSynchronisation);

        MainApplication.getZimbraSessionManager().addSessionListener(this::updateButtonsEnableState);
    }

    @Override
    public Node getNode() {
        return synchronisationPane;
    }

    /**
     * Rend les actions possibles si l'utilisateur est connecté
     */
    private void updateButtonsEnableState(SessionStatus sessionStatus) {
        boolean connected = sessionStatus == SessionStatus.CONNECTED;
        taggedCheckBox.setDisable(!connected);
        archiveAfterDaysCountCheckBox.setDisable(!connected);
        this.updateStartSynchronisationButtonEnabledState(sessionStatus);
    }

    /**
     * Active ou désactive le bouton de synchronisation en fonction du statut de connexion et du remplissage du formulaire
     */
    private void updateStartSynchronisationButtonEnabledState(SessionStatus sessionStatus) {
        boolean archiveAfterDaysCount = archiveAfterDaysCountCheckBox.isSelected();
        boolean archivedTagged = taggedCheckBox.isSelected();
        boolean somethingToDo = archiveAfterDaysCount || archivedTagged;
        boolean connected = sessionStatus == SessionStatus.CONNECTED;
        boolean enable = somethingToDo && connected;
        startSynchronisationButton.setDisable(!enable);
    }

    private int getRemoteEmailsMustBeArchivedAfterDaysCount() {
        return MainApplication.getProfile().getRemoteEmailsMustBeArchivedAfterDaysCount();
    }

    private String getEmailToBeArchivedTag() {
        return MainApplication.getConfiguration().getEmailToBeArchivedTag();
    }

    public void setOnSynchronisationTerminatedListener(BiConsumer<MountPoint, ZonedDateTime> onSynchronisationTerminatedListener) {
        this.onSynchronisationTerminatedListener = onSynchronisationTerminatedListener;
    }

    /**
     * Confirme le lancement de la synchronization
     */
    public void confirmStartSynchronisation() {
        boolean archiveAfterDaysCount = archiveAfterDaysCountCheckBox.isSelected();
        boolean archivedTagged = taggedCheckBox.isSelected();

        if (!archiveAfterDaysCount && !archivedTagged) { return; }

        String message = getStartSynchronisationConfirmationMessage(archiveAfterDaysCount, archivedTagged);

        Alert alert = new CustomAlert(Alert.AlertType.CONFIRMATION, message, ButtonType.CANCEL, ButtonType.APPLY);
        Optional<ButtonType> alertOutcome = alert.showAndWait();

        if (alertOutcome.isPresent() && alertOutcome.get() == ButtonType.APPLY) {
            startSynchronisation();
        }
    }

    private String getStartSynchronisationConfirmationMessage(boolean archiveAfterDaysCount, boolean archivedTagged) {
        String message = "Vous êtes sur le point de lancer l'archivage des :\n\n";

        ZonedDateTime now = MainApplication.getConfiguration().now();

        if (archiveAfterDaysCount) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(now.getZone());
            Instant instant = now.minus(Duration.ofDays(getRemoteEmailsMustBeArchivedAfterDaysCount())).toInstant();

            message += "- courriels datés de plus de " + getRemoteEmailsMustBeArchivedAfterDaysCount() + " jours (" + formatter.format(instant) + ")";

            if (archivedTagged) {
                message += ",\n\n";
            } else {
                message += ".\n\n";
            }
        }

        if (archivedTagged) {
            message += "- courriels libellés « " + getEmailToBeArchivedTag() + " ».\n\n";
        }

        message += "Voulez-vous procéder à l'archivage ?";
        return message;
    }

    /**
     * Lance la sychronisation en fonction de l'option sélectionnée
     */
    public void startSynchronisation() {
        boolean archiveAfterDaysCount = archiveAfterDaysCountCheckBox.isSelected();
        boolean archivedTagged = taggedCheckBox.isSelected();

        MountPoint targetMountPoint = MainApplication.getProfile().getSynchronizationMountPoint();
        ZimbraAuthToken zimbraAuthToken = MainApplication.getZimbraSessionManager().getZimbraAuthToken();
        Set<ZimbraService.SearchRequest.Query> queries = new LinkedHashSet<>();
        // Si on doit synchroniser les mails en fonction de leur date d'envoi
        if (archiveAfterDaysCount) {
            Duration duration = Duration.ofDays(getRemoteEmailsMustBeArchivedAfterDaysCount());
            ZimbraService.SearchRequest.Query query =
                    ZimbraService.SearchRequest.Query.searchOlderThanDuration(duration);
            queries.add(query);
        }
        // Si on doit synchroniser les mails qui comportent le tag d'archivage
        if (archivedTagged) {
            ZimbraService.SearchRequest.Query query =
                    ZimbraService.SearchRequest.Query.searchTagged(getEmailToBeArchivedTag());
            queries.add(query);
        }

        // Lance une boîte de dialogue indiquant la progression de la tâche de synchronisation
        String profileId = MainApplication.getProfile().getProfileId();
        ArchivageService archivageService = MainApplication.getArchivageService();
        SynchronisationTask synchronisationTask = new SynchronisationTask(zimbraAuthToken, queries, targetMountPoint, profileId, archivageService);
        synchronisationTask.stateProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == Worker.State.FAILED) {
                LOGGER.error("erreur pendant l’exécution de " + synchronisationTask, synchronisationTask.getException());
            }
        });
        CustomProgressDialog progressDialog = new CustomProgressDialog(synchronisationTask, "Synchronisation en cours");
        progressDialog.addCancelButton();
        hide();
        Thread thread = new Thread(synchronisationTask);
        thread.setUncaughtExceptionHandler((t, e) -> LOGGER.error("erreur pendant l’exécution de " + synchronisationTask, e));
        thread.start();
        progressDialog.showAndWait();

        if (synchronisationTask.getCancelledSynchronisationMessage() != null) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, synchronisationTask.getCancelledSynchronisationMessage(), ButtonType.OK);
            alert.showAndWait();
        }

        String errorsSummary = switch (synchronisationTask.getErrorsCount()) {
            case 0 -> "Aucune erreur n’est survenue pendant la synchronisation.";
            case 1 -> "Une erreur est survenue pendant la synchronisation.\nVous pouvez consulter les journaux pour en savoir plus.";
            default -> synchronisationTask.getErrorsCount() + " erreurs sont survenues pendant la synchronisation.\nVous pouvez consulter les journaux pour en savoir plus.";
        };

        switch (synchronisationTask.getInvalidFolderPathsCount()) {
            case 0 -> {}
            case 1 -> {
                errorsSummary += "\nUn dossier a un chemin incompatible avec votre système d'exploitation."
                    + "\nConsulter les journaux pour plus d'informations.";
            }
            default -> {
                errorsSummary += "\n"
                    + synchronisationTask.getInvalidFolderPathsCount()
                    + " dossiers ont un chemin incompatible avec votre système d'exploitation."
                    + "\nConsulter les journaux pour plus d'informations.";
            }
        }

        Alert.AlertType alertType = synchronisationTask.getErrorsCount() > 0 ? Alert.AlertType.WARNING : Alert.AlertType.INFORMATION;
        Alert alert = new CustomAlert(alertType, synchronisationTask.getProgressMessage() + ".\n" + errorsSummary, ButtonType.OK);
        alert.showAndWait();

        if (synchronisationTask.getTerminatedAfterStartAt() != null) {
            onSynchronisationTerminatedListener.accept(targetMountPoint, synchronisationTask.getTerminatedAfterStartAt());
        }
    }

    private void updateTexts() {
        archiveAfterDaysCountCheckBox.setText("Courriels datés de plus de " + getRemoteEmailsMustBeArchivedAfterDaysCount() + " jours");
        taggedCheckBox.setText("Courriels libellés « " + getEmailToBeArchivedTag() + " »");
    }

    @Override
    public void show() {
        updateTexts();
        try {
            MainApplication.getZimbraSessionManager().updateStatus();
            super.show();
        } catch (ArchivageZimbraException e) {
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Impossible d’échanger avec Zimbra : " + e.getMessage(), ButtonType.OK);
            alert.showAndWait();
            LOGGER.warn("impossible d’échanger avec Zimbra", e);
        }
    }

    private class SynchronisationTask extends Task<Void> implements ArchivageService.SynchronisationProgressHandler {

        private final ZimbraAuthToken zimbraAuthToken;

        private final Set<ZimbraService.SearchRequest.Query> queries;

        private final MountPoint targetMountPoint;

        private final String profileId;

        private final ArchivageService archivageService;

        private int toBeDone;

        private int done;

        private long sizeToBeDownloaded;

        private long sizeDownloaded;

        private int errorsCount = 0;

        private String cancelledSynchronisationMessage;

        private ZonedDateTime terminatedAfterStartAt;

        private Set<String> invalidFolderPaths = new LinkedHashSet<>();

        private SynchronisationTask(ZimbraAuthToken zimbraAuthToken, Set<ZimbraService.SearchRequest.Query> queries, MountPoint targetMountPoint, String profileId, ArchivageService archivageService) {
            this.zimbraAuthToken = zimbraAuthToken;
            this.queries = queries;
            this.targetMountPoint = targetMountPoint;
            this.profileId = profileId;
            this.archivageService = archivageService;
        }

        @Override
        public void synchronisationPlanComputationStarted() {
            updateMessage("Recherche des messages à archiver");
        }

        @Override
        public void synchronisationPlanComputed(int toBeDone, long sizeToBeDownloaded) {
            this.toBeDone += toBeDone;
            this.sizeToBeDownloaded += sizeToBeDownloaded;
        }

        @Override
        public void done(ArchivageService.DownloadTask downloadTask) {
            done += 1;
            sizeDownloaded += downloadTask.size();
            String progressMessage = getProgressMessage();
            LOGGER.info(progressMessage);
            updateMessage(progressMessage);
            updateProgress(sizeDownloaded, sizeToBeDownloaded);
        }

        public String getProgressMessage() {
            return "%d/%d messages et %s/%s téléchargés".formatted(
                done, toBeDone,
                FileUtils.byteCountToDisplaySize(sizeDownloaded),
                FileUtils.byteCountToDisplaySize(sizeToBeDownloaded)
            );
        }

        @Override
        public void terminated(ZonedDateTime start) {
            LOGGER.info("synchronisation débutée à " + start + " terminée");
            this.terminatedAfterStartAt = start;
        }

        @Override
        public void onErrorOccurred() {
            errorsCount += 1;
        }

        @Override
        public void onCancelledSynchronisation(ArchivageException e) {
            this.cancelledSynchronisationMessage = e.getMessage();
        }

        public String getCancelledSynchronisationMessage() {
            return cancelledSynchronisationMessage;
        }

        public int getErrorsCount() {
            return errorsCount;
        }

        public ZonedDateTime getTerminatedAfterStartAt() {
            return terminatedAfterStartAt;
        }

        @Override
        public void registerFolderWithInvalidPath(ZimbraService.Folder folder) {
            invalidFolderPaths.add(folder.absFolderPath());
        }

        public int getInvalidFolderPathsCount() {
            return invalidFolderPaths.size();
        }

        public int getDone() {
            return done;
        }

        @Override
        protected Void call() {
            startSynchronisationButton.setDisable(true);
            archivageService.startSynchronisation(zimbraAuthToken, queries, targetMountPoint, profileId, this, this::isCancelled);
            startSynchronisationButton.setDisable(false);
            return null;
        }
    }
}
