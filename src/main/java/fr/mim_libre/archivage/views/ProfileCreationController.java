package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import dev.dirs.BaseDirectories;
import dev.dirs.UserDirectories;
import fr.mim_libre.archivage.ArchivageService;
import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.SessionStatus;
import fr.mim_libre.archivage.index.IndexationRequest;
import fr.mim_libre.archivage.index.IndexationTask;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.components.CustomProgressDialog;
import fr.mim_libre.archivage.views.components.InfoLabel;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.function.BiConsumer;

/**
 * Classe qui contrôle la vue de création de profile. Vue racine quand l'application est lancée pour la première fois.
 */
public class ProfileCreationController {

    private static final Logger LOGGER = LogManager.getLogger(ProfileCreationController.class);

    @FXML
    private ImageView logoImageView;

    @FXML
    private Window window;

    @FXML
    private Label mountPointPathLabel;

    @FXML
    private Button nextButton;

    @FXML
    private Pane mountPathInfoLabelContainer;

    @FXML
    private VBox profileCreationTitleContainer;

    private Path mountPointPath;

    private String defaultMountPointPathString;

    private BiConsumer<String, Boolean> startMainStageForProfileCallback;

    private SessionStatus sessionStatus;

    @FXML
    public void initialize() {
        logoImageView.setImage(MainApplication.getApplicationImage());
        mountPointPath = getDefaultMountPoint();
        defaultMountPointPathString = mountPointPath.toString();
        mountPointPathLabel.setText(mountPointPath + " (par défaut)");
        nextButton.setDisable(true);
        onSessionStatusChange(SessionStatus.DISCONNECTED);
        MainApplication.getZimbraSessionManager().addSessionListener(this::onSessionStatusChange);
        setApplicationTitles();
    }

    private void setApplicationTitles() {
        String applicationName = MainApplication.getConfiguration().getApplicationName();
        Label applicationLabel = new Label(applicationName);

        String specificClientName = MainApplication.getConfiguration().getSpecificClientName();
        if (specificClientName.isEmpty()) {
            applicationLabel.getStyleClass().add("profile-creation-side-title");
        } else {
            Label label = new Label(specificClientName);
            label.getStyleClass().add("profile-creation-side-title");
            profileCreationTitleContainer.getChildren().add(label);
            applicationLabel.getStyleClass().add("profile-creation-side-subtitle");
        }

        profileCreationTitleContainer.getChildren().add(applicationLabel);
    }


    private void onSessionStatusChange(SessionStatus sessionStatus) {
        this.sessionStatus = sessionStatus;
        updateNextButton();
    }

    private void updateNextButton() {
        boolean enable = sessionStatus == SessionStatus.CONNECTED
                      && mountPointPath != null;
        nextButton.setDisable(!enable);
    }

    /**
     * Méthode permettant la sélection d'un point de montage pour l'archivage des mails.
     */
    public void askMountPointPath() {
        DirectoryChooser chooser = new DirectoryChooser();
        if (mountPointPath != null) {
            chooser.setInitialDirectory(mountPointPath.toFile());
        }
        File file = chooser.showDialog(window);

        if (file != null) {
            if (!Files.isWritable(file.toPath())) {
                this.replaceInfoLabel(null);
                Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Le répertoire " + file + " n’est pas accessible en écriture", ButtonType.OK);
                alert.showAndWait();
            } else {
                try {
                    if (!FileUtils.isEmptyDirectory(file)) {
                        InfoLabel infoLabel = new InfoLabel(InfoLabel.InfoLabelType.WARNING, "Le dossier choisi contient des données, nous allons faire une première indexation");
                        this.replaceInfoLabel(infoLabel);
                    } else {
                        InfoLabel infoLabel = new InfoLabel(InfoLabel.InfoLabelType.VALID, "Ok");
                        this.replaceInfoLabel(infoLabel);
                    }
                    mountPointPath = file.toPath();
                    String mountPointPathString = mountPointPath.toString();
                    if (mountPointPath.toString().equals(defaultMountPointPathString)) {
                        mountPointPathString += " (par défaut)";
                    }
                    mountPointPathLabel.setText(mountPointPathString);
                } catch (IOException e) {
                    this.replaceInfoLabel(null);
                    Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Impossible de lire le contenu de " + file, ButtonType.OK);
                    alert.showAndWait();
                }
            }
        }
        updateNextButton();
    }

    public void replaceInfoLabel(InfoLabel infoLabel) {
        mountPathInfoLabelContainer.getChildren().clear();
        if (infoLabel != null) {
            mountPathInfoLabelContainer.getChildren().add(infoLabel);
        }
    }

    /**
     * Méthode appelée à la fin du processus de création de profil. Pour lancer l'application principale.
     */
    public void next() {
        if (!mountPointPath.toFile().exists()) {
            mountPointPath.toFile().mkdirs();
        }
        ImmutableSet<String> mountPointValidationErrors = MainApplication.getConfigurationManager()
                .validateMountPointPaths(Collections.singleton(mountPointPath))
                .get(mountPointPath);
        if (mountPointValidationErrors.isEmpty()) {
            String login = MainApplication.getZimbraSessionManager().getLogin();
            String profileId = MainApplication.getConfigurationManager().createProfile(login, mountPointPath);
            MountPoint synchronizationMountPoint = MainApplication.getConfigurationManager().getProfile(profileId).getSynchronizationMountPoint();
            IndexationRequest indexationRequest = new IndexationRequest(
                    Collections.singleton(synchronizationMountPoint),
                    "Première indexation du dossier de travail",
                    IndexationRequest.CleaningStrategy.DO_NOTHING
            );
            ArchivageService archivageService = MainApplication.newArchivageService(profileId);
            IndexationTask indexationTask = MainApplication.startIndexationTask(archivageService, indexationRequest);
            CustomProgressDialog progressDialog = new CustomProgressDialog(indexationTask, indexationRequest.description());
            progressDialog.showAndWait();
            boolean cancelled = indexationTask.getCancellationMessages().containsKey(synchronizationMountPoint);
            if (cancelled) {
                LOGGER.warn("la première indexation a échoué");
                MainApplication.getConfigurationManager().deleteProfile(profileId);
                Alert alert = new CustomAlert(
                        Alert.AlertType.ERROR,
                        "L’indexation a échoué. Erreur : " + indexationTask.getCancellationMessages().get(synchronizationMountPoint),
                        ButtonType.OK
                );
                alert.showAndWait();
            } else {
                // l'indexation s'est bien passé, on peut ajouter le point de montage
                startMainStageForProfileCallback.accept(profileId, true);
            }
        } else {
            Alert alert = new CustomAlert(
                    Alert.AlertType.ERROR,
                    "Le dossier " + mountPointPath + " ne peut pas être utilisé. Erreurs " + String.join(". ", mountPointValidationErrors),
                    ButtonType.OK
            );
            alert.showAndWait();
        }
    }

    public void setStartMainStageForProfileCallback(BiConsumer<String, Boolean> startMainStageForProfileCallback) {
        this.startMainStageForProfileCallback = startMainStageForProfileCallback;
    }

    public static Path getDefaultMountPoint() {
        if (SystemUtils.IS_OS_LINUX || SystemUtils.IS_OS_MAC) {
            return Path.of(
                BaseDirectories.get().dataDir, "mim-libre-archivage"
            );
        } else if (SystemUtils.IS_OS_WINDOWS) {
            return Path.of(
                UserDirectories.get().documentDir, "Archivage"
            );
        }

        return Path.of(System.getProperty("user.home"), ".mim-libre-archivage");
    }
}
