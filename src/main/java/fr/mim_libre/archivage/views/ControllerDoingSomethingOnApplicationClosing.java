package fr.mim_libre.archivage.views;

/**
 * Ce controleur va faire quelque-chose à la fermeture de l’application.
 */
public interface ControllerDoingSomethingOnApplicationClosing {

    /**
     * Sera appelée à la fermeture de l’application.
     */
    void onApplicationClose();
}
