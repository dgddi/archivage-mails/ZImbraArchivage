package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.configuration.ConfigurationManager;
import fr.mim_libre.archivage.views.components.InfoLabel;
import fr.mim_libre.archivage.views.components.PaneController;
import fr.mim_libre.archivage.views.components.PositiveIntegerTextFormatter;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.Pane;

/**
 * Classe qui contrôle l'affichage du panneau latéral de paramètres. 
 */
public class SettingsPaneController extends PaneController {
    @FXML
    public Label versionLabel;

    @FXML
    private Node settingsPane;

    @FXML
    private Spinner<Integer> loggingDaysOfRetentionSpinner;

    @FXML
    private Pane settingsErrorsContainer;

    public void initialize() {
        int loggingDaysOfRetention = MainApplication.getConfiguration().getLoggingDaysOfRetention();
        loggingDaysOfRetentionSpinner.getEditor().setTextFormatter(new PositiveIntegerTextFormatter());
        loggingDaysOfRetentionSpinner.setEditable(true);
        loggingDaysOfRetentionSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(ConfigurationManager.MIN_LOGGING_DAYS_OF_RETENTION, ConfigurationManager.MAX_LOGGING_DAYS_OF_RETENTION, loggingDaysOfRetention, 1));
        loggingDaysOfRetentionSpinner.getValueFactory().valueProperty().addListener((observable, oldValue, newValue) -> {
            MainApplication.getConfigurationManager().setLoggingDaysOfRetention(newValue);
            InfoLabel errorLabel = new InfoLabel(InfoLabel.InfoLabelType.VALID, "Enregistré");
            settingsErrorsContainer.getChildren().clear();
            settingsErrorsContainer.getChildren().add(errorLabel);
        });
        versionLabel.setText("Version : " + MainApplication.VERSION);
    }

    @Override
    public Node getNode() {
        return settingsPane;
    }
}
