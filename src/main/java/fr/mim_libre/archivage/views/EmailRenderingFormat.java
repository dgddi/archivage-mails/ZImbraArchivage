package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Énumération ppermettant l'affichage des formats possibles de corps de mail
 */
public enum EmailRenderingFormat {
    TEXT("Texte"), HTML("HTML"), EML("Original");

    private final String label;

    EmailRenderingFormat(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }
}
