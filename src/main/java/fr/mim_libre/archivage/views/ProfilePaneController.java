package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Ordering;
import fr.mim_libre.archivage.ArchivageService;
import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.SessionStatus;
import fr.mim_libre.archivage.index.IndexationRequest;
import fr.mim_libre.archivage.index.IndexationTask;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.components.CustomProgressDialog;
import fr.mim_libre.archivage.views.components.PaneController;
import fr.mim_libre.archivage.views.components.PositiveIntegerTextFormatter;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import org.apache.commons.io.FileSystem;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Classe qui contrôle l'affichage du panneau latéral de profil
 */
public class ProfilePaneController extends PaneController {
    private static final Logger LOGGER = LogManager.getLogger(ProfilePaneController.class);

    @FXML
    private Node profilePane;

    @FXML
    private Label attachmentsFolderLabel;

    @FXML
    private Label loginLabel;

    @FXML
    private Label synchronizationMountPointPathLabel;

    @FXML
    private Spinner<Integer> remoteEmailsToArchiveSpinner;

    @FXML
    private Window window;

    @FXML
    private Label profilIcon;

    private MountPointsListener mountPointsListener;

    private Consumer<IndexationRequest> indexationRequestHandler;

    @FXML
    private ListView<MountPoint> visualizationMountPointsListView;

    @FXML
    private Button openSelectedVisualizationMountPointButton;

    @FXML
    private Button reindexSelectedVisualizationMountPointButton;

    @FXML
    private Button removeSelectedVisualizationMountPointButton;

    @FXML
    public void initialize() {

        attachmentsFolderLabel.setText(MainApplication.getProfile().getDefaultAttachmentsSavingPath().toString());
        loginLabel.setText(MainApplication.getProfile().getLogin());

        int remoteEmailsMustBeArchivedAfterDaysCount = MainApplication.getProfile()
                .getRemoteEmailsMustBeArchivedAfterDaysCount();
        remoteEmailsToArchiveSpinner.getEditor().setTextFormatter(new PositiveIntegerTextFormatter());
        remoteEmailsToArchiveSpinner.setEditable(true);
        remoteEmailsToArchiveSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, Integer.MAX_VALUE, remoteEmailsMustBeArchivedAfterDaysCount, 1));
        remoteEmailsToArchiveSpinner.getValueFactory().valueProperty().addListener((observable, oldValue, newValue) -> {
            String profileId = MainApplication.getProfile().getProfileId();
            MainApplication.getConfigurationManager().setRemoteEmailsMustBeArchivedAfterDaysCount(profileId,
                    newValue);
        });

        visualizationMountPointsListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            boolean nothingSelected = newValue == null;
            openSelectedVisualizationMountPointButton.setDisable(nothingSelected);
            reindexSelectedVisualizationMountPointButton.setDisable(nothingSelected);
            removeSelectedVisualizationMountPointButton.setDisable(nothingSelected);
        });
        visualizationMountPointsListView.setCellFactory((ListView<MountPoint> l) -> new ListCell<>() {
            @Override
            protected void updateItem(MountPoint item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(item.root().toString());
                } else {
                    setText(null);
                }
            }
        });

        synchronizationMountPointPathLabel.setText(MainApplication.getProfile().getSynchronizationMountPoint().root().toString());
        MainApplication.getProfile().getVisualizationMountPoints().forEach(mountPoint -> visualizationMountPointsListView.getItems().add(mountPoint));

        onSessionStatusChange(SessionStatus.DISCONNECTED);
        MainApplication.getZimbraSessionManager().addSessionListener(this::onSessionStatusChange);
    }

    public void setMountPointsListener(MountPointsListener mountPointsListener) {
        this.mountPointsListener = mountPointsListener;
    }

    public void setIndexationRequestHandler(Consumer<IndexationRequest> indexationRequestController) {
        this.indexationRequestHandler = indexationRequestController;
    }

    /**
     * Met à jour l'icône de profil en fonction du statut de connexion
     */
    private void onSessionStatusChange(SessionStatus sessionStatus) {
        switch (sessionStatus) {
            case CONNECTED -> {
                profilIcon.getStyleClass().remove("not-connected");
            }
            case DISCONNECTED ->  {
                profilIcon.getStyleClass().add("not-connected");
            }
            default -> throw new IllegalStateException("non-géré " + sessionStatus);
        }
    }

    @Override
    public Node getNode() {
        return profilePane;
    }

    /**
     * Méthode appelée lors du rafraichissement des mails
     */
    public void startRescan() {
        Set<MountPoint> mountPoints = MainApplication.getProfile().getMountPoints();
        IndexationRequest indexationRequest =
                new IndexationRequest(
                        mountPoints,
                        "Rafraichissement complet",
                        IndexationRequest.CleaningStrategy.DELETE_FULL_INDEX
                );
        indexationRequestHandler.accept(indexationRequest);
        hide();
    }

    /**
     * Permet d'ouvrir le dossier qui contient les logs
     */
    public void openLogs() {
        final Path logsDirectoryPath = MainApplication.getProfile().getLogsDirectoryPath().toAbsolutePath();
        this.openDirectory(logsDirectoryPath);
    }

    public void addMountPoint() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setInitialDirectory(FileUtils.getUserDirectory());
        File newMountPointFile = chooser.showDialog(window);
        if (newMountPointFile != null) {
            LOGGER.info("L’utilisateur veut ajouter un point de montage vers " + newMountPointFile);
            Path newMountPointPath = newMountPointFile.toPath();
            boolean valid = validateMountPointPath(newMountPointPath);
            MountPoint newMountPoint = new MountPoint(newMountPointPath);
            if (valid) {
                try {
                    boolean firstIndexationNecessary = !FileUtils.isEmptyDirectory(newMountPointFile);
                    boolean canAddMountPoint;
                    if (firstIndexationNecessary) {
                        IndexationRequest indexationRequest = new IndexationRequest(
                                Collections.singleton(newMountPoint),
                                "Première indexation de " + newMountPoint.root(),
                                IndexationRequest.CleaningStrategy.DO_NOTHING
                        );
                        ArchivageService archivageService = MainApplication.getArchivageService();
                        IndexationTask indexationTask = MainApplication.startIndexationTask(archivageService, indexationRequest);
                        CustomProgressDialog progressDialog = new CustomProgressDialog(indexationTask, indexationRequest.description());
                        progressDialog.showAndWait();
                        boolean cancelled = indexationTask.getCancellationMessages().containsKey(newMountPoint);
                        if (cancelled) {
                            LOGGER.warn("la première indexation a échoué");
                            Alert alert = new CustomAlert(
                                    Alert.AlertType.ERROR,
                                    "L’indexation a échoué. Erreur : " + indexationTask.getCancellationMessages().get(newMountPoint),
                                    ButtonType.OK
                            );
                            alert.showAndWait();
                            canAddMountPoint = false;
                        } else {
                            // l'indexation s'est bien passé, on peut ajouter le point de montage
                            canAddMountPoint = true;
                        }
                    } else {
                        canAddMountPoint = true;
                    }
                    if (canAddMountPoint) {
                        MainApplication.getConfigurationManager().addVisualizationMountPoint(MainApplication.getProfile().getProfileId(), newMountPoint);
                        visualizationMountPointsListView.getItems().add(newMountPoint);
                        mountPointsListener.onVisualizationMountPointAdded(newMountPoint);
                        hide();
                    } else {
                        LOGGER.warn("le point de montage n’a pas été ajouté");
                    }
                } catch (IOException e) {
                    Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Impossible de lire le contenu de " + newMountPointFile, ButtonType.OK);
                    alert.showAndWait();
                }
            }
        }
    }

    private boolean validateMountPointPath(Path newMountPointPath) {
        ImmutableSet<Path> existingMountPointPaths = MainApplication.getProfile().getMountPoints().stream()
                .map(MountPoint::root)
                .collect(ImmutableSortedSet.toImmutableSortedSet(Ordering.natural()));
        ImmutableSet<Path> toValidate = ImmutableSortedSet.<Path>naturalOrder()
                .add(newMountPointPath)
                .addAll(existingMountPointPaths)
                .build();
        ImmutableSetMultimap<Path, String> errorPerPaths = MainApplication.getConfigurationManager().validateMountPointPaths(toValidate);
        errorPerPaths.get(newMountPointPath).forEach(error -> {
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, error, ButtonType.OK);
            alert.showAndWait();
        });
        return errorPerPaths.isEmpty();
    }

    public void removeSelectedVisualizationMountPoint() {
        MountPoint selectedItem = visualizationMountPointsListView.getSelectionModel().getSelectedItem();
        ArchivageService archivageService = MainApplication.getArchivageService();
        archivageService.removeMountPoint(selectedItem);
        MainApplication.getConfigurationManager().removeVisualizationMountPoint(MainApplication.getProfile().getProfileId(), selectedItem);
        visualizationMountPointsListView.getSelectionModel().clearSelection();
        visualizationMountPointsListView.getItems().remove(selectedItem);
        mountPointsListener.onVisualizationMountPointRemoved(selectedItem);
    }

    public void openSelectedVisualizationMountPoint() {
        MountPoint selectedItem = visualizationMountPointsListView.getSelectionModel().getSelectedItem();
        openDirectory(selectedItem.root());
    }

    public void reindexSelectedVisualizationMountPoint() {
        MountPoint selectedItem = visualizationMountPointsListView.getSelectionModel().getSelectedItem();
        IndexationRequest indexationRequest =
                new IndexationRequest(
                        Collections.singleton(selectedItem),
                        "Rafraichissement du dossier " + selectedItem,
                        IndexationRequest.CleaningStrategy.REMOVE_EACH_MOUNT_POINT
                );
        indexationRequestHandler.accept(indexationRequest);
        hide();
    }

    /**
     * Permet de choisir un dossier pour sauvegarder les pièces jointes
     */
    public void askDefaultAttachmentsSavingPath() {
        DirectoryChooser chooser = new DirectoryChooser();
        Path defaultAttachmentsSavingPath = MainApplication.getProfile().getDefaultAttachmentsSavingPath();
        chooser.setInitialDirectory(defaultAttachmentsSavingPath.toFile());
        File file = chooser.showDialog(window);
        if (file != null) {
            String profileId = MainApplication.getProfile().getProfileId();
            MainApplication.getConfigurationManager().setDefaultAttachmentsSavingPath(profileId, file);
            attachmentsFolderLabel.setText(file.toString());
        }
    }

    private void openDirectory(Path directoryPath) {
        FileSystem fileSystem = FileSystem.getCurrent();
        switch (fileSystem) {
            case LINUX -> {
                try {
                    Runtime.getRuntime().exec(String.format("xdg-open %s", directoryPath));
                } catch (IOException e) {
                    LOGGER.error("impossible d'ouvrir l'explorateur de fichiers sous Linux");
                }
            }
            case WINDOWS -> {
                try {
                    Runtime.getRuntime().exec(String.format("explorer.exe \"%s\"", directoryPath));
                } catch (IOException e) {
                    LOGGER.error("impossible d'ouvrir l'explorateur de fichiers Windows");
                }
            }
            default -> {
                if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE_FILE_DIR)) {
                    Desktop.getDesktop().browseFileDirectory(directoryPath.toFile());
                } else {
                    LOGGER.warn("l’ouverture de l'explorateur de fichiers n’est pas gérée");
                }
            }
        }
    }
}
