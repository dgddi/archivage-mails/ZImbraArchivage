package fr.mim_libre.archivage.views;

import fr.mim_libre.archivage.store.MountPoint;

/**
 * Pour un controleur qui doit être à l’écoute de la modifications des points de montage.
 */
public interface MountPointsListener {

    /**
     * Appelé après qu’un point de montage a été ajouté.
     */
    void onVisualizationMountPointAdded(MountPoint addedMountPoint);

    /**
     * Appelé après que le point de montage passé a été supprimé.
     */
    void onVisualizationMountPointRemoved(MountPoint removedMountPoint);

    /**
     * Appelé après que le contenu du point de montage passé a changé, il faut rafraîchir la vue.
     */
    void onMountPointContentUpdated(MountPoint mountPoint);
}
