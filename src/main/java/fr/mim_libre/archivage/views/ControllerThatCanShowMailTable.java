package fr.mim_libre.archivage.views;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.index.ArchivageQuery;
import fr.mim_libre.archivage.index.MissingRelevantKeywordException;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.views.utils.FilesManagerView;

/**
 * Représente un controlleur permettant à l’utilisateur d’afficher des messages.
 */
public interface ControllerThatCanShowMailTable {

    /**
     * L’utilisateur veut afficher le contenu du dossier passé.
     */
    void showFolderContentInTableView(Folder folder);

    /**
     * L’utilisateur veut afficher les résultats de la recherche passée.
     */
    void showSearchResultsInTableView(ArchivageQuery archivageQuery) throws MissingRelevantKeywordException;

    /**
     * L’utilisateur veut basculer de la vue recherche à la vue arborescence.
     */
    void changeView(FilesManagerView chosenView);
}
