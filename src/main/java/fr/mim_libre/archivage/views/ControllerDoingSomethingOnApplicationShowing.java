package fr.mim_libre.archivage.views;

/**
 * Ce controleur va faire quelque-chose à l'ouverture de l’application.
 */
public interface ControllerDoingSomethingOnApplicationShowing {

    /**
     * Sera appelée à l'ouverture de l’application.
     */
    void onApplicationShow();
}
