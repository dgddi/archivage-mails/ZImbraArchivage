package fr.mim_libre.archivage.views.files;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.collect.MoreCollectors;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import com.google.common.collect.Streams;
import fr.mim_libre.archivage.MainApplication;
import fr.mim_libre.archivage.configuration.ConfigurationManager;
import fr.mim_libre.archivage.index.ArchivageQuery;
import fr.mim_libre.archivage.index.HasAnyAttachment;
import fr.mim_libre.archivage.index.IndexationRequest;
import fr.mim_libre.archivage.index.MissingRelevantKeywordException;
import fr.mim_libre.archivage.index.SearchKeywordStrategy;
import fr.mim_libre.archivage.store.ArchivageStoreException;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.views.ControllerDoingSomethingOnApplicationClosing;
import fr.mim_libre.archivage.views.ControllerThatCanShowMailTable;
import fr.mim_libre.archivage.views.MountPointsListener;
import fr.mim_libre.archivage.views.components.CustomAlert;
import fr.mim_libre.archivage.views.components.InfoLabel;
import fr.mim_libre.archivage.views.utils.FilesManagerView;
import fr.mim_libre.archivage.views.utils.NodeUtils;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kordamp.ikonli.javafx.FontIcon;
import org.kordamp.ikonli.javafx.StackedFontIcon;
import org.kordamp.ikonli.remixicon.RemixiconAL;
import org.kordamp.ikonli.remixicon.RemixiconMZ;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;

/**
 * Classe qui contrôle la navigation dans les fichiers de mail (soit via l'arborescence, soit via la recherche)
 */
public class FilesManagerController implements MountPointsListener, ControllerDoingSomethingOnApplicationClosing {

    private static final Logger LOGGER = LogManager.getLogger(FilesManagerController.class);

    private static final Ordering<FoldersTreeNode> FOLDERS_TREE_NODE_SORTING_COMPARATOR =
            Folder.FOLDERS_ORDERING.onResultOf(FoldersTreeNode::folder);

    private static final Ordering<TreeItem<FoldersTreeNode>> TREE_ITEM_SORTING_COMPARATOR =
            FOLDERS_TREE_NODE_SORTING_COMPARATOR.onResultOf(TreeItem::getValue);

    private static final TreeItem<FoldersTreeNode> ROOT_ITEM = new TreeItem<>(new FoldersTreeNode() {

        @Override
        public String id() {
            return "ROOT_ITEM";
        }

        @Override
        public Folder folder() {
            throw new UnsupportedOperationException("ce nœud de l’arbre ne correspond à aucun dossier à ouvrir");
        }

        @Override
        public String label() {
            return "Tous les dossiers";
        }
    });

    private static final TreeItem<FoldersTreeNode> VISUALIZATION_MOUNT_POINTS_ITEM = new TreeItem<>(new FoldersTreeNode() {

        @Override
        public String id() {
            return "VISUALIZATION_MOUNT_POINTS_ITEM";
        }

        @Override
        public Folder folder() {
            throw new UnsupportedOperationException("ce nœud de l’arbre ne correspond à aucun dossier à ouvrir");
        }

        @Override
        public String label() {
            return "Dossiers en consultation";
        }

    });

    private ControllerThatCanShowMailTable controllerThatCanShowMailTable;

    private Consumer<IndexationRequest> indexationRequestHandler;

    private FilesManagerView currentView;

    @FXML
    private Label filesTitle;

    @FXML
    private Button treeButton;

    @FXML
    private Button searchButton;

    @FXML
    private TreeView<FoldersTreeNode> foldersTreeView;

    @FXML
    private VBox searchView;

    @FXML
    private ChoiceBox<SearchKeywordStrategy> keywordsChoiceBox;

    @FXML
    private Button startResearchButton;

    @FXML
    private TextField fromTextField;

    @FXML
    private TextField toTextField;

    @FXML
    private TextField ccTextField;

    @FXML
    private TextField keywordsTextField;

    @FXML
    private DatePicker fromDatePicker;

    @FXML
    private DatePicker toDatePicker;

    @FXML
    private Pane searchErrorsContainer;

    @FXML
    private CheckBox attachedCheckBox;

    @FXML
    public void initialize() {
        initializeFoldersTree();

        foldersTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                // ça peut arriver lors du changement de point de montage, en quel cas on quitte la sélection
                // de l'ancien point de montage alors que le nouveau est pas encore dans l'interface
            } else if (Set.of(ROOT_ITEM, VISUALIZATION_MOUNT_POINTS_ITEM).contains(newValue)) {
                // l'utilisateur a cliqué sur un nœud intermédiaire de l'arbre, on ne fait rien
            } else {
                Folder folder = newValue.getValue().folder();
                controllerThatCanShowMailTable.showFolderContentInTableView(folder);
            }
        });

        keywordsChoiceBox.setItems(FXCollections.observableArrayList(SearchKeywordStrategy.SUBJECT_OR_BODY, SearchKeywordStrategy.SUBJECT, SearchKeywordStrategy.BODY));
        keywordsChoiceBox.getSelectionModel().select(SearchKeywordStrategy.SUBJECT_OR_BODY);

        showWithoutUpdatingMailsTable(FilesManagerView.TREE);
        treeButton.setOnAction(event -> show(FilesManagerView.TREE));
        searchButton.setOnAction(event -> show(FilesManagerView.SEARCH));

        ChangeListener<Object> formChangedListener = (obs, oldValue, newValue) -> {
            updateStartResearchButtonStateAndValidation();
        };
        fromTextField.textProperty().addListener(formChangedListener);
        toTextField.textProperty().addListener(formChangedListener);
        ccTextField.textProperty().addListener(formChangedListener);
        keywordsTextField.textProperty().addListener(formChangedListener);
        fromDatePicker.valueProperty().addListener(formChangedListener);
        toDatePicker.valueProperty().addListener(formChangedListener);
        attachedCheckBox.selectedProperty().addListener(formChangedListener);

        MainApplication.registerCloseListener(this);
    }

    public void setControllerThatCanShowSearchResultsInTableView(ControllerThatCanShowMailTable controllerThatCanShowMailTable) {
        this.controllerThatCanShowMailTable = controllerThatCanShowMailTable;
    }

    public void setIndexationRequestHandler(Consumer<IndexationRequest> indexationRequestHandler) {
        this.indexationRequestHandler = indexationRequestHandler;
    }

    /**
     * Méthode permettant d'initialiser l'arborescence de dossiers
     */
    private void initializeFoldersTree() {
        foldersTreeView.setCellFactory(tv ->  new TreeCell<>() {
            @Override
            public void updateItem(FoldersTreeNode item, boolean empty) {
                // Remise à zéro des noeuds
                super.updateItem(item, empty);
                getStyleClass().remove("mount-point");
                setDisclosureNode(null);
                setGraphic(null);

                if (empty) {
                    setText("");
                } else {
                    // Si le noeud concerné représente un point de montage, on lui ajoute une icône et un style particulier
                    if (item instanceof MountPointFoldersTreeNode) {
                        if (((MountPointFoldersTreeNode) item).mountPoint().equals(MainApplication.getProfile().getSynchronizationMountPoint())) {
                            setGraphic(newMountPointIcon(RemixiconAL.ARROW_UP_DOWN_LINE));
                            setText("Dossier de travail");
                        } else {
                            setGraphic(newMountPointIcon(RemixiconAL.EYE_LINE));
                            setText(item.label());
                        }
                        getStyleClass().add("mount-point");
                    } else {
                        setText(item.label());
                    }

                    if (item.id().equals(VISUALIZATION_MOUNT_POINTS_ITEM.getValue().id())) {
                        Tooltip tooltip = new Tooltip("Liste des dossiers d'archivage de courriels en lecture seule");
                        tooltip.setShowDelay(Duration.seconds(1));
                        FontIcon icon = new FontIcon(RemixiconMZ.QUESTION_LINE);
                        icon.getStyleClass().addAll("tree-icon", "tree-tooltip");
                        Tooltip.install(icon, tooltip);
                        setGraphic(icon);
                        setGraphicTextGap(10);
                        setContentDisplay(ContentDisplay.RIGHT);
                    }

                    // Pour chaque noeud de l'arbre, on change la flèche native par la nôtre
                    StackPane arrowIconContainer = new StackPane();
                    FontIcon arrowIcon = new FontIcon(RemixiconAL.ARROW_DROP_RIGHT_LINE);
                    arrowIcon.getStyleClass().add("arrow");
                    arrowIconContainer.getChildren().add(arrowIcon);
                    setDisclosureNode(arrowIconContainer);
                    setWrapText(true);
                }
            }
        });

        Map<String, Boolean> expandedTreeItemIds = MainApplication.getProfile().getExpandedTreeItemIds();

        ROOT_ITEM.setExpanded(expandedTreeItemIds.getOrDefault(ROOT_ITEM.getValue().id(), true));
        foldersTreeView.setRoot(ROOT_ITEM);

        MountPoint synchronizationMountPoint = MainApplication.getProfile().getSynchronizationMountPoint();
        try {
            TreeItem<FoldersTreeNode> mountPointTreeItem = newTreeItem(synchronizationMountPoint);
            ROOT_ITEM.getChildren().add(mountPointTreeItem);
            mountPointTreeItem.setExpanded(expandedTreeItemIds.getOrDefault(mountPointTreeItem.getValue().id(), true));
        } catch (ArchivageStoreException e) {
            LOGGER.warn("erreur pour générer l’arbre de " + synchronizationMountPoint, e);
        }

        ROOT_ITEM.getChildren().add(VISUALIZATION_MOUNT_POINTS_ITEM);
        VISUALIZATION_MOUNT_POINTS_ITEM.setExpanded(expandedTreeItemIds.getOrDefault(VISUALIZATION_MOUNT_POINTS_ITEM.getValue().id(), true));

        for (MountPoint mountPoint : MainApplication.getProfile().getVisualizationMountPoints()) {
            try {
                TreeItem<FoldersTreeNode> mountPointTreeItem = newTreeItem(mountPoint);
                VISUALIZATION_MOUNT_POINTS_ITEM.getChildren().add(mountPointTreeItem);
                mountPointTreeItem.setExpanded(expandedTreeItemIds.getOrDefault(mountPointTreeItem.getValue().id(), false));
            } catch (ArchivageStoreException e) {
                LOGGER.warn("erreur pour générer l’arbre de " + mountPoint, e);
            }
        }
    }

    private StackedFontIcon newMountPointIcon(RemixiconAL innerIconCode) {
        StackedFontIcon newMountPointIcon = new StackedFontIcon();
        FontIcon outerIcon = new FontIcon(RemixiconAL.FOLDER_3_LINE);
        outerIcon.getStyleClass().add("stacked-outer-icon");
        newMountPointIcon.getChildren().add(outerIcon);
        FontIcon innerIcon = new FontIcon(innerIconCode);
        StackedFontIcon.setIconSize(innerIcon, 0.50);
        StackedFontIcon.setMargin(innerIcon, new Insets(15, 0, 0, 10));
        newMountPointIcon.getChildren().add(innerIcon);
        newMountPointIcon.getStyleClass().add("tree-icon");
        return newMountPointIcon;
    }

    public void selectFirstItem() {
        foldersTreeView.getSelectionModel().selectFirst();
    }

    private TreeItem<FoldersTreeNode> newTreeItem(MountPoint mountPoint) throws ArchivageStoreException {
        Map<String, Boolean> expandedTreeItemIds = MainApplication.getProfile().getExpandedTreeItemIds();
        try {
            MountPointFoldersTreeNode mountPointNode = new MountPointFoldersTreeNode(mountPoint);
            Map<Folder, Folder> folderToParents = new LinkedHashMap<>();
            Set<Folder> folders = MainApplication.getArchivageService().getFolders(mountPoint);
            folders.forEach(folder -> {
                if (folder.isRoot()) {
                    // le dossier racine n’a pas de parent
                } else {
                    folderToParents.put(folder, folder.getParent());
                }
            });
            Map<Folder, TreeItem<FoldersTreeNode>> folderToTreeItems = new LinkedHashMap<>();

            TreeItem<FoldersTreeNode> mountPointTreeItem = new TreeItem<>(mountPointNode);
            folderToTreeItems.put(mountPoint.rootFolder(), mountPointTreeItem);
            folderToTreeItems.putAll(Maps.transformEntries(folderToParents, (folder, parent) -> {
                FoldersTreeNode foldersTreeNode = new FolderFoldersTreeNode(folder);
                TreeItem<FoldersTreeNode> treeItem = new TreeItem<>(foldersTreeNode);
                return treeItem;
            }));
            folderToParents.forEach((folder, parent) -> {
                TreeItem<FoldersTreeNode> childItem = folderToTreeItems.get(folder);
                TreeItem<FoldersTreeNode> parentItem = folderToTreeItems.get(parent);
                parentItem.getChildren().add(childItem);
            });

            folderToTreeItems.values().forEach(aTreeItem -> {
                aTreeItem.setExpanded(expandedTreeItemIds.getOrDefault(aTreeItem.getValue().id(), false));
                aTreeItem.getChildren().sort(TREE_ITEM_SORTING_COMPARATOR);
            });

            return mountPointTreeItem;
        } catch (ArchivageStoreException e) {
            Alert alert = new CustomAlert(
                    Alert.AlertType.ERROR,
                    "Erreur lors de l’affichage du dossier " + mountPoint.root() + ". Cause : " + e.getMessage(),
                    ButtonType.OK
            );
            alert.showAndWait();
            throw e;
        }
    }

    @Override
    public void onVisualizationMountPointAdded(MountPoint addedMountPoint) {
        try {
            TreeItem<FoldersTreeNode> addedTreeItem = newTreeItem(addedMountPoint);
            VISUALIZATION_MOUNT_POINTS_ITEM.getChildren().add(addedTreeItem);
            foldersTreeView.getSelectionModel().select(addedTreeItem);
        } catch (ArchivageStoreException e) {
            LOGGER.warn("erreur pour générer l’arbre de " + addedMountPoint, e);
        }
    }

    @Override
    public void onVisualizationMountPointRemoved(MountPoint removedMountPoint) {
        LOGGER.debug("on veut supprimer de l’arbre " + removedMountPoint);
        Optional<TreeItem<FoldersTreeNode>> treeItemToRemoveOptional = VISUALIZATION_MOUNT_POINTS_ITEM.getChildren().stream()
                .filter(mountPointTreeItem -> {
                        MountPointFoldersTreeNode mountPointFoldersTreeNode = (MountPointFoldersTreeNode) mountPointTreeItem.getValue();
                        return mountPointFoldersTreeNode.mountPoint().equals(removedMountPoint);
                })
                .collect(MoreCollectors.toOptional());
        treeItemToRemoveOptional.ifPresent(treeItemToRemove -> {
            TreeItem<FoldersTreeNode> selectedItem = foldersTreeView.getSelectionModel().getSelectedItem();
            if (selectedItem.equals(treeItemToRemove)) {
                foldersTreeView.getSelectionModel().select(ROOT_ITEM);
            }
            VISUALIZATION_MOUNT_POINTS_ITEM.getChildren().remove(treeItemToRemove);
        });
    }

    private interface FoldersTreeNode {

        String id();

        Folder folder();

        String label();
    }

    record FolderFoldersTreeNode(Folder folder) implements FoldersTreeNode {

        @Override
        public String label() {
            return folder().getHumanReadableName();
        }

        @Override
        public String id() {
            return "FOLDER_" + folder().toFullPath();
        }
    }

    record MountPointFoldersTreeNode(MountPoint mountPoint) implements FoldersTreeNode {

        @Override
        public Folder folder() {
            return mountPoint.rootFolder();
        }

        @Override
        public String label() {
            return mountPoint.root().getFileName().toString();
        }

        @Override
        public String id() {
            return "MOUNT_POINT_" + mountPoint.root().toString();
        }
    }

    @Override
    public void onMountPointContentUpdated(MountPoint mountPoint) {
        foldersTreeView.getSelectionModel().clearSelection();
        try {
            TreeItem<FoldersTreeNode> treeItemToUpdate = Streams.concat(
                    ROOT_ITEM.getChildren().stream(),
                    VISUALIZATION_MOUNT_POINTS_ITEM.getChildren().stream()
            ).filter(foldersTreeNodeTreeItem -> {
                FoldersTreeNode foldersTreeNode = foldersTreeNodeTreeItem.getValue();
                return foldersTreeNode instanceof MountPointFoldersTreeNode && ((MountPointFoldersTreeNode) foldersTreeNode).mountPoint().equals(mountPoint);
            }).collect(MoreCollectors.onlyElement());
            TreeItem<FoldersTreeNode> rebuilt = newTreeItem(mountPoint);
            treeItemToUpdate.getChildren().clear();
            treeItemToUpdate.getChildren().addAll(rebuilt.getChildren());
            foldersTreeView.getSelectionModel().select(treeItemToUpdate);
        } catch (ArchivageStoreException e) {
            LOGGER.warn("erreur pour générer l’arbre suite à la synchro de " + mountPoint, e);
        }
    }

    /**
     * Reset la vue de recherche en vidant le formulaire.
     */
    private void resetSearchView() {
        toTextField.setText("");
        fromTextField.setText("");
        ccTextField.setText("");
        keywordsChoiceBox.setValue(SearchKeywordStrategy.SUBJECT_OR_BODY);
        keywordsTextField.setText("");
        toDatePicker.setValue(null);
        fromDatePicker.setValue(null);
        startResearchButton.setDisable(true);
        searchErrorsContainer.getChildren().clear();
    }

    private void showWithoutUpdatingMailsTable(FilesManagerView chosenView) {
        if (chosenView.equals(this.currentView)) {
            return;
        }

        String filesTitleText;
        switch (chosenView) {
            case SEARCH -> {
                filesTitleText = "Critères de recherche";
                resetSearchView();
                searchButton.getStyleClass().add("open-tab");
                treeButton.getStyleClass().remove("open-tab");
                NodeUtils.showNode(foldersTreeView, false);
                NodeUtils.showNode(searchView, true);
            }
            case TREE -> {
                filesTitleText = "Dossier local";
                treeButton.getStyleClass().add("open-tab");
                searchButton.getStyleClass().remove("open-tab");
                NodeUtils.showNode(foldersTreeView, true);
                NodeUtils.showNode(searchView, false);
            }
            default -> throw new IllegalStateException("Unexpected value: " + chosenView);
        }
        filesTitle.setText(filesTitleText);
        this.currentView = chosenView;
    }

    private void show(FilesManagerView chosenView) {
        controllerThatCanShowMailTable.changeView(chosenView);
        showWithoutUpdatingMailsTable(chosenView);
    }

    private void updateStartResearchButtonStateAndValidation() {
        LocalDate fromDatePickerValue = fromDatePicker.getValue();
        LocalDate toDatePickerValue = toDatePicker.getValue();
        boolean allFieldsAreEmpty = fromTextField.getText().isEmpty()
                && toTextField.getText().isEmpty()
                && ccTextField.getText().isEmpty()
                && keywordsTextField.getText().isEmpty()
                && fromDatePickerValue == null
                && toDatePickerValue == null
                && !attachedCheckBox.isSelected();
        boolean toDateIsBeforeFromDate = fromDatePickerValue != null && toDatePickerValue != null && toDatePickerValue.isBefore(fromDatePickerValue);

        startResearchButton.setDisable(allFieldsAreEmpty || toDateIsBeforeFromDate);

        searchErrorsContainer.getChildren().clear();
        if (toDateIsBeforeFromDate) {
            InfoLabel errorLabel = new InfoLabel(InfoLabel.InfoLabelType.ERROR, "La date de fin doit être inférieure à la date de départ.");
            searchErrorsContainer.getChildren().add(errorLabel);
        }
    }

    @FXML
    private void startResearch() {
        String from = Strings.emptyToNull(fromTextField.getText());
        String to = Strings.emptyToNull(toTextField.getText());
        String cc = Strings.emptyToNull(ccTextField.getText());

        SearchKeywordStrategy searchKeywordStrategy;
        String searchKeyword;
        if (keywordsTextField.getText().isEmpty()) {
            searchKeywordStrategy = SearchKeywordStrategy.IGNORED;
            searchKeyword = null;
        } else {
            searchKeywordStrategy = keywordsChoiceBox.getValue();
            searchKeyword = keywordsTextField.getText();
        }

        ZoneId zoneId = MainApplication.getConfiguration().getZoneId();
        LocalDate fromDatePickerValue = fromDatePicker.getValue();
        Date fromDate = null;
        Date toDate = null;
        if (fromDatePickerValue != null) {
            Instant fromInstant = Instant.from(fromDatePickerValue.atStartOfDay(zoneId));
            fromDate = Date.from(fromInstant);
        }
        LocalDate toDatePickerValue = toDatePicker.getValue();
        if (toDatePickerValue != null) {
            Instant toInstant = Instant.from(toDatePickerValue.plusDays(1).atStartOfDay(zoneId));
            toDate = Date.from(toInstant);
        }
        Range<Date> dateRange = null;
        if (fromDate != null && toDate != null) {
            dateRange = Range.closedOpen(fromDate, toDate);
        }
        else if (fromDate != null) {
            dateRange = Range.atLeast(fromDate);
        }
        else if (toDate != null){
            dateRange = Range.lessThan(toDate);
        }

        HasAnyAttachment hasAnyAttachment = attachedCheckBox.isSelected() ? HasAnyAttachment.SOME_ATTACHMENT : HasAnyAttachment.IGNORED;

        ArchivageQuery archivageQuery =
                new ArchivageQuery(
                        from,
                        to,
                        cc,
                        dateRange,
                        hasAnyAttachment,
                        null, null,
                        searchKeywordStrategy,
                        searchKeyword);
        try {
            controllerThatCanShowMailTable.showSearchResultsInTableView(archivageQuery);
        } catch (MissingRelevantKeywordException e) {
            searchErrorsContainer.getChildren().clear();
            InfoLabel errorLabel = new InfoLabel(InfoLabel.InfoLabelType.ERROR, e.getMessage());
            searchErrorsContainer.getChildren().add(errorLabel);
            Button showStopWordsHelp = new Button();
            showStopWordsHelp.getStyleClass().add("round-button");
            FontIcon fontIcon = new FontIcon(RemixiconAL.INFORMATION_FILL);
            showStopWordsHelp.setGraphic(fontIcon);
            showStopWordsHelp.getStyleClass().add("icon-only-button");
            showStopWordsHelp.setOnAction(event -> {
                Alert alert = new CustomAlert(Alert.AlertType.INFORMATION, "sera remplacé", ButtonType.CLOSE);
                Text text = new Text("Les mots considérés comme non pertinents sont :" + System.lineSeparator() + String.join(", ", e.getStopWords()));
                text.setWrappingWidth(800);
                alert.getDialogPane().setContent(text);
                alert.showAndWait();
            });
            searchErrorsContainer.getChildren().add(showStopWordsHelp);
        }
    }

    /**
     * Méthode appelée lors du rafraichissement des mails
     */
    public void startGlobalRescan() {
        Set<MountPoint> mountPoints = MainApplication.getProfile().getMountPoints();
        IndexationRequest indexationRequest =
                new IndexationRequest(
                        mountPoints,
                        "Rafraichissement complet",
                        IndexationRequest.CleaningStrategy.DELETE_FULL_INDEX
                );
        indexationRequestHandler.accept(indexationRequest);
    }

    @Override
    public void onApplicationClose() {
        TreeItem<FoldersTreeNode> root = foldersTreeView.getRoot();
        Map<String, Boolean> expandedTreeItemIds = collectExpandedFolderPaths(root);
        LOGGER.debug("on doit sauvegarder " + expandedTreeItemIds);
        String profileId = MainApplication.getProfile().getProfileId();
        ConfigurationManager configurationManager = MainApplication.getConfigurationManager();
        configurationManager.saveExpandedTreeItemIds(profileId, expandedTreeItemIds);
    }

    private Map<String, Boolean> collectExpandedFolderPaths(TreeItem<FoldersTreeNode> treeItem) {
        Map<String, Boolean> expandedFolderPaths = new TreeMap<>();
        for (TreeItem<FoldersTreeNode> child : treeItem.getChildren()) {
            Map<String, Boolean> childrenPaths = collectExpandedFolderPaths(child);
            expandedFolderPaths.putAll(childrenPaths);
        }
        String expendedTreeItemId = treeItem.getValue().id();
        expandedFolderPaths.put(expendedTreeItemId, treeItem.isExpanded());
        return expandedFolderPaths;
    }
}
