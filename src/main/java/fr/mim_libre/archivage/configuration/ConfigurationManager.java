package fr.mim_libre.archivage.configuration;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.MoreCollectors;
import fr.mim_libre.archivage.PropertiesFileNotFoundArchivageException;
import fr.mim_libre.archivage.logging.ArchivageLoggingLevel;
import fr.mim_libre.archivage.store.MountPoint;
import org.apache.commons.io.FileSystem;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Gestion de la configuration de l’application : chargement, modification, sauvegarde, validation, typage...
 */
public class ConfigurationManager {

    private static final Logger LOGGER = LogManager.getLogger(ConfigurationManager.class);

    /**
     * Version dans laquelle un fichier de configuration doit se trouver pour être considéré comme valide.
     */
    private static final int EXPECTED_VERSION = 0;

    /**
     * Durée minimum de rétention des journaux utilisateurs (en jours).
     */
    public static final int MIN_LOGGING_DAYS_OF_RETENTION = 14;

    /**
     * Durée maximum de rétention des journaux utilisateurs (en jours) afin d’éviter la consommation d’espace disque.
     */
    public static final int MAX_LOGGING_DAYS_OF_RETENTION = 365;

    /**
     * Le fichier de configuration étant au format JSON, il sera lu/écrit via ce {@link JsonMapper}.
     */
    private final JsonMapper jsonMapper;

    /**
     * Le répertoire dans lequel on va stocker toutes les données de configuration.
     */
    private final File userDirectory;

    /**
     * Le fichier JSON dans lequel on persiste la configuration.
     */
    private final File configuration;

    {
        jsonMapper = JsonMapper.builder()
                .addModule(new Jdk8Module())
                .addModule(new JavaTimeModule())
                .enable(SerializationFeature.INDENT_OUTPUT)
                .build();
    }

    public ConfigurationManager(File userDirectory) {
        this.userDirectory = userDirectory;
        this.configuration = new File(userDirectory, "archivage-config.json");
    }

    public static ConfigurationManager forOs() {
        Path resolved = getOsSpecificPath();
        File directory = resolved.toFile();
        directory.mkdirs();
        return new ConfigurationManager(directory);
    }

    /**
     * {@return le répertoire à utiliser pour stocker la configuration selon le SE utilisé}
     */
    private static Path getOsSpecificPath() {
        FileSystem fileSystem = FileSystem.getCurrent();
        Path configurationDirectory;
        switch (fileSystem) {
            case LINUX ->
                configurationDirectory = Path.of(
                        FileUtils.getUserDirectoryPath(),
                        ".config",
                        "mim-libre-archivage"
                );
            case WINDOWS -> {
                String appdata = Strings.emptyToNull(System.getenv("APPDATA"));
                String dir = Optional.ofNullable(appdata)
                        .orElse(FileUtils.getUserDirectoryPath());
                configurationDirectory = Path.of(dir).resolve(Path.of("MIM-Libre", "Archivage"));
            }
            default -> throw new IllegalStateException("non géré " + fileSystem);
        }
        LOGGER.trace(configurationDirectory);
        return configurationDirectory;
    }

    /**
     * {@return la configuration obtenue une fois le fichier lu et validé}
     */
    @VisibleForTesting
    ConfigurationFileContent read() {
        Preconditions.checkState(configuration.exists());
        try {
            return jsonMapper.readValue(configuration, ConfigurationFileContent.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Valide la configuration passée, lève une erreur si le fichier est mal configuré.
     *
     * Contrôle que le fichier est dans le bon format (= version), que les URL sont valides,
     * les directives obligatoires renseignées, les valeurs cohérentes.
     *
     * On valide également les profils contenus dans le fichier.
     *
     * @param configurationFileContent la configuration à valider
     */
    private void check(ConfigurationFileContent configurationFileContent) throws ArchivageMisconfigurationException {
        if (configurationFileContent.getVersion() != EXPECTED_VERSION) {
            throw new ArchivageMisconfigurationException("version attendue " + EXPECTED_VERSION + ", version du fichier " + configurationFileContent.getVersion());
        }
        try {
            URI.create(configurationFileContent.getZimbraSoapUri());
        } catch (IllegalArgumentException e) {
            throw new ArchivageMisconfigurationException("URI SOAP Zimbra invalide " + configurationFileContent.getZimbraSoapUri(), e);
        }
        try {
            URI.create(configurationFileContent.getZimbraRestUri());
        } catch (IllegalArgumentException e) {
            throw new ArchivageMisconfigurationException("URI REST Zimbra invalide " + configurationFileContent.getZimbraSoapUri(), e);
        }
        if (configurationFileContent.getLoggingLevel() == null) {
            throw new ArchivageMisconfigurationException("Il faut préciser un niveau de journalisation. Valeurs possibles " + Arrays.toString(ArchivageLoggingLevel.values()));
        }
        if (configurationFileContent.getLoggingDaysOfRetention() < MIN_LOGGING_DAYS_OF_RETENTION) {
            throw new ArchivageMisconfigurationException("La durée de rétention des journaux doit être ≥ " + MIN_LOGGING_DAYS_OF_RETENTION);
        }
        if (configurationFileContent.getLoggingDaysOfRetention() > MAX_LOGGING_DAYS_OF_RETENTION) {
            throw new ArchivageMisconfigurationException("La durée de rétention des journaux doit être ≤ " + MAX_LOGGING_DAYS_OF_RETENTION);
        }
        if (configurationFileContent.getTimeZone() != null && !ZoneId.getAvailableZoneIds().contains(configurationFileContent.getTimeZone())) {
            throw new ArchivageMisconfigurationException("La zone " + configurationFileContent.getTimeZone() + " n’est pas valide. Zones possibles : " + ZoneId.getAvailableZoneIds());
        }
        if (configurationFileContent.getSynchronisationBatchSize() <= 1) {
            throw new ArchivageMisconfigurationException("La taille des batch doit être > 1");
        }
        for (ConfigurationFileContent.Profile profile : configurationFileContent.getProfiles()) {
            if (Strings.isNullOrEmpty(profile.getId())) {
              throw new ArchivageMisconfigurationException("Les profils doivent avoir des identifiants techniques");
            }
            if (Strings.isNullOrEmpty(profile.getLogin())) {
              throw new ArchivageMisconfigurationException("Les profils doivent avoir un identifiant/login");
            }

            ImmutableSet<Path> mountPointPaths = ImmutableSet.<Path>builder()
                    .add(profile.getSynchronizationMountPointPath())
                    .addAll(profile.getVisualizationMountPointPaths())
                    .build();
            ImmutableSetMultimap<Path, String> errorPerPaths = validateMountPointPaths(mountPointPaths);
            for (Map.Entry<Path, String> entry : errorPerPaths.entries()) {
                Path path = entry.getKey();
                String error = entry.getValue();
                throw new ArchivageMisconfigurationException("Erreur de configuration pour le dossier " + path + ". " + error);
            }
        }
    }

    /**
     * {@return les identifiants des profils existants dans la configuration}
     */
    public Set<String> getProfileIds() {
        ConfigurationFileContent configurationFileContent = read();
        return configurationFileContent.getProfiles().stream()
                .map(ConfigurationFileContent.Profile::getId)
                .collect(Collectors.toSet());
    }

    /**
     * Pour l’identifiant technique passé, toutes les informations sur le profil d’après la configuration.
     *
     * @param profileId un identifiant technique, voir {@link ConfigurationFileContent.Profile#getId()}
     */
    public Profile getProfile(String profileId) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile storedProfile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        File profileDirectory = getProfileDirectory(profileId);
        File indexDirectory = new File(profileDirectory, "index");
        indexDirectory.mkdirs();
        File logsDirectory = new File(profileDirectory, "journaux");
        logsDirectory.mkdirs();
        LOGGER.trace("selon le profil, l’index sera stocké dans " + indexDirectory);
        return new Profile() {

            @Override
            public Set<MountPoint> getMountPoints() {
                return ImmutableSet.<MountPoint>builder()
                        .add(getSynchronizationMountPoint())
                        .addAll(getVisualizationMountPoints())
                        .build();
            }

            @Override
            public MountPoint getSynchronizationMountPoint() {
                return new MountPoint(storedProfile.getSynchronizationMountPointPath());
            }

            @Override
            public Set<MountPoint> getVisualizationMountPoints() {
                return storedProfile.getVisualizationMountPointPaths().stream()
                        .map(MountPoint::new)
                        .collect(Collectors.toUnmodifiableSet());
            }

            @Override
            public Path getIndexPath() {
                return indexDirectory.toPath();
            }

            @Override
            public Path getLogsDirectoryPath() {
                return logsDirectory.toPath();
            }

            @Override
            public int getRemoteEmailsMustBeArchivedAfterDaysCount() {
                return storedProfile.getRemoteEmailsMustBeArchivedAfterDaysCount();
            }

            @Override
            public Path getDefaultAttachmentsSavingPath() {
                return storedProfile.getDefaultAttachmentsSavingPath();
            }

            @Override
            public String getProfileId() {
                return storedProfile.getId();
            }

            @Override
            public ZonedDateTime getLastSynchronisation() {
                return storedProfile.getLastSynchronisation();
            }

            @Override
            public String getLogin() {
                return storedProfile.getLogin();
            }

            @Override
            public MailTableSortPreference getMailTableSortPreference() {
                return storedProfile.getMailTableSortPreference();
            }

            @Override
            public Map<String, Boolean> getExpandedTreeItemIds() {
                return storedProfile.getExpandedTreeItemIds();
            }

            @Override
            public double getTableToVisualisationSplitPaneDividerPosition() {
                return storedProfile.getTableToVisualisationSplitPaneDividerPosition();
            }

            @Override
            public double getFilesManagerToMailsSplitPaneDividerPosition() {
                return storedProfile.getFilesManagerToMailsSplitPaneDividerPosition();
            }
        };
    }

    private File getProfileDirectory(String profileId) {
        File profileDirectory = new File(userDirectory, profileId);
        return profileDirectory;
    }

    /**
     * Création d’un nouveau profil utilisateur à enregistrer dans la configuration.
     *
     * @param login l’adresse courriel utilisée pour la synchronisation
     * @param synchronizationMountPointPath le répertoire à utiliser comme {@link MountPoint}
     * @return l’identifiant technique du profil créé
     */
    public String createProfile(String login, Path synchronizationMountPointPath) {
        Preconditions.checkArgument(Files.isDirectory(synchronizationMountPointPath));
        Preconditions.checkArgument(Files.isWritable(synchronizationMountPointPath));
        ConfigurationFileContent configurationFileContent = read();
        Set<String> existingProfileIds = configurationFileContent.getProfiles().stream()
                .map(ConfigurationFileContent.Profile::getId)
                .collect(Collectors.toSet());
        int newId = 0;
        do {
            newId += 1;
        } while (existingProfileIds.contains(String.valueOf(newId)));
        ConfigurationFileContent.Profile profile = new ConfigurationFileContent.Profile();
        profile.setId(String.valueOf(newId));
        profile.setSynchronizationMountPointPath(synchronizationMountPointPath);
        profile.setRemoteEmailsMustBeArchivedAfterDaysCount(1000);
        profile.setDefaultAttachmentsSavingPath(Path.of(FileUtils.getUserDirectoryPath()));
        profile.setMailTableSortPreference(new ConfigurationFileContent.Profile.MailTableSortPreference());
        profile.setTableToVisualisationSplitPaneDividerPosition(0.5);
        profile.setFilesManagerToMailsSplitPaneDividerPosition(0.3);
        profile.setLogin(login);
        configurationFileContent.getProfiles().add(profile);
        save(configurationFileContent);
        return profile.getId();
    }

    public void deleteProfile(String profileToRemoveId) {
        ConfigurationFileContent configurationFileContent = read();
        configurationFileContent.getProfiles().removeIf(profile -> profile.getId().equals(profileToRemoveId));
        File profileDirectory = getProfileDirectory(profileToRemoveId);
        try {
            FileUtils.deleteDirectory(profileDirectory);
        } catch (IOException e) {
            throw new RuntimeException("impossible de supprimer " + profileDirectory + " alors qu’on cherche à supprimer le profil " + profileToRemoveId, e);
        }
        save(configurationFileContent);
    }

    /**
     * Enregistrer la date de la de dernière synchronisation.
     *
     * Le but n’est pas tant de permettre la configuration que de conserver l'info entre les lancements de l'appli.
     *
     * @param profileId le profil qui a été synchronisé
     * @param lastSynchronisation la date à laquelle la synchro terminée a commencé
     */
    public void setLastSynchronisation(String profileId, ZonedDateTime lastSynchronisation) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.setLastSynchronisation(lastSynchronisation);
        save(configurationFileContent);
    }

    /**
     * Enregistrer le choix de l’utilisateur pour le dossier préféré où enregistre les pièces-jointes.
     *
     * @param profileId le profil qui a changé
     * @param file le chemin vers le dossier qu’il faut proposer pour l’enregistrement des pièces jointes
     */
    public void setDefaultAttachmentsSavingPath(String profileId, File file) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.setDefaultAttachmentsSavingPath(file.toPath());
        save(configurationFileContent);
    }

    /**
     * Enregistrer le choix de l’utilisateur sur la durée de rétention des messages avant archivage sur le serveur
     *
     * @param profileId le profil qui a changé
     * @param remoteEmailsMustBeArchivedAfterDaysCount la durée en jours
     */
    public void setRemoteEmailsMustBeArchivedAfterDaysCount(String profileId, int remoteEmailsMustBeArchivedAfterDaysCount) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.setRemoteEmailsMustBeArchivedAfterDaysCount(remoteEmailsMustBeArchivedAfterDaysCount);
        save(configurationFileContent);
        LOGGER.info("pour le profil " + profileId + " on fixe un nombre de jours à partir duquel un courriel doit être archivé à " + remoteEmailsMustBeArchivedAfterDaysCount);
    }

    /**
     * Enregistrer le choix de l’utilisateur sur la durée de rétention des journaux utilisateur.
     *
     * @param loggingDaysOfRetention la durée de rétention en jours
     */
    public void setLoggingDaysOfRetention(int loggingDaysOfRetention) {
        ConfigurationFileContent configurationFileContent = read();
        configurationFileContent.setLoggingDaysOfRetention(loggingDaysOfRetention);
        save(configurationFileContent);
        LOGGER.info("on fixe la durée de rétention des journaux à " + loggingDaysOfRetention + " jours");
    }

    public void addVisualizationMountPoint(String profileId, MountPoint mountPoint) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.getVisualizationMountPointPaths().add(mountPoint.root());
        save(configurationFileContent);
        LOGGER.info("pour le profil " + profileId + " on ajoute le point de montage " + mountPoint);
    }

    public void removeVisualizationMountPoint(String profileId, MountPoint mountPointToRemove) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.getVisualizationMountPointPaths().remove(mountPointToRemove.root());
        save(configurationFileContent);
        LOGGER.info("pour le profil " + profileId + " on retire le point de montage " + mountPointToRemove);
    }

    /**
     * Enregistrer dans le fichier de configuration la conf passée.
     */
    private void save(ConfigurationFileContent configurationFileContent) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(configuration)) {
            jsonMapper.writeValue(fileOutputStream, configurationFileContent);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Enregistre la préfèrence utilisateur quant à l’ordre d’affichage des courriels dans l’interface.
     *
     * @param profileId le profil concerné
     * @param mailTableSortPreference la consigne de tri
     */
    public void saveMailTableSortPreference(String profileId, ConfigurationFileContent.Profile.MailTableSortPreference mailTableSortPreference) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.setMailTableSortPreference(mailTableSortPreference);
        save(configurationFileContent);
        LOGGER.info("pour le profil " + profileId + " on fixe la règle de tri à " + mailTableSortPreference);
    }

    public void saveExpandedTreeItemIds(String profileId, Map<String, Boolean> expandedTreeItemIds) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.setExpandedTreeItemIds(expandedTreeItemIds);
        save(configurationFileContent);
        LOGGER.info("pour le profil " + profileId + " on retient les éléments de l’arbre à ouvrir au prochain démarrage " + expandedTreeItemIds);
    }

    public void saveTableToVisualisationSplitPaneDividerPosition(String profileId, double dividerPosition) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.setTableToVisualisationSplitPaneDividerPosition(dividerPosition);
        save(configurationFileContent);
        LOGGER.info("pour le profil " + profileId + " on retient une position du séparateur entre le tableau des courriels et la visualisation de " + dividerPosition);
    }

    public void saveFilesManagerToMailsSplitPaneDividerPosition(String profileId, double dividerPosition) {
        ConfigurationFileContent configurationFileContent = read();
        ConfigurationFileContent.Profile profile = configurationFileContent.getProfiles().stream()
                .filter(aProfile -> aProfile.getId().equals(profileId))
                .collect(MoreCollectors.onlyElement());
        profile.setFilesManagerToMailsSplitPaneDividerPosition(dividerPosition);
        save(configurationFileContent);
        LOGGER.info("pour le profil " + profileId + " on retient une position du séparateur entre l'arborescence et les mails " + dividerPosition);
    }

    /**
     * Charge la configuration en créant à la volée une configuration par défaut si besoin.
     */
    public ArchivageConfiguration getConfiguration() throws ArchivageMisconfigurationException {
        if (!configuration.exists()) {
            ArchivageProperties archivageProperties = null;
            try {
                archivageProperties = new ArchivageProperties();
            } catch (PropertiesFileNotFoundArchivageException e) {
                throw new RuntimeException(e);
            }

            ConfigurationFileContent empty = new ConfigurationFileContent();
            empty.setVersion(EXPECTED_VERSION);
            empty.setZimbraSoapUri(archivageProperties.getZimbraSoapUri());
            empty.setZimbraRestUri(archivageProperties.getZimbraRestUri());
            empty.setSpecificClientName(archivageProperties.getSpecificClientName());
            empty.setLoggingLevel(ArchivageLoggingLevel.STANDARD);
            empty.setLoggingDaysOfRetention(MIN_LOGGING_DAYS_OF_RETENTION);
            empty.setSynchronisationBatchSize(500);
            save(empty);
            return getConfiguration();
        }
        ConfigurationFileContent configurationFileContent = read();
        check(configurationFileContent);
        return new ArchivageConfiguration() {
            @Override
            public URI getZimbraSoapUri() {
                return URI.create(configurationFileContent.getZimbraSoapUri());
            }

            @Override
            public URI getZimbraRestUri() {
                return URI.create(configurationFileContent.getZimbraRestUri());
            }

            @Override
            public String getSpecificClientName() {
                return configurationFileContent.getSpecificClientName();
            }

            @Override
            public ArchivageLoggingLevel getLoggingLevel() {
                return configurationFileContent.getLoggingLevel();
            }

            @Override
            public int getLoggingDaysOfRetention() {
                return configurationFileContent.getLoggingDaysOfRetention();
            }

            @Override
            public ZoneId getZoneId() {
                return Optional.ofNullable(configurationFileContent.getTimeZone())
                        .map(ZoneId::of)
                        .orElseGet(ZoneId::systemDefault);
            }

            @Override
            public int getSynchronisationBatchSize() {
                return configurationFileContent.getSynchronisationBatchSize();
            }

            @Override
            public boolean isDeletionOnSynchronizationEnabled() {
                return true;
            }

            @Override
            public int getMaxFileNameLength() {
                return configurationFileContent.getMaxFileNameLength();
            }
        };
    }

    public ImmutableSetMultimap<Path, String> validateMountPointPaths(Set<Path> mountPointPaths) {
        ImmutableSortedSet<Path> sortedMountPointPaths = ImmutableSortedSet.copyOf(mountPointPaths);
        ImmutableSetMultimap.Builder<Path, String> errorPerPathsBuilder = ImmutableSetMultimap.builder();
        for (Path mountPointPath : mountPointPaths) {
            if (!Files.exists(mountPointPath)) {
                errorPerPathsBuilder.put(mountPointPath, "Le répertoire " + mountPointPath + " n’existe pas");
            } else if (!Files.isDirectory(mountPointPath)) {
                errorPerPathsBuilder.put(mountPointPath, "Le chemin " + mountPointPath + " n’est pas un dossier");
            } else if (!Files.isReadable(mountPointPath)) {
                errorPerPathsBuilder.put(mountPointPath, "Le répertoire " + mountPointPath + " n’est pas accessible en lecture");
            } else if (!Files.isWritable(mountPointPath)) {
                errorPerPathsBuilder.put(mountPointPath, "Le répertoire " + mountPointPath + " n’est pas accessible en écriture");
            } else {
                for (Path anotherMountPointPath : sortedMountPointPaths.headSet(mountPointPath)) {
                    if (anotherMountPointPath.startsWith(mountPointPath)) {
                        errorPerPathsBuilder.put(mountPointPath, "Le dossier " + mountPointPath + " contient déjà le dossier " + anotherMountPointPath);
                    } else if (mountPointPath.startsWith(anotherMountPointPath)) {
                        errorPerPathsBuilder.put(mountPointPath, "Le dossier " + mountPointPath + " est déjà contenu dans le dossier " + anotherMountPointPath);
                    }
                }
            }
        }
        ImmutableSetMultimap<Path, String> errorPerPaths = errorPerPathsBuilder.build();
        LOGGER.trace("Problèmes identifiés à la validation de " + mountPointPaths + " = " + errorPerPaths);
        return errorPerPaths;
    }
}
