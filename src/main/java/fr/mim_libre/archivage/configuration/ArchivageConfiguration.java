package fr.mim_libre.archivage.configuration;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.logging.ArchivageLoggingLevel;

import java.net.URI;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Représente l’ensemble des directives de configuration.
 *
 * Certaines sont figées et ne peuvent être changé, il s’agit de permettre de changer la valeur dans une future version
 * ou une version adaptée pour tel ou tel client.
 */
public interface ArchivageConfiguration {

    /**
     * {@return L’URL vers l'API SOAP de Zimbra}
     */
    URI getZimbraSoapUri();

    /**
     * {@return L’URL vers l'API ReST de Zimbra}
     */
    URI getZimbraRestUri();

    /**
     * {@return le niveau de journalisation utilisateur souhaité}
     */
    ArchivageLoggingLevel getLoggingLevel();

    /**
     * {@return le nombre de jours de rétention des journaux utilisateurs}
     */
    int getLoggingDaysOfRetention();

    /**
     * {@return le fuseau horraire dans lequel il faut afficher les heures, s'il est différent de celui système}
     */
    ZoneId getZoneId();

    /**
     * {@return l’horloge pour savoir quand est-on}
     */
    default Clock getClock() {
        return Clock.system(getZoneId());
    }

    /**
     * {@return le nom de l’étiquette posée par l’utilisateur sur les courriels qu’il souhaite archiver lors de la synchronisation}
     */
    default String getEmailToBeArchivedTag() {
        return "Archiver";
    }

    /**
     * {@return le nom de l’étiquette posée par l’utilisateur sur les courriels qu’il souhaite archiver lors de la synchronisation}
     */
    default String getEmailToIgnoreWhileArchivingTag() {
        return "Ne pas archiver";
    }

    /**
     * {@return le nom de l’étiquette posée par archivage sur les courriels qu’on a pas réussi à lire lors de la synchro}
     */
    default String getNonArchivableEmailTag() {
        return "Non archivable";
    }

    /**
     * {@return ce qu'il faut considérer comme étant « Maintenant »}
     */
    default ZonedDateTime now() {
        LocalDateTime now = LocalDateTime.now(getClock());
        return ZonedDateTime.of(now, getZoneId());
    }

    /**
     * {@return nombre de jours en deça duquel on considère qu’une synchronisation est suffisamment récente pour être considéré « à jour »}
     */
    default int maxDaysForUpToDateSynchronization() {
        return 1;
    }

    /**
     * {@return nombre de jours à partir duquel on considère qu’une synchronisation est trop lointaine et qu’il faut prévenir l’utilisateur}
     */
    default int maxDaysForAgingSynchronization() {
        return 31;
    }

    /**
     * {@return la taille des lots à utiliser quand on télécharge en masse depuis Zimbra}
     */
    int getSynchronisationBatchSize();

    /**
     * {@return si on supprime les courriels du serveur Zimbra après qu'ils aient été téléchargés en local pendant la
     * synchronisation. Pour éviter tout comportement non désiré, par défaut on désactive la suppression}
     */
    boolean isDeletionOnSynchronizationEnabled();

    /**
     * @return le nom de l'application
     */
    default String getApplicationName() { return "Archivage"; }

    /**
     * @return le nom à ajouter en plus du nom de l'application pour customiser l'application
     */
    String getSpecificClientName();

    /**
     * {@return le nom du dossier dans la boîte Zimbra distante dans lequel on va téléverser les courriels lors de la restauration}
     */
    default String getRestorationTargetFolderName() {
        return "Restauration depuis Archivage";
    }

    /**
     * @return la taille maximale des noms des fichiers
     */
    int getMaxFileNameLength();
}
