package fr.mim_libre.archivage.configuration;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.PropertiesFileNotFoundArchivageException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ArchivageProperties {
    private static final Logger LOGGER = LogManager.getLogger(ArchivageProperties.class);

    private String zimbraRestUri;
    private String zimbraSoapUri;
    private String specificClientName;
    private String archivageLogo;

    public ArchivageProperties() throws PropertiesFileNotFoundArchivageException {
        if (zimbraRestUri == null) {
            try (final InputStream propertiesStream = ArchivageProperties.class.getResourceAsStream("archivage.properties")) {
                Properties properties = new Properties();
                properties.load(propertiesStream);

                zimbraRestUri = properties.getProperty("zimbra.rest.uri");
                zimbraSoapUri = properties.getProperty("zimbra.soap.uri");
                archivageLogo = properties.getProperty("archivage.logo");
                specificClientName = properties.getProperty("archivage.client.name");
            } catch (IOException e) {
                LOGGER.error("Impossible de charger la configuration", e);
                throw new PropertiesFileNotFoundArchivageException("", e);
            }
        }
    }

    public String getZimbraRestUri() {
        return zimbraRestUri;
    }

    public String getZimbraSoapUri() {
        return zimbraSoapUri;
    }

    public String getArchivageLogo() {
        return archivageLogo;
    }

    public String getSpecificClientName() { return specificClientName; }
}
