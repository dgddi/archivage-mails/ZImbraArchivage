package fr.mim_libre.archivage.configuration;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.views.mails.MailTableColumn;
import javafx.scene.control.TableColumn;

import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;

/**
 * Représente un profil utilisateur. Un profil = une boîte courriel à synchroniser.
 */
public interface Profile {

    /**
     * {@return tous les points de montages}
     */
    Set<MountPoint> getMountPoints();

    Set<MountPoint> getVisualizationMountPoints();

    /**
     * {@return le répertoire local dans lequel on stocker l’index de tous les courriels connus}
     */
    Path getIndexPath();

    /**
     * {@return le répertoire local où on va stocker les journaux utilisateurs pour ce profil}
     */
    Path getLogsDirectoryPath();

    /**
     * {@return le nombre de jours au delà desquels un courriel dans la boîte distante est considéré à synchroniser}
     */
    int getRemoteEmailsMustBeArchivedAfterDaysCount();

    /**
     * {@return le point de montage dans lequel on enregistrera les courriels téléchargés pendant la synchronisation}
     */
    MountPoint getSynchronizationMountPoint();

    /**
     * {@return le chemin que l’utilisateur veut se voir proposé au moment d’enregistrer les pièces jointes d’un courriel donné}
     */
    Path getDefaultAttachmentsSavingPath();

    /**
     * {@return l’identifiant technique du profil}
     */
    String getProfileId();

    /**
     * {@return pour la dernière synchronisation terminée, la date où elle a commencé}
     */
    ZonedDateTime getLastSynchronisation();

    /**
     * {@return l’adresse courriel qui sera utilisée comme identifiant pour se connecter au serveur}
     */
    String getLogin();

    /**
     * {@return la préférence de l’utilisateur quant au tri à l’affichage des messages}
     */
    MailTableSortPreference getMailTableSortPreference();

    /**
     * {@return les éléments de l’arbre existants lors de la dernière fermeture de l’application et si chacun étant ouvert/fermé dans l’interface}
     */
    Map<String, Boolean> getExpandedTreeItemIds();

    /**
     * {@return préférence de l’utilisateur quant à la position du sépérateur entre le tableaux des courriels et la visualisation}
     */
    double getTableToVisualisationSplitPaneDividerPosition();

    /**
     * {@return préférence de l’utilisateur quant à la position du sépérateur entre l'arborescence et les mails}
     */
    double getFilesManagerToMailsSplitPaneDividerPosition();

    /**
     * Une consigne de tri de courriels.
     */
    interface MailTableSortPreference {

        /**
         * {@return le critère de tri}
         */
        MailTableColumn getColumn();

        /**
         * {@return l’ordre de tri}
         */
        TableColumn.SortType getSortType();
    }
}
