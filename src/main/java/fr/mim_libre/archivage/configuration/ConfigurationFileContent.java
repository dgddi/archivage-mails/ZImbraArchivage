package fr.mim_libre.archivage.configuration;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.logging.ArchivageLoggingLevel;
import fr.mim_libre.archivage.views.mails.MailTableColumn;
import javafx.scene.control.TableColumn;

import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * POJO pour assurer la lecture/écriture du fichier de configuration en JSON.
 */
public class ConfigurationFileContent {

    private int version = 0;

    private String zimbraSoapUri;

    private String zimbraRestUri;

    private String specificClientName;

    private ArchivageLoggingLevel loggingLevel;

    private int loggingDaysOfRetention;

    private String timeZone;

    private int synchronisationBatchSize;

    private Set<Profile> profiles = new LinkedHashSet<>();

    private int maxFileNameLength = 50;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getZimbraSoapUri() {
        return zimbraSoapUri;
    }

    public void setZimbraSoapUri(String zimbraSoapUri) {
        this.zimbraSoapUri = zimbraSoapUri;
    }

    public String getZimbraRestUri() {
        return zimbraRestUri;
    }

    public void setZimbraRestUri(String zimbraRestUri) {
        this.zimbraRestUri = zimbraRestUri;
    }

    public String getSpecificClientName() { return specificClientName; }

    public void setSpecificClientName(String specificClientName) {
        this.specificClientName = specificClientName;
    }

    public ArchivageLoggingLevel getLoggingLevel() {
        return loggingLevel;
    }

    public void setLoggingLevel(ArchivageLoggingLevel loggingLevel) {
        this.loggingLevel = loggingLevel;
    }

    public int getLoggingDaysOfRetention() {
        return loggingDaysOfRetention;
    }

    public void setLoggingDaysOfRetention(int loggingDaysOfRetention) {
        this.loggingDaysOfRetention = loggingDaysOfRetention;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public int getSynchronisationBatchSize() {
        return synchronisationBatchSize;
    }

    public void setSynchronisationBatchSize(int synchronisationBatchSize) {
        this.synchronisationBatchSize = synchronisationBatchSize;
    }

    public Set<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }

    public int getMaxFileNameLength() {
        return maxFileNameLength;
    }

    public void setMaxFileNameLength(int maxFileNameLength) {
        this.maxFileNameLength = Math.max(50, Math.min(maxFileNameLength, 180));
    }

    public static class Profile {

        private String id;

        private Path synchronizationMountPointPath;

        private Set<Path> visualizationMountPointPaths = new LinkedHashSet<>();

        private String login;

        private ZonedDateTime lastSynchronisation;

        private int remoteEmailsMustBeArchivedAfterDaysCount;

        private Path defaultAttachmentsSavingPath;

        private MailTableSortPreference mailTableSortPreference;

        private Map<String, Boolean> expandedTreeItemIds = new TreeMap<>();

        private double tableToVisualisationSplitPaneDividerPosition;

        private double filesManagerToMailsSplitPaneDividerPosition;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Path getSynchronizationMountPointPath() {
            return synchronizationMountPointPath;
        }

        public void setSynchronizationMountPointPath(Path synchronizationMountPointPath) {
            this.synchronizationMountPointPath = synchronizationMountPointPath;
        }

        public Set<Path> getVisualizationMountPointPaths() {
            return visualizationMountPointPaths;
        }

        public void setVisualizationMountPointPaths(Set<Path> visualizationMountPointPaths) {
            this.visualizationMountPointPaths = visualizationMountPointPaths;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public ZonedDateTime getLastSynchronisation() {
            return lastSynchronisation;
        }

        public void setLastSynchronisation(ZonedDateTime lastSynchronisation) {
            this.lastSynchronisation = lastSynchronisation;
        }

        public void setRemoteEmailsMustBeArchivedAfterDaysCount(int remoteEmailsMustBeArchivedAfterDaysCount) {
            this.remoteEmailsMustBeArchivedAfterDaysCount = remoteEmailsMustBeArchivedAfterDaysCount;
        }

        public int getRemoteEmailsMustBeArchivedAfterDaysCount() {
            return remoteEmailsMustBeArchivedAfterDaysCount;
        }

        public Path getDefaultAttachmentsSavingPath() {
            return defaultAttachmentsSavingPath;
        }

        public void setDefaultAttachmentsSavingPath(Path defaultAttachmentsSavingPath) {
            this.defaultAttachmentsSavingPath = defaultAttachmentsSavingPath;
        }

        public MailTableSortPreference getMailTableSortPreference() {
            return mailTableSortPreference;
        }

        public void setMailTableSortPreference(MailTableSortPreference mailTableSortPreference) {
            this.mailTableSortPreference = mailTableSortPreference;
        }

        public void setExpandedTreeItemIds(Map<String, Boolean> expandedTreeItemIds) {
            this.expandedTreeItemIds = expandedTreeItemIds;
        }

        public Map<String, Boolean> getExpandedTreeItemIds() {
            return expandedTreeItemIds;
        }

        public void setTableToVisualisationSplitPaneDividerPosition(double tableToVisualisationSplitPaneDividerPosition) {
            this.tableToVisualisationSplitPaneDividerPosition = tableToVisualisationSplitPaneDividerPosition;
        }

        public void setFilesManagerToMailsSplitPaneDividerPosition(double filesManagerToMailsSplitPaneDividerPosition) {
            this.filesManagerToMailsSplitPaneDividerPosition = filesManagerToMailsSplitPaneDividerPosition;
        }

        public double getTableToVisualisationSplitPaneDividerPosition() {
            return tableToVisualisationSplitPaneDividerPosition;
        }

        public double getFilesManagerToMailsSplitPaneDividerPosition() {
            return filesManagerToMailsSplitPaneDividerPosition;
        }

        public static class MailTableSortPreference implements fr.mim_libre.archivage.configuration.Profile.MailTableSortPreference {

            private MailTableColumn column = MailTableColumn.SENT_DATE;

            private TableColumn.SortType sortType = TableColumn.SortType.DESCENDING;

            @Override
            public MailTableColumn getColumn() {
                return column;
            }

            public void setColumn(MailTableColumn column) {
                this.column = column;
            }

            @Override
            public TableColumn.SortType getSortType() {
                return sortType;
            }

            public void setSortType(TableColumn.SortType sortType) {
                this.sortType = sortType;
            }
        }
    }
}
