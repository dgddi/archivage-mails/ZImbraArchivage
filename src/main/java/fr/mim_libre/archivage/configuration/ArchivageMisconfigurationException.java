package fr.mim_libre.archivage.configuration;

import fr.mim_libre.archivage.ArchivageException;

public class ArchivageMisconfigurationException extends ArchivageException {

    public ArchivageMisconfigurationException(String message) {
        super(message);
    }

    public ArchivageMisconfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
