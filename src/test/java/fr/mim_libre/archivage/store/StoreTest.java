package fr.mim_libre.archivage.store;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Throwables;
import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import fr.mim_libre.archivage.configuration.ArchivageConfigurationForTesting;
import fr.mim_libre.archivage.ArchivageEmail;
import fr.mim_libre.archivage.logging.ArchivageLogger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class StoreTest {

    private static final Logger LOGGER = LogManager.getLogger();

    private Store store;

    private MountPoint mountPoint;

    @Before
    public void setUp() throws Exception {
        ArchivageConfiguration configuration = new ArchivageConfigurationForTesting();
        Path archivage = Files.createTempDirectory("archivage");
        archivage.resolve(Path.of("dossier", "sous-dossier", "sous-sous-dossier")).toFile().mkdirs();
        mountPoint = new MountPoint(archivage);
//        mountPoint = new MountPoint(Path.of("/tmp/spams"));
        Path logsPath = Files.createTempDirectory("archivage-journaux");
        ArchivageLogger archivageLogger = new ArchivageLogger(configuration, logsPath);
        store = new Store(archivageLogger);
    }

    @Test
    public void getLocalDirectories() {
        try {
            Set<Folder> existingFolders = store.getExistingFolders(mountPoint);
            LOGGER.debug(existingFolders);
            Assert.assertEquals(4, existingFolders.size());
        } catch (ArchivageStoreException e) {
            Assert.fail("erreur inattendue");
        }
    }

    @Test
    @Ignore("en attente de données de test")
    public void scanRoot() {
        Set<ArchivageEmail> emails = new LinkedHashSet<>();
        EmlFileVisitor emlFileVisitor = new EmlFileVisitor() {
            @Override
            public void onEmlFileReadSuccess(ArchivageEmail archivageEmail) {
                emails.add(archivageEmail);
            }

            @Override
            public void onEmlFileReadFailure(EmlFilePath emlFilePath, ErrorReadingEmlFileException errorReadingEmlFileException) {
                Throwable rootCause = Throwables.getRootCause(errorReadingEmlFileException);
                String errorMessage = rootCause.getMessage();
                LOGGER.debug("impossible de lire " + emlFilePath + " cause = " + errorMessage);
            }
        };
        StoreScanProgressHandler storeScanProgressHandler = new StoreScanProgressHandler() {

            int numberOfEmlFilesRead = 0;

            @Override
            public void onFilesListed(int numberOfEmlFilesFound) {
                Assert.assertEquals(8574, numberOfEmlFilesFound);
                Assert.assertEquals(numberOfEmlFilesRead, numberOfEmlFilesFound);
            }

            @Override
            public void onOneFileRead() {
                numberOfEmlFilesRead += 1;
            }

            @Override
            public void onScanFailure(MountPoint mountPoint, ArchivageStoreException archivageStoreException) {
                Assert.fail("erreur inattendue");
            }
        };
        store.scanPathAndConsumeEmlFiles(Collections.singleton(mountPoint), emlFileVisitor, storeScanProgressHandler);
        Assert.assertEquals(8574, emails.size());
    }
}
