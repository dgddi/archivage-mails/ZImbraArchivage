package fr.mim_libre.archivage.store;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.configuration.ArchivageConfiguration;
import fr.mim_libre.archivage.configuration.ArchivageConfigurationForTesting;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.util.Date;

public class MountPointTest {
    private int maxFileNameLength;

    @Before
    public void setUp() throws Exception {
        ArchivageConfiguration configuration = new ArchivageConfigurationForTesting();

        maxFileNameLength = configuration.getMaxFileNameLength();
    }


    @Test
    public void testPath() {
        MountPoint mountPoint = new MountPoint(Path.of("/tmp/spams"));
        {
            Folder folder = mountPoint.folder(Path.of("a", "b"));
            Assert.assertEquals("b", folder.getName());
            EmlFilePath emlFilePath = folder.getEmlFilePath("machin.eml");
            Assert.assertEquals("/tmp/spams/a/b/machin.eml", emlFilePath.toFullPath().toString());
        }
        {
            Folder folder = mountPoint.folder(Path.of(""));
            EmlFilePath emlFilePath = folder.getEmlFilePath("machin.eml");
            Assert.assertEquals("/tmp/spams/machin.eml", emlFilePath.toFullPath().toString());
        }
        {
            Assert.assertThrows(IllegalArgumentException.class, () -> mountPoint.folder(Path.of("/")));
        }
    }

    @Test
    public void testParenting() {
        MountPoint mountPoint = new MountPoint(Path.of("/tmp/spams"));
        Folder folder = mountPoint.folder(Path.of("a", "b", "c"));
        Folder parent = folder.getParent();
        Assert.assertEquals(mountPoint.folder(Path.of("a" ,"b")), parent);
        Assert.assertThrows(IllegalArgumentException.class, () -> mountPoint.rootFolder().getParent());
        Assert.assertEquals(mountPoint.rootFolder(), mountPoint.folder(Path.of("a" )).getParent());
    }

    @Test
    public void testTranslations() {
        MountPoint mountPoint = new MountPoint(Path.of("/tmp/spams"));
        Folder folder = mountPoint.folder(Path.of("Inbox"));
        Assert.assertEquals("Réception", folder.getHumanReadableName());
    }

    @Test
    public void testFileNameTruncate() {
        MountPoint mountPoint = new MountPoint(Path.of("/tmp/spams"));
        Folder folder = mountPoint.folder(Path.of("Inbox"));
        Date fakeDate = new Date();
        DateUtils.setDays(fakeDate, 29);
        DateUtils.setMonths(fakeDate, 11);
        DateUtils.setYears(fakeDate, 2023);
        for (int i = 0; i < 1000; i++) {
            EmlFilePath emlFilePath = folder.getEmlFilePath("12345", fakeDate, StringUtils.repeat("x", i), maxFileNameLength);
            Assert.assertTrue(emlFilePath.fileName().toString().length() <= EmlFilePath.MAX_FILE_NAME_LENGTH);
        }
    }
}
