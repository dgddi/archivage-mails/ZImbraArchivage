package fr.mim_libre.archivage.logging;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.PropertiesFileNotFoundArchivageException;
import fr.mim_libre.archivage.configuration.ArchivageConfigurationForTesting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ArchivageLoggerTest {

    private static final Logger LOGGER = LogManager.getLogger();

    private ArchivageLogger archivageLogger;

    @Before
    public void setUp() throws IOException, PropertiesFileNotFoundArchivageException {
        ArchivageConfigurationForTesting configuration = new ArchivageConfigurationForTesting();
        Path logsPath = Files.createTempDirectory("archivage-journaux");
        archivageLogger = new ArchivageLogger(configuration, logsPath);
    }

    @Test
    public void clean() {
        archivageLogger.getToBeDeletedLogFiles();
    }

    @Test
    public void write() {
        archivageLogger.write("une ligne");
        archivageLogger.write("une seconde ligne");
        archivageLogger.write("une troisième ligne");
        String logContent = archivageLogger.getLogContent();
        LOGGER.debug("contenu du journal " + logContent);
    }
}
