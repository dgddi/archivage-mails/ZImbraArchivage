package fr.mim_libre.archivage.index;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Range;
import fr.mim_libre.archivage.ArchivageEmail;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.store.MountPoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class ArchivageIndexTest {

    private static final Logger LOGGER = LogManager.getLogger();

    private ArchivageIndex archivageIndex;

    @Before
    public void setUp() throws IOException {
        Path indexPath = Files.createTempDirectory("archivage");
        archivageIndex = new ArchivageIndex(indexPath);
    }

    @After
    public void tearDown() {
        archivageIndex.delete();
    }

    @Test
    public void writeIndexAndSearch() throws MissingRelevantKeywordException {
        MountPoint mountPoint = new MountPoint(Path.of("point de montage"));
        Folder folder = mountPoint.folder("dossier/sous-dossier");

        try (ArchivageIndex.Writer archivageIndexWriter = archivageIndex.openWriter()) {
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("a"), "Salut", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(3), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("b"), "Everybody", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.SOME_ATTACHMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(4), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("c"), "Tout", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "corps du message en texte brut", null, getDatePlus(5), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("d"), "Le", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.SOME_ATTACHMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(6), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("e"), "Monde", new ArchivageEmail.Recipient("Bob", "bob@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, null, "corps du message en HTML", getDatePlus(7), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("f"), "C’est", new ArchivageEmail.Recipient("Bob", "bob@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.SOME_ATTACHMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(8), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("g"), "Mardi et un autre mot", new ArchivageEmail.Recipient("Bob", "bob@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(9), Set.of(), "<Source EML>"));
        }

        {
            List<IndexSearchResult> subjectSearchResults = archivageIndex.search(new ArchivageQuery(null, null, null, null, HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.SUBJECT, "Monde"));
            Assert.assertEquals(1, subjectSearchResults.size());
            Assert.assertEquals(folder.getEmlFilePath("e"), Iterables.getOnlyElement(subjectSearchResults).emlFilePath());
        }

        {
            List<IndexSearchResult> fromSearchResults = archivageIndex.search(new ArchivageQuery("Alice", null, null, null, HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.IGNORED, null));
            Assert.assertEquals(4, fromSearchResults.size());
        }

        {
            List<IndexSearchResult> twoCriteriaSearchResults = archivageIndex.search(new ArchivageQuery("Bob", null, null, null, HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.SUBJECT, "autre"));
            Assert.assertEquals(1, twoCriteriaSearchResults.size());
        }

        {
            List<IndexSearchResult> hasAnyAttachmentSearchResults = archivageIndex.search(new ArchivageQuery(null, null, null, null, HasAnyAttachment.SOME_ATTACHMENT, null, folder, SearchKeywordStrategy.IGNORED, null));
            Assert.assertEquals(3, hasAnyAttachmentSearchResults.size());
        }

        {
            List<IndexSearchResult> dateSearchResults = archivageIndex.search(new ArchivageQuery(null, null, null, Range.atMost(getDatePlus(7)), HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.IGNORED, null));
            Assert.assertEquals(5, dateSearchResults.size());
        }
    }

    @Test
    public void writeIndexAndSearchByKeyword() {
        MountPoint mountPoint = new MountPoint(Path.of("point de montage"));
        Folder folder = mountPoint.folder("dossier/sous-dossier");

        try (ArchivageIndex.Writer archivageIndexWriter = archivageIndex.openWriter()) {
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("a"), "Avion", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "Il a été intercepté", "<html>", getDatePlus(3), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("b"), "Interception", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.SOME_ATTACHMENT, "C’était un avion", "<html>", getDatePlus(4), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folder.getEmlFilePath("c"), "Voiture interceptée", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "Nous avons intercepté une voiture", null, getDatePlus(5), Set.of(), "<Source EML>"));
        }

        try {
            {
                List<IndexSearchResult> subjectSearchResults = archivageIndex.search(new ArchivageQuery(null, null, null, null, HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.SUBJECT, "avion"));
                Assert.assertEquals(1, subjectSearchResults.size());
                Assert.assertEquals(folder.getEmlFilePath("a"), Iterables.getOnlyElement(subjectSearchResults).emlFilePath());
            }

            {
                List<IndexSearchResult> subjectSearchResults = archivageIndex.search(new ArchivageQuery(null, null, null, null, HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.BODY, "avion"));
                Assert.assertEquals(1, subjectSearchResults.size());
                Assert.assertEquals(folder.getEmlFilePath("b"), Iterables.getOnlyElement(subjectSearchResults).emlFilePath());
            }

            {
                List<IndexSearchResult> bothSubjectAndBodySearchResults = archivageIndex.search(new ArchivageQuery(null, null, null, null, HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.SUBJECT_OR_BODY, "avion"));
                Assert.assertEquals(2, bothSubjectAndBodySearchResults.size());
            }
        } catch (MissingRelevantKeywordException e) {
            Assert.fail(e + "n’aurait pas dû être levée");
        }

        try {
            String stopWord = "pas"; // est un stop word par défaut pour FrenchAnalyzer
            List<IndexSearchResult> stopWordSearchResults = archivageIndex.search(new ArchivageQuery(null, null, null, null, HasAnyAttachment.IGNORED, null, folder, SearchKeywordStrategy.SUBJECT_OR_BODY, stopWord));
            Assert.fail("aurait dû lever une exception");
        } catch (MissingRelevantKeywordException e) {
            LOGGER.trace("exception attendue levée", e);
        }
    }

    @Test
    public void testDeletion() throws MissingRelevantKeywordException {
        MountPoint mountPointToKeep = new MountPoint(Path.of("point de montage à conserver"));
        Folder folderInMountPointToKeep = mountPointToKeep.folder("dossier/sous-dossier");

        MountPoint mountPointToDelete = new MountPoint(Path.of("point de montage à supprimer"));
        Folder folderInMountPointToDelete = mountPointToDelete.folder("dossier/sous-dossier");

        try (ArchivageIndex.Writer archivageIndexWriter = archivageIndex.openWriter()) {
            archivageIndexWriter.add(new ArchivageEmail(folderInMountPointToKeep.getEmlFilePath("a"), "Salut", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(3), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folderInMountPointToKeep.getEmlFilePath("b"), "Everybody", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.SOME_ATTACHMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(4), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folderInMountPointToKeep.getEmlFilePath("c"), "Tout", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "corps du message en texte brut", null, getDatePlus(5), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folderInMountPointToKeep.getEmlFilePath("d"), "Le", new ArchivageEmail.Recipient("Alice", "alice@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.SOME_ATTACHMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(6), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folderInMountPointToDelete.getEmlFilePath("e"), "Monde", new ArchivageEmail.Recipient("Bob", "bob@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, null, "corps du message en HTML", getDatePlus(7), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folderInMountPointToDelete.getEmlFilePath("f"), "C’est", new ArchivageEmail.Recipient("Bob", "bob@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.SOME_ATTACHMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(8), Set.of(), "<Source EML>"));
            archivageIndexWriter.add(new ArchivageEmail(folderInMountPointToDelete.getEmlFilePath("g"), "Mardi et un autre mot", new ArchivageEmail.Recipient("Bob", "bob@douanes.fr"), Set.of(), Set.of(), HasAnyAttachment.NO_ATTACHEMENT, "corps du message en texte brut", "corps du message en HTML", getDatePlus(9), Set.of(), "<Source EML>"));
        }

        {
            List<IndexSearchResult> mountPointToDeleteIndexResults = archivageIndex.search(ArchivageQuery.forMountPoint(mountPointToDelete));
            Assert.assertEquals(3, mountPointToDeleteIndexResults.size());
        }

        try (ArchivageIndex.Writer archivageIndexWriter = archivageIndex.openWriter()) {
            archivageIndexWriter.delete(mountPointToDelete);
        }

        {
            List<IndexSearchResult> mountPointToKeepIndexResults = archivageIndex.search(ArchivageQuery.forMountPoint(mountPointToKeep));
            Assert.assertEquals(4, mountPointToKeepIndexResults.size());
        }
        {
            List<IndexSearchResult> mountPointToDeleteIndexResults = archivageIndex.search(ArchivageQuery.forMountPoint(mountPointToDelete));
            Assert.assertEquals(0, mountPointToDeleteIndexResults.size());
        }
    }

    @NotNull
    private static Date getDatePlus(int days) {
        LocalDateTime localDateTime = LocalDateTime.of(2023, 3, 10, 16, 56, 3).plusDays(days);
        Date date = Date.from(localDateTime.atZone(ZoneId.of("Europe/Paris")).toInstant());
        return date;
    }
}
