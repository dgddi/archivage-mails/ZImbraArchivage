package fr.mim_libre.archivage.zimbra;

import org.junit.Ignore;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.net.Authenticator;
import java.net.CookieHandler;
import java.net.HttpURLConnection;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpTimeoutException;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class ZimbraHttpClientTest {

    /**
     * Un bouchon qui simule un serveur Zimbra qui ne répond pas.
     */
    private static final HttpClient ZIMBRA_STUB = new HttpClient() {
        @Override
        public Optional<CookieHandler> cookieHandler() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public Optional<Duration> connectTimeout() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public HttpClient.Redirect followRedirects() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public Optional<ProxySelector> proxy() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public SSLContext sslContext() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public SSLParameters sslParameters() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public Optional<Authenticator> authenticator() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public HttpClient.Version version() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public Optional<Executor> executor() {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public <T> HttpResponse<T> send(HttpRequest request, HttpResponse.BodyHandler<T> responseBodyHandler) {
            return new HttpResponse<T>() {
                @Override
                public int statusCode() {
                    return HttpURLConnection.HTTP_UNAVAILABLE;
                }

                @Override
                public HttpRequest request() {
                    throw new UnsupportedOperationException("test stub");
                }

                @Override
                public Optional<HttpResponse<T>> previousResponse() {
                    throw new UnsupportedOperationException("test stub");
                }

                @Override
                public HttpHeaders headers() {
                    throw new UnsupportedOperationException("test stub");
                }

                @Override
                public T body() {
                    throw new UnsupportedOperationException("test stub");
                }

                @Override
                public Optional<SSLSession> sslSession() {
                    throw new UnsupportedOperationException("test stub");
                }

                @Override
                public URI uri() {
                    throw new UnsupportedOperationException("test stub");
                }

                @Override
                public HttpClient.Version version() {
                    throw new UnsupportedOperationException("test stub");
                }
            };
        }

        @Override
        public <T> CompletableFuture<HttpResponse<T>> sendAsync(HttpRequest request, HttpResponse.BodyHandler<T> responseBodyHandler) {
            throw new UnsupportedOperationException("test stub");
        }

        @Override
        public <T> CompletableFuture<HttpResponse<T>> sendAsync(HttpRequest request, HttpResponse.BodyHandler<T> responseBodyHandler, HttpResponse.PushPromiseHandler<T> pushPromiseHandler) {
            throw new UnsupportedOperationException("test stub");
        }
    };

    @Ignore("Trop long")
    @Test(expected = HttpTimeoutException.class)
    public void testTimeoutOnZimbraUnavailable() throws URISyntaxException, IOException, InterruptedException {
        ZimbraHttpClient zimbraHttpClient = new ZimbraHttpClient(ZIMBRA_STUB);
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(new URI("http://localhost:1234"))
                .build();
        zimbraHttpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
    }
}