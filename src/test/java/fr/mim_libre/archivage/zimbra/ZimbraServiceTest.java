package fr.mim_libre.archivage.zimbra;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.MoreCollectors;
import fr.mim_libre.archivage.PropertiesFileNotFoundArchivageException;
import fr.mim_libre.archivage.SessionStatus;
import fr.mim_libre.archivage.configuration.ArchivageConfigurationForTesting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Properties;
import java.util.UUID;

public class ZimbraServiceTest {

    private static final Logger LOGGER = LogManager.getLogger();

    public static final String USERNAME;

    public static final String PASSWORD;

    private ZimbraService zimbraService;

    static {
        try (final InputStream propertiesStream = ZimbraServiceTest.class.getResourceAsStream("zimbra-credentials.properties")) {
            Properties properties = new Properties();
            properties.load(propertiesStream);
            USERNAME = properties.getProperty("archivage.zimbra.test.email");
            PASSWORD = properties.getProperty("archivage.zimbra.test.password");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getUsername() {
        Assume.assumeFalse(USERNAME.isEmpty());
        return USERNAME;
    }

    public static String getPassword() {
        Assume.assumeFalse(PASSWORD.isEmpty());
        return PASSWORD;
    }

    @Before
    public void setUp() throws PropertiesFileNotFoundArchivageException {
        ArchivageConfigurationForTesting configuration = new ArchivageConfigurationForTesting();
        Assume.assumeTrue(configuration.hasZimbraUrls());
        zimbraService = new ZimbraService(configuration);
    }

    @Test
    public void testLogin() {

        try {
            ZimbraAuthToken zimbraAuthToken = zimbraService.login(getUsername(), "un_mot_de_passe_faux");
            LOGGER.debug("a obtenu " + zimbraAuthToken);
            Assert.fail("une exception aurait du être levée");
        } catch (AuthenticationFailedArchivageException e) {
            Assert.assertEquals("authentication failed for [%s]".formatted(getUsername()), e.getFaultReasonText());
            Assert.assertEquals("account.AUTH_FAILED", e.getFaultDetailErrorCode());
        } catch (ArchivageZimbraException e) {
            LOGGER.debug("erreur inattendue", e);
            Assert.fail("ne devrait pas avoir échoué");
        }

        try {
            ZimbraAuthToken zimbraAuthToken = zimbraService.login(getUsername(), getPassword());
            Assert.assertFalse(Strings.isNullOrEmpty(zimbraAuthToken.value()));
            Assert.assertTrue(zimbraAuthToken.lifetime().toMinutes() > 0);
        } catch (AuthenticationFailedArchivageException e) {
            LOGGER.debug("erreur inattendue " + e.getHost() + " " + e.getFaultReasonText() + " " + e.getFaultDetailErrorCode(), e);
            Assert.fail("ne devrait pas avoir échoué");
        } catch (ArchivageZimbraException e) {
            LOGGER.debug("erreur inattendue", e);
            Assert.fail("ne devrait pas avoir échoué");
        }
    }

    @Test
    public void testGetAllFolders() throws ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        ZimbraService.Folder folder = zimbraService.postGetFolderRequest(zimbraAuthToken);
        Assert.assertEquals(Path.of("/"), Path.of(folder.absFolderPath()));
        Assert.assertEquals(29, folder.children().size());
    }

    @Test
    public void testPostSearchRequest() throws ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        ZimbraService.SearchResponse searchResponse = zimbraService.postSearchRequest(zimbraAuthToken, ZimbraService.SearchRequest.forQuery(1, 0, ZimbraService.SearchRequest.Query.empty()));
        LOGGER.debug("searchResponse = " + searchResponse);
    }

    @Test
    public void testPostTagMsgActionRequest() throws ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        String id = getAnyMessageId(zimbraAuthToken);
        zimbraService.postTagMsgActionRequest(zimbraAuthToken, id, "Un exemple de tag");
    }

    @Test
    public void testPostDeleteMsgActionRequest() throws ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        String id = getAnyMessageId(zimbraAuthToken);
        zimbraService.postDeleteMsgActionRequest(zimbraAuthToken, id);
    }

    @Test
    public void testCreateFolderRequest() throws ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        String folderId = zimbraService.postCreateFolderRequest(zimbraAuthToken, "essai de création 1");
        LOGGER.debug("Identifiant du dossier créé " + folderId);
    }

    @Test
    public void testTags() throws ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        ImmutableSet<String> existingTags = zimbraService.postGetTagsRequest(zimbraAuthToken);
        LOGGER.trace("tags existants {}", existingTags);
        String aNewTag = UUID.randomUUID().toString();
        LOGGER.trace("va créer un tag {}", aNewTag);
        zimbraService.postCreateTagRequest(zimbraAuthToken, aNewTag);
        ImmutableSet<String> afterCreationExistingTags = zimbraService.postGetTagsRequest(zimbraAuthToken);
        Assert.assertEquals(existingTags.size() + 1, afterCreationExistingTags.size());
        Assert.assertTrue(afterCreationExistingTags.contains(aNewTag));
    }

    @Test
    public void testNoOp() throws ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        Assert.assertEquals(SessionStatus.CONNECTED, zimbraService.postNoOpRequest(zimbraAuthToken));

        ZimbraAuthToken invalidToken = new ZimbraAuthToken("un jeton invalide", Duration.ZERO);
        Assert.assertEquals(SessionStatus.DISCONNECTED, zimbraService.postNoOpRequest(invalidToken));

        // token créé le 2023-11-21T14:26:04.618071792
        ZimbraAuthToken expiredToken = new ZimbraAuthToken("0_cb80e8ec4619e73c8c3cec905b83cf29249b1ea3_69643d33363a62313035613363662d346136372d343137622d616132342d3364626463333764373463343b6578703d31333a313730303734353936343239363b76763d313a383b747970653d363a7a696d6272613b753d313a613b7469643d393a3533383031303239343b76657273696f6e3d31343a382e382e31355f47415f343137393b", Duration.ZERO);
        Assert.assertEquals(SessionStatus.DISCONNECTED, zimbraService.postNoOpRequest(expiredToken));
    }

    private ZimbraAuthToken login() {
        try {
            return zimbraService.login(getUsername(), getPassword());
        } catch (ArchivageZimbraException e) {
            throw new RuntimeException(e);
        }
    }

    private String getAnyMessageId(ZimbraAuthToken zimbraAuthToken) throws ArchivageZimbraException {
        ZimbraService.SearchResponse searchResponse = zimbraService.postSearchRequest(zimbraAuthToken, ZimbraService.SearchRequest.forQuery(1, 0, ZimbraService.SearchRequest.Query.empty()));
        String id = searchResponse.ms().stream()
                .map(ZimbraService.SearchResponse.M::id)
                .collect(MoreCollectors.onlyElement());
        return id;
    }
}
