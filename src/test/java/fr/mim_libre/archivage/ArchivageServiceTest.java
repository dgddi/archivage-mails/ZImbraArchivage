package fr.mim_libre.archivage;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.configuration.ArchivageConfigurationForTesting;
import fr.mim_libre.archivage.configuration.ConfigurationManager;
import fr.mim_libre.archivage.index.ArchivageIndex;
import fr.mim_libre.archivage.index.ArchivageQuery;
import fr.mim_libre.archivage.index.IndexSearchResult;
import fr.mim_libre.archivage.index.IndexationRequest;
import fr.mim_libre.archivage.index.MissingRelevantKeywordException;
import fr.mim_libre.archivage.logging.ArchivageLogger;
import fr.mim_libre.archivage.store.ArchivageStoreException;
import fr.mim_libre.archivage.store.Folder;
import fr.mim_libre.archivage.store.MountPoint;
import fr.mim_libre.archivage.store.Store;
import fr.mim_libre.archivage.store.StoreScanProgressHandler;
import fr.mim_libre.archivage.zimbra.ArchivageZimbraException;
import fr.mim_libre.archivage.zimbra.ZimbraAuthToken;
import fr.mim_libre.archivage.zimbra.ZimbraService;
import fr.mim_libre.archivage.zimbra.ZimbraServiceTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

public class ArchivageServiceTest {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final StoreScanProgressHandler NULL_STORE_SCAN_PROGRESS_HANDLER = new StoreScanProgressHandler() {
        @Override
        public void onFilesListed(int numberOfEmlFilesFound) {

        }

        @Override
        public void onOneFileRead() {

        }

        @Override
        public void onScanFailure(MountPoint mountPoint, ArchivageStoreException archivageStoreException) {
            Assert.fail("erreur inattendue");
        }
    };

    private ArchivageService archivageService;

    private MountPoint mountPoint;

    private ZimbraService zimbraService;

    @Before
    public void setUp() throws IOException, PropertiesFileNotFoundArchivageException {
        ArchivageConfigurationForTesting configuration = new ArchivageConfigurationForTesting();
        Assume.assumeTrue(configuration.hasZimbraUrls());
        zimbraService = new ZimbraService(configuration);
        mountPoint = new MountPoint(Files.createTempDirectory("archivage"));
        Path logsPath = Files.createTempDirectory("archivage-journaux");
        ArchivageLogger archivageLogger = new ArchivageLogger(configuration, logsPath);
        Store store = new Store(archivageLogger);
        Path indexPath = Files.createTempDirectory("archivage-index");
        ArchivageIndex archivageIndex = new ArchivageIndex(indexPath);
        ConfigurationManager configurationManager = new ConfigurationManager(Files.createTempDirectory("archivage").toFile());
        archivageService = new ArchivageService(zimbraService, store, archivageIndex, archivageLogger, configurationManager, configuration);
    }

    @Test
    @Ignore("trop long, à filtrer")
    public void download() throws NotEnoughDiskSpaceArchivageException, ArchivageZimbraException {
        ZimbraAuthToken zimbraAuthToken = login();
        archivageService.download(zimbraAuthToken, Collections.singleton(new ZimbraService.SearchRequest.Query(Duration.ofDays(1000), null, null, null)), mountPoint, new ArchivageService.SynchronisationProgressHandler() {
            @Override
            public void terminated(ZonedDateTime start) {

            }

            @Override
            public void onErrorOccurred() {

            }

            @Override
            public void onCancelledSynchronisation(ArchivageException e) {

            }

            @Override
            public void registerFolderWithInvalidPath(ZimbraService.Folder folder) {

            }

            @Override
            public void synchronisationPlanComputationStarted() {

            }

            @Override
            public void synchronisationPlanComputed(int toBeDone, long sizeToBeDownloaded) {

            }

            @Override
            public void done(ArchivageService.DownloadTask downloadTask) {

            }
        }, () -> false);
    }

    @Test
    public void indexMountPoint() throws MissingRelevantKeywordException {
        archivageService.indexMountPoints(new IndexationRequest(Collections.singleton(mountPoint), "Test", IndexationRequest.CleaningStrategy.DO_NOTHING), NULL_STORE_SCAN_PROGRESS_HANDLER);
        Folder folder = mountPoint.rootFolder();
        archivageService.browse(ArchivageQuery.forFolder(folder));
    }

    @Test
    @Ignore("Temporaire, échoue à cause du bricolage qui ignore les certificats SSL")
    public void testRestoreAll() throws MissingRelevantKeywordException {
        archivageService.indexMountPoints(new IndexationRequest(Collections.singleton(mountPoint), "Test", IndexationRequest.CleaningStrategy.DO_NOTHING), NULL_STORE_SCAN_PROGRESS_HANDLER);
        ZimbraAuthToken zimbraAuthToken = login();
        List<IndexSearchResult> indexSearchResults = archivageService.browse(ArchivageQuery.forMountPoint(mountPoint));
        indexSearchResults.stream()
                .map(IndexSearchResult::emlFilePath)
                .forEach(emlFilePath -> {
                    try {
                        archivageService.restore(zimbraAuthToken, emlFilePath);
                    } catch (ArchivageZimbraException | ArchivageStoreException e) {
                        LOGGER.error("impossible de restaurer " + emlFilePath, e);
                        Assert.fail("impossible de restaurer " + emlFilePath);
                    }
                });
    }

    private ZimbraAuthToken login() {
        try {
            return zimbraService.login(ZimbraServiceTest.getUsername(), ZimbraServiceTest.getPassword());
        } catch (ArchivageZimbraException e) {
            throw new RuntimeException(e);
        }
    }
}
