package fr.mim_libre.archivage.configuration;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ConfigurationManagerTest {

    private static final Logger LOGGER = LogManager.getLogger();

    private File userDirectory;

    @Before
    public void setUp() throws IOException {
        userDirectory = Files.createTempDirectory("archivage").toFile();
    }

    @Test
    public void testCreateProfile() throws IOException {
        ConfigurationManager configurationManager = new ConfigurationManager(userDirectory);
        try {
            configurationManager.getConfiguration();
        } catch (ArchivageMisconfigurationException e) {
            Assert.fail("erreur inattendue");
        }
        configurationManager.createProfile("a@zimbra", Files.createTempDirectory("a"));
        configurationManager.createProfile("b@zimbra", Files.createTempDirectory("b"));
        configurationManager.createProfile("c@zimbra", Files.createTempDirectory("b"));
        ConfigurationFileContent configurationFileContent = configurationManager.read();
        LOGGER.debug("profilesFileContent = " + configurationFileContent);
        Assert.assertEquals(3, configurationFileContent.getProfiles().size());
    }
}
