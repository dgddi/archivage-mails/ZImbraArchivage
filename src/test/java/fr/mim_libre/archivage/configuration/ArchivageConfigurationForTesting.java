package fr.mim_libre.archivage.configuration;

/*-
 * #%L
 * archivage
 * %%
 * Copyright (C) 2023 Code Lutin, DGDDI, Zextras
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.mim_libre.archivage.PropertiesFileNotFoundArchivageException;
import fr.mim_libre.archivage.logging.ArchivageLoggingLevel;

import java.net.URI;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public class ArchivageConfigurationForTesting implements ArchivageConfiguration {

    private final ArchivageProperties archivageProperties;

    public ArchivageConfigurationForTesting() throws PropertiesFileNotFoundArchivageException {
        this.archivageProperties = new ArchivageProperties();
    }

    @Override
    public URI getZimbraSoapUri() {
        return URI.create(this.archivageProperties.getZimbraSoapUri());
    }

    @Override
    public URI getZimbraRestUri() {
        return URI.create(this.archivageProperties.getZimbraRestUri());
    }

    @Override
    public String getSpecificClientName() { return this.archivageProperties.getSpecificClientName(); }

    @Override
    public int getMaxFileNameLength() {
        return 50;
    }

    @Override
    public ZoneId getZoneId() {
        return ZoneId.of("Europe/Paris");
    }

    @Override
    public Clock getClock() {
        return Clock.fixed(Instant.now(), getZoneId());
    }

    @Override
    public ArchivageLoggingLevel getLoggingLevel() {
        return ArchivageLoggingLevel.STANDARD;
    }

    @Override
    public int getLoggingDaysOfRetention() {
        return 14;
    }

    @Override
    public int getSynchronisationBatchSize() {
        return 500;
    }

    @Override
    public boolean isDeletionOnSynchronizationEnabled() {
        return false;
    }

    public boolean hasZimbraUrls() {
        return !this.archivageProperties.getZimbraSoapUri().isBlank() && !this.archivageProperties.getZimbraRestUri().isBlank();
    }
}
